/** menu.hpp

\verbatim
       ||          __  __  ___  ____   ___  ____  _____ ____
       --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
  -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
  -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
       --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
       ||
\endverbatim

*/


#ifndef SRC_MENU_HPP_
#define SRC_MENU_HPP_


#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "components/id.hpp"
#include "globals.hpp"
#include "json.hpp"
#include "postal_service/subscription.hpp"


class Stage;


class Menu : public Subscription {
public:
    Menu();
    ~Menu();

    void load(SDL_Renderer* renderer, const std::string filename);
    void unload();

    void start(Stage& stage);
    void stop(Stage& stage);
    bool isRunning() const;

    void notify(const EVENT event, const ID id, Stage& stage) override;
    void update(Stage& stage);

    void render(SDL_Renderer* renderer);

private:
    SDL_Texture* loadTexture(SDL_Renderer* renderer, const std::string filename);

    bool _is_running;

    int _window_width;
    int _window_height;

    SDL_Texture* _current;
    SDL_Texture* _ui_base;
    SDL_Texture* _ui_success;
    SDL_Texture* _ui_failure;
    SDL_Texture* _ui_map;
};


#endif /* SRC_MENU_HPP_ */
