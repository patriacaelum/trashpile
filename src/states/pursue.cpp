#include "pursue.hpp"


Pursue::Pursue(const ID id, const JSON& data, Stage& stage)
:   Pathfinding("pursue", false, {}, id, data)
{}


/** When entering the pursue state, the target location is set to be the player
entity.

\param stage
    the currently loaded stage.
*/
void Pursue::enter(Stage& stage) {
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "Pursue :: id=%d is entering pursue state", id);
    Pathfinding::enter(stage);

    _reference_id = stage.player();

    if (in_seeing_range(stage)) {
        startPathfinding(stage);
    }
}


void Pursue::exit(Stage& stage) {
    _reference_id = -1;

    Pathfinding::exit(stage);
}


/** The pursue state periodically updates the pathfinding to the target's
location until the target is outside of the entity's seeing range.

\param stage
    the currently loaded stage.
*/
State* Pursue::update(Stage& stage) {
    // On last frame of animation loop, check if pathfinding finished
    if (_hframes == _max) {
        setPath(stage);
    }

    // Restart pathfinding when destination is reached
    if (stage.scene.path(id).empty()) {
        if (in_seeing_range(stage)) {
            startPathfinding(stage);
        }
        else {
            _finished = true;

            return nullptr;
        }
    }

    followPath(stage);

    return Pathfinding::update(stage);
}


void Pursue::setGoal(Stage& stage) {
    const SDL_Rect map = stage.scene.mapSize();
    const Position position = stage.scene.position(_reference_id);
    const SDL_Rect hitbox = stage.scene.hitbox(_reference_id).unionbox();

    _goal = {
        (position.x - map.x + hitbox.x) / _scale,
        (position.y - map.y + hitbox.y) / _scale,
        (hitbox.w / _scale) + 2,
        (hitbox.h / _scale) + 2
    };
}


bool Pursue::in_seeing_range(Stage& stage) {
    // Define the seeing range of the pursuer
    const Position position = stage.scene.position(id);
    const SDL_Rect hitbox = stage.scene.hitbox(id).unionbox();
    const int seeing_range = stage.scene.property(id).at("seeing_range");

    const SDL_Rect range = {
        position.x + hitbox.x - seeing_range,
        position.y + hitbox.y - seeing_range,
        hitbox.w + 2 * seeing_range,
        hitbox.h + 2 * seeing_range
    };

    // Define the position of the pursued entity
    const Position g_position = stage.scene.position(_reference_id);
    const SDL_Rect g_hitbox = stage.scene.hitbox(_reference_id).unionbox();

    const SDL_Rect target = {
        g_position.x + g_hitbox.x,
        g_position.y + g_hitbox.y,
        g_hitbox.w,
        g_hitbox.h
    };

    if (SDL_HasIntersection(&range, &target) == SDL_TRUE) {
        return true;
    }

    return false;
}
