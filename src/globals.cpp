#include "globals.hpp"


std::unordered_map<std::string, std::string> BGM = {
	{"ff", "../../audio/final_fantasy_iv_prelude.mp3"}
};


std::unordered_map<std::string, DIRECTION> DIRECTION_STRINGS = {
	{"north", DIRECTION::NORTH},
	{"northeast", DIRECTION::NORTHEAST},
	{"east", DIRECTION::EAST},
	{"southeast", DIRECTION::SOUTHEAST},
	{"south", DIRECTION::SOUTH},
	{"southwest", DIRECTION::SOUTHWEST},
	{"west", DIRECTION::WEST},
	{"northwest", DIRECTION::NORTHWEST}
};


std::unordered_map<std::string, std::pair<std::string, int>> FONTS = {
	{"normal", {"./arial.ttf", 18}},
	{"title", {"./arial.ttf", 24}}
};


std::unordered_map<std::string, std::string> IMG = {
	{"test", "../../img/test.png"},
	{"npc1", "../../img/selfportait.png"},
	{"test_grass", "../../img/test_grass.png"},
	{"test_tree", "../../img/test_tree.png"},
	{"bridge","../../img/bridge.png"},
	{"platform", "../../img/platform.png"},
	{"railing", "../../img/railing.png"},
	{"test_textbox", "../../img/test_textbox.png"},
	{"test_sign", "../../img/test_sign.png"},
	{"red_flower", "../../img/red_rose.png"},
	{"purple_flower", "../../img/purple_flower_pot.png"},
	{"skeley", "../../img/testskeleton.png"},
	{"jungle_house", "../../img/jungle_house.png"},
	{"test_door", "../../img/test_door.png"},
	{"grappling_hook", "../../img/grappling_hook.png"}
};


std::unordered_map<std::string, std::string> SCENES = {
	{"test", "../../scenes/test.json"}
};


std::unordered_map<std::string, STATES> STATES_STRINGS = {
	{"alert", STATES::ALERT},
	{"attack_long", STATES::ATTACK_LONG},
	{"attack_short", STATES::ATTACK_SHORT},
	{"door_close", STATES::DOOR_CLOSE},
	{"door_open", STATES::DOOR_OPEN},
	{"dormant", STATES::DORMANT},
	{"goto", STATES::GOTO},
	{"grappling_hook_inactive", STATES::GRAPPLING_HOOK_INACTIVE},
	{"grappling_hook_long", STATES::GRAPPLING_HOOK_LONG},
	{"grappling_hook_short", STATES::GRAPPLING_HOOK_SHORT},
	{"guard", STATES::GUARD},
	{"idle", STATES::IDLE},
	{"listen", STATES::LISTEN},
	{"noisemaker_off", STATES::NOISEMAKER_OFF},
	{"noisemaker_on", STATES::NOISEMAKER_ON},
	{"pursue", STATES::PURSUE},
	{"roll", STATES::ROLL},
	{"room", STATES::ROOM},
	{"sneak", STATES::SNEAK},
	{"speak", STATES::SPEAK},
	{"walk", STATES::WALK}
};
