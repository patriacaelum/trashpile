/** room.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

A `Room` is like the `Dormant` state except, unlike the `Dormant` state, which
infinitely loops over a single animation, the `Room` state changes its mode
based on the level of knowledge the player entity has about the `Room`.
*/


#ifndef SRC_STATES_ROOM_HPP_
#define SRC_STATES_ROOM_HPP_


#include <SDL2/SDL.h>

#include "state.hpp"

#include "../components/id.hpp"
#include "../components/knowledge.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Room : public State {
public:
    Room(const ID id, const JSON& data, Stage& stage);

    State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_ROOM_HPP_ */
