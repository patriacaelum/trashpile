/** door_close.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The initial state for door-like entities. This is the equivalent idle state.

The state process is

```
Close -> Open
```
*/


#ifndef SRC_STATES_DOOR_CLOSE_HPP_
#define SRC_STATES_DOOR_CLOSE_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../scene.hpp"
#include "../stage.hpp"


class DoorClose : public State {
public:
	DoorClose(const ID id, const JSON& data, Stage& stage);

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_DOOR_CLOSE_HPP_ */
