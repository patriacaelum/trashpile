#include "init.hpp"


/** Initializes `SDL2` and all its subsystems.

Initializes `SDL2`, `SDL2_mixer`, `SDL2_image`, and 'SDL2_ttf'.

\returns: `true` if all subsystems were initialized, `false` otherwise.
*/
bool initAll() {
	bool sdl = initSDL();
	bool image = initImage();
	bool mixer = initMixer();
	bool ttf = initTTF();

	return (sdl && image && mixer && ttf);
}


/** Initializes `SDL2` with all subsystems and sets up the logger.

This is where the log priorities are set. The initialized subsystems
include:
  - TIMER
  - AUDIO
  - VIDEO
  - JOYSTICK
  - HAPTIC
  - GAME_CONTROLLER
  - EVENTS

\returns    `true` if all subsystems were initialized, `false`
             otherwise.
*/
bool initSDL() {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::clog << "initSDL :: SDL failed initialization" << std::endl;

        return false;
    }

    SDL_LogSetOutputFunction(&Logger, NULL);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_ASSERT, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_AUDIO, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_ERROR, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_INPUT, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_SYSTEM, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_TEST, SDL_LOG_PRIORITY_INFO);
	SDL_LogSetPriority(SDL_LOG_CATEGORY_VIDEO, SDL_LOG_PRIORITY_INFO);

    SDL_LogInfo(
        SDL_LOG_CATEGORY_APPLICATION,
        "initSDL :: SDL successfully initialized all subsystems"
    );

    return true;
}


/** Initializes `SDL2_mixer`.

SDL Mixer handles everything audio related. `SDL2_mixer` is initialized
with flags for compatibility with `.flac`, `.mp3`, and `.ogg` files.

TODO: remove mp3 dependence and use only flac (ogg if needed)

\returns    `true` if `SDL2_mixer` initialized, `false` otherwise.
*/
bool initMixer() {
    int flags = MIX_INIT_FLAC | MIX_INIT_MP3 | MIX_INIT_OGG;
    int initted = Mix_Init(flags);

    if ((initted & flags) != flags) {
    	SDL_LogError(
    		SDL_LOG_CATEGORY_AUDIO,
			"initMixer :: SDL mixer failed initialization: %s",
			Mix_GetError ()
		);

    	SDL_LogWarn(
    		SDL_LOG_CATEGORY_AUDIO,
			"initMixer :: SDL mixer initializing with no external library support"
		);

    	flags = 0;
    	initted = Mix_Init(flags);
    }

    if ((initted & flags) != flags) {
    	SDL_LogError(
    		SDL_LOG_CATEGORY_AUDIO,
			"initMixer :: SDL mixer failed initialization: %s",
			Mix_GetError()
		);

    	return false;
    }

    SDL_LogInfo(
    	SDL_LOG_CATEGORY_AUDIO,
		"initMixer :: SDL mixer successfully initialized"
	);

    return true;
}


/** Initializes `SDL2_image`.

SDL Image handles loading different image formats. `SDL2_image` is
initialized with flags for compatibility with `.png` files.

\returns    `true` if `SDL2_image` initialized, `false` otherwise.
*/
bool initImage() {
	int flags = IMG_INIT_PNG;
	int initted = IMG_Init(flags);

	if ((initted & flags) != flags) {
		SDL_LogError(
			SDL_LOG_CATEGORY_VIDEO,
			"initImage :: SDL image failed initialization: %s",
			IMG_GetError()
		);

		return false;
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_VIDEO,
		"initImage :: SDL image successfully initialized"
	);

	return true;
}


/** Initializes `SDL2_ttf`.
 *
SDL TTF handles interpreting and rendering TrueType fonts for text.

\returns    'true' if `SDL2_ttf` initialized, 'false' otherwise.
*/
bool initTTF() {
	int initted = TTF_Init();

	if(initted == -1){
		SDL_LogError (
			 SDL_LOG_CATEGORY_ERROR,
			 "initTTF :: SDL TTF failed initialization: %s",
			 TTF_GetError()
		 );

		return false;
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_VIDEO,
		"initTTF :: SDL TTF successfully initialized"
	);

	return true;
}

/** Quits `SDL2` and all its subsystems.

Quits `SDL2`, `SDL2_image`, `SDL2_mixer`, and 'SDL2_ttf'.
*/
void quitAll() {
	quitTTF();
	quitMixer();
	quitImage();
	quitSDL();
}


/** Quits `SDL2` and all its subsystems.
*/
void quitSDL() {
    SDL_LogWarn(
    	SDL_LOG_CATEGORY_APPLICATION,
		"quitSDL :: quitting all subsystems"
	);

    atexit(SDL_Quit);
}


/** Quits `SDL2_image`.
*/
void quitImage() {
	SDL_LogWarn(
		SDL_LOG_CATEGORY_VIDEO,
		"quitImage :: quitting SDL image"
	);

	IMG_Quit();
}


/** Quits `SDL2_mixer`.
*/
void quitMixer() {
	SDL_LogWarn(
		SDL_LOG_CATEGORY_AUDIO,
		"quitMixer :: quitting SDL mixer"
	);

	while (Mix_Init(0)) {
		Mix_Quit();
	}
}


/** Quits `SDL2_ttf`.
*/
void quitTTF(){
	SDL_LogWarn(
		SDL_LOG_PRIORITY_INFO,
		"quitTTF :: quitting SDL TTF"
	);

	TTF_Quit();
}
