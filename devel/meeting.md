# 2019/12/08

### Updates
  - Austin has done nothing
  - Ron has done nothing
  - Lincoln has done nothing
    
### Problems
  - Do something?

--------------------------------------------------------------------------------

# 2019/12/01 

### Updates
  - Scene class is finished 
  - Ron to think about interactions 
  - Austin state machines and collision
  - Link concept art. Experiement, try to figure out the feel to out game

--------------------------------------------------------------------------------

# 2019/11/24

### Updates
  - Scene class is mostly implemented, but the toFloor() method is still unfinished
  
### Problems
  - Collidables can be finite state machine stacks , but the states should be separate circles to distinguish different objects
  - Interaction code is hard to fix, they just commented out now

  
--------------------------------------------------------------------------------


# 2019/11/17

### Updates 
  - Notes on disguise
  - Scene class interface discussed
  
### Issues
  - Ron please look for a good way to implement Scene::toFloor() method
  - Austin please implement the operators for the Scene class
  - RON NEEDS TO FIX DISCORD NOTIFICATIONS

### Problems
  - Instead of tackling each issue separately, we should propose a design interface before implementing the actual code, to be discussed with team
  - Proposals should include the header and a UML of class relations
  - Major problems should be reported to discord


--------------------------------------------------------------------------------

# 2019/11/03

### Updates
  - Ron made collidables collidable on the 2nd floor, next thing is to make a door on the second floor to teleport back to first floor
  - Austin started drawing using overhead perspective and is working on a snow song based on Debussy's "Snow is Dancing"
  - Austin is reading a book on dresses, to work clothing design
  
### Issues
  - Make the invisible wall around the 2nd floor to not move everywhere
  - Scene class should hold a vector of vectors
  - Problem is to move objects from one floor to another, we need to figure out a way to generalize this into a function
  - It may be beneficial to split the each floor into floor tiles and midground objects since the floor tiles don't collide with anything
  - Lincoln and Austin to work on how overhead perspective look
  

--------------------------------------------------------------------------

# 2019/10/27

### Updates:
  - Austin finished long grapple animation
  - Development now has sign text render problem, crashes when interacting with sign
  - Austin wrote a new chord progression that's a little too complicate
  - Ron drew new sprites and background
  - Lincoln continues his quest to be the best QA

### Issues:
  - Scene class requires reordering whenever object changes y-coordinate
  - Austin found a talk on physical penetration testers (guys who infiltrate buildings to find security flaws), notes are in devel, further research required
  - Find interesting uses of overhead camera angle, put together a reference list?

  
------------------------------------------------------------------------

# 2019-10-20

### Updates
  - Ron fix fixes. no more reading sign from 2nd floor 
  - Austin long grapple animation, just need a few more frames 
  - Lincoln QAing 
  - reorder rendering code 
  - walking in front vs back of npc 


------------------------------------------------------------------------

# 2019/10/13

### Updates
  - Created new Milestone: try different art styles, pick one we are happy with later after test level code is done
  - Dark + gloomy art style with dark story 
  - Bright + happy art style with dark story
  - bright and happy everything 

### Isuues
  - Austin to work on scene storing code 
  - Ron to work on finishing second floor stuff + bug fixes: shouldnt be able to interact with sign (or anything) on ground floor while on 2nd 
  - try to finish with Demo1 ost 
  - Lincoln to continue with QA work
