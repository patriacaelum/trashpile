/** id.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `ID` uniquely identifies an `Entity` with a name and its index in the
`Scene`.
*/


#ifndef SRC_COMPONENTS_ID_HPP_
#define SRC_COMPONENTS_ID_HPP_


using ID = int;


#endif /* SRC_COMPONENTS_ID_HPP_ */
