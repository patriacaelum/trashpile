/** state.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The base class for all states. Each state controls the animation of the entity,
how it moves in the `Scene`, and how it interacts with other entities.

The data must be in the form

```
{
	"states": {
		"name of state": {
			"state key": "state value"
		}
	}
}
```
*/


#ifndef SRC_STATES_STATE_HPP_
#define SRC_STATES_STATE_HPP_


#include <cmath>
#include <string>
#include <vector>

#include <SDL2/SDL.h>

#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class State {
public:
	State(
		const std::string name,
		const bool is_initial,
		const std::vector<STATES> dependent_states,
		const ID id,
		const JSON& data
	);
	virtual ~State();

	static State* create(
		const STATES state,
		const ID id,
		const JSON& data,
		Stage& stage
	);

	virtual void enter(Stage& stage);
	virtual void exit(Stage& stage);

	virtual void setReferenceId(const ID reference_id);
	virtual State* notify(const EVENT event, const ID sender, Stage& stage);
	virtual State* update(Stage& stage);

	virtual std::vector<STATES> dependentStates() const;
	int hframes() const;
	bool isFinished() const;
	bool isInitial() const;
	std::string name() const;

	/// ID of the associated entity
	ID id = -1;

protected:
	virtual void updateSprite(Stage& stage);

	virtual void notifySubentity(const EVENT event, const ID id, Stage& stage);
	virtual void updateSubentity(Stage& stage);

	int factorFromDirection(const DIRECTION direction);

	/// Name of the state;
	const std::string _name;

	/// The entity ID of any other entity to use as a reference for a variety of
	/// purposes.
	ID _reference_id;

	/// These are the states that this state is dependent on, i.e. the states
	/// that this state may return. This should be overrided when declaring
	/// a child state class that is dependent on other states.
	const std::vector<STATES> _dependent_states;

	/// Indicates if the state has finished its action.
	bool _finished;

	/// The remaining number of frames the current sprite will be held.
	int _hframes;
	/// The y-coordinate of the origin of the subsection of the spritesheet for
	/// sprites used for this state.
	int _origin;

	/// The number of sprites in a single cycle.
	const int _nsprites;
	/// Indicates if the sprites are meant to loop.
	const bool _loops;
	/// The maximum number of frames a single sprite is held for.
	const int _max;

	/// Determines if this state is an initial state.
	const bool _is_initial;
};


#endif /* SRC_STATES_STATE_HPP_ */
