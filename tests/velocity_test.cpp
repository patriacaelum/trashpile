/** velocity_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `Velocity` component.
*/


#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/velocity.hpp"
#include "../src/json.hpp"


TEST_CASE("Velocity component tests", "[velocity][component]") {
	JSON data = R"(
		{
			"velocity": {
				"dx": 0,
				"dy": 1
			},
			"bad_velocity": {
				"foo": "bar"
			}
		}
	)"_json;

	SECTION("Velocity component default constructor") {
		Velocity velocity = data.at("velocity").get<Velocity>();

		REQUIRE(velocity.dx == 0);
		REQUIRE(velocity.dy == 1);
	}

	SECTION("Velocity component bad key") {
		REQUIRE_THROWS(data.at("bad_velocity").get<Velocity>());
	}
}
