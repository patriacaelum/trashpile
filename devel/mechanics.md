# Game mechanics


### Grappling Hook

One of the ideas we had as a main weapon or tool was a grappling hook. Some
possible extensions are

- Grapple toward large stationary objects or grapple smaller objects toward the
  player
- A fishing pole is a weaker tool but can be used to pull objects upward from a
  lower area or lower an object from a higher area (like a microphone to record
  sounds or conversations)
- Attach the hook to a large stationary object and lower the player toward a
  lower area, an interesting perspective can be used where the player hangs
  upside-down, but the entire screen is flipped so the player is still upright
  (like the shot in "Spiderman: Into the Spiderverse")


### Growth-based system

The basic idea is focusing the idea on how your character grows instead of the
raw stats they are given.

One of the things I notice about the games where stats are involved is that I
don't really care what the numbers are (like the stats in Pokemon, or Final
Fantasy), I really care more about whether my character is a defensive tank, or
an all-out attacker. What I'm proposing is a system based on how you want your
character to grow instead of increasing your stats. This is the implementation
I've thought of so far.

Say when you're buying equipment, the player doesn't see how it increases their
attack or defense (since we don't know the stats of whom we're fighting anyway,
so it's almost arbitrary), but instead we see how the character feels about the
equipment. I've only thought of two so far, but for the sake of example, say the
equipment affects the character's confidence, and how heavy the equipment is.

This lends itself easily to a risk and reward system. If, for example, the
player chooses a very big and heavy armour. This would increase the character's
confidence (and unknowingly, the character's defense), however, during battle,
the character's base defense stat would increase more slowly. This would also
reduce the character's speed, but their base speed stat would increase more
quickly as time went on. If the player had chosen light and flexible armour
instead, then the character's defense would be lower, but their base defense
would grow more quickly, and their speed would be much higher but grow more
slowly. This way, very skilled players could play nude and increase their base
stats very quickly, and less skilled players would still be able to play, but be
forced to constantly upgrade their equipment.

This also allows natural conversation if barriers are needed for certain areas.
For example, if the player wanted to enter a difficult area, then the guard may
say, "Are you sure you're ready to enter? You don't look very _confident_."


### Saving

based on Undertale, where the game remembers if you've played the game and
killed the character before.

One of the things I somewhat enjoy, but also get tired of, is doing badly on a
level or regretting a decision, and then just rebooting the game so that I can
do better. We can turn this on its head and make it so that normal game
mechanics, like saving, have a consequence in the game. What would essentially
happen is if the player does an action (or perhaps inaction), the game would
remember what happened, even if the player rebooted and started again from their
last save point (one that the player chose, not the game).

For example, say the player decided to enter a house and rob them, but later on,
the player does something wrong and reboots to try again. This time, the family
who had their goods stolen would somehow know they might get robbed today (like
they had a dream about it or something), and decided to lock their cabinets.
Another example might be if the player killed someone at a certain spot, and
decided to reboot because they wanted to save them instead, then when the player
looks for the victim, the victim avoids the player and the area they got killed
because they have a bad feeling about the area and player.

This can bring about interesting quests, like ones that are blocked or
permanently changed because of an error, or having to think about the
consequences to get the desired outcome. For example, perhaps the goal is to
save someone from getting assassinated. The way to save that man might be to
watch the assassin first, and then reboot to mess with his procedure. Another
way might be to the man first so that he avoids the area where his assassin is.

One of the problems may be that constantly rebooting may be abused. To encourage
the player to consider when a good time to reboot is may be to make rebooting
have consequences of its own. One idea is that rebooting is equivalent in the
game world to suicide. Thus, if you reboot inside a town, then people will
witness the death and avoid that area. Killing yourself multiple times in a town
may cause everyone to stay indoors, thus closing all the stores, and making the
town empty (which may be a desired result as well). If this is not desired, then
the player must think carefully about when a suitable time to kill themselves
is. This also allows consistency so if the player if killed, the same thing
happens as if they killed themselves.

This leads to interesting ways to complete missions. For example, if the mission
is to sneak into a bank vault and steal gold, there are multiple ways to do this
while exploiting this mechanic

1. The player could sneak into the bank and steal the gold.
2. The player sneaks into the bank and is seen. The player then reboots to try
   again, but this time the area they took is more heavily guarded.
3. The player takes advantage of this system and purposely gets caught in a
   certain area, as to make sure that area is guarded more heavily, and proceeds
   to go a different way that is now less heavily guarded.
4. The player enters and purposely gets seen and kills themselves in a certain
   area so that guards are wary of that area and is thus less heavily guarded.
5. A mission can be split into two parts, where the first part is mostly a
   reconnaissance mission, where the player explores the area beforehand, trying
   to find information, and the second part is when the player gets caught, and
   the goal then becomes to try to escape or try to commit suicide in order to
   save their newfound knowledge.
   

### Stealing

We should allow the player to choose when/where to steal from a character when
needed. One way that we can have 'stealing' work is by having the player move up
close to the target and then entering in a sequence of characters in order to
successfully steal from a character.

This could be hard pretty hard for the player to do especially if the target is
moving at the same time. We can either freeze the game to allow the player to
enter in the keys or force the player to enter in the keys while keeping close
to the target in real time. I believe that we should have the player enter keys
in real time since a moving target is harder to steal from.

The player would have to face some sort of consequence if they are caught
stealing. (if the user doesn't enter the keys correctly/quickly, or if noticed
by another NPC) Consequences can be:

- Being forced to reboot (commit suicide)
- Fight back (fighting should be difficult in the game if we want to keep
  stealing as the main mechanic)
- Escape out of sight
- Frame a nearby NPC

The difficulty of stealing should probably be dependent on the character that
the player is stealing from. For example, it is easier to steal from a regular
person as opposed to a guard. We should not tell the player if the target will
be hard to steal from. The player will then have to rely on the appearance of
the target to judge. Again, if the target looks like a guard/cop/tough guy, then
it should indicate to the player that the target will be hard to steal from.

#### Lincoln Comment on Stealing Mechanic:

An idea I had to enhance the stealing mechanic above. When you successfully
steal from someone, perhaps that can increase the movement speed of the
character. Therefore, riskier 'plays' can occur where you steal something, get a
quick speed boost to hide in a location. Perhaps this can be an obtainable
upgrade or just a general mechanic. I feel this could encourage unique ways to
complete a level, similar to how in Mario you can chain various jumps and dives
to move or get to locations you normally would not be able to with the base
mechanic.

#### Actions for Stealth

During one of our meetings, we discussed ideas for how our thief could traverse
stealthily. The list of ideas that resulted
- Using sound and distraction
  - Throwing or breaking objects to distract guards
  - Being able to hear the footsteps of guards around walls and corners
  - Throwing a guard off your trail by framing someone else
- Hiding in plain sight
  - Dressing in more regular clothes to scope out the area
  - Hiding among a crowd of people
- Planning and traps
  - Being able to prepare something for the mission
- Revisiting areas
  - Reconnaissance missions and being able to know the possible paths
  - Sabotaging the area to make the later visit easier
- Integrated design
  - Mission areas are integrated with its surroundings
  - Stealth mechanics are not limited to mission areas, and are applicable in
    multiple places
    
#### Physical Penetration Techniques

A summary of [this talk](https://www.youtube.com/watch?v=rnmcRTnTNC8) by Deviant
Ollam and real world physical penetration.

- (1:00) Apparently lock-picking is a slow process and is seldom needed, often
  the last resort
- (3:00) Remove door hinges
- (4:00) A small hook or wire pushes in the latch
- (6:00) Doors are poorly installed with huge latch holes or wide door spacing
- (10:00) Thumb turn flipper
- (11:00) Simple infrared or thermal sensors are on the inside, so you can spray
  a condesning gas, or smoke from an E-cig, or even a cold drink and trip it
  open
- (15:00) Radar sensors track motion, and you can blow up a balloon and let it
  go flying to trip it
- (16:00) Under-door tools
- (18:00) Use film or wire to make a loop over the door, and pull it around the
  door handle
- (19:00) Cart crash shrouds that protect the door handle are surprisingly hard
  to get around
- (20:00) Door handles installed vertically are hard to open from the top or
  bottom, as are common in hotels
- (21:00) Steal keys
- (23:00) Card reader machines might have a master key by the manufacturer,
  allowing you to open to bridge the right circuit or flip the test switch, it
  turns out key variety is not as varied as every key being unique
- (29:00) Even car manufacturers use a master key, used by police force and the
  retired cars go to taxis, so taxi driver keys could open cop cars
- (34:00) Act as an elevator repair guy, make yourself known by calling help,
  say you're testing it, if they can hear you, and if they know where you're
  calling from, also hide stuff in your clipboard

- Use a car jack to expand the door frame

#### Comments from a Former Jewel Robber

A summary of [this video](https://www.youtube.com/watch?v=CtWqv0Z3ErM) where a
real jewel robber makes comments on how real heists are reflected in movies.

- (0:00) Apparently "thief" has a connotation of a petty house thief, whereas
  the more hardcore professional guys who stick guns in people's faces prefer to
  be called "robbers."
- (1:00) Stealing money from the bank is more akin to stealing money from the
  insurance companies since money in a bank is insured, rather than stealing
  ordinary people's money. In fact, some people would gain from getting robbed
  or their building purged in fire because their struggling business is replaced
  with insurance money.
- (2:00) "Cash is king, but cash is heavy." Though other things, like diamonds
  or art, are lighter, you need a "fence," which is someone who exchanges stolen
  goods for cash. Fences are pretty specialized in the merchandise they deal
  with (diamonds, VCRs, clothes, phones, etc.)
- (3:00) People only steal what they know, implying they have an educational
  background on the object they are stealing, because they know its value.
- (5:00) "If you rob something, rob something you can get rid of." You'd only
  steal something rare like art if you knew there was a buyer.
- (6:00) Sometimes it's easier to steal the entire safe rather than opening the
  safe during the heist. Also, misdirection and timing is key.
- (7:00) Use small incidents to test how long it takes for the police or guards
  to arrive and how they immediately respond. A small fire or fire alarm, for
  example of false alarms. This also allows you to know the size of the
  response.
- (9:00) Sometimes the big safe doesn't hold the most valuable items, and the
  most valuable item is actually stored in a more commonplace location so that
  it's hard to keep track of. Also, secure doors make it hard to enter, but also
  makes it hard for anyone else to leave.
- (11:00) "Casing the joint" is reconnaissance. You want to know every detail
  about the people, their shifts, their personalities. You want to know the
  location, when the mail arrives, when it's most crowded. He mentions how he'd
  know when the sun comes up, and where the glare is the brightest, and use the
  glare for cover so people passing by couldn't see inside.
- (12:00) Blackmail is stronger than bribery. You want dirt that would hurt
  somebody. Money can always be bought out for a higher price, but blackmail
  will shut them up forever.
- (13:00) Larger groups opens more possibility for a snitch or information leak.
  There must be a lot of money involved if it's going to be split up so many
  ways. The less information is distributed, the better, because it ensures you
  can't snitch on each other.
- (14:00) Casinos place their cages (where the money is kept) in the centre and
  away from any exits. Nearly impossible to rob them. Also, different reactions
  to getting robbed are crying, laughing, anger, silence.
- (15:00) He covered his fingertips with Krazy Glue so that he could handle
  things without the obvious use of gloves while never leaving prints. Also, the
  mask should cover your eyes because people are good at remembering eyes. It
  should be a fabric like pantyhose that disfigures your face while still
  allowing you to see through it.
- (16:00) Some safes can only be opened by certain people at certain times.
  Banks may put exposives in packs of money if they are taken from the bank
  without being disarmed. Bank employees are trained to give the money away so
  nobody gets hurt, and the mitigation procedures hinge on getting the robbers
  out of there as soon as possible.
- (18:00) Older video security systems had to have the recordings on-site, which
  could be taken and destroyed. Newer ones may have direct live-feeds to
  somewhere off-site.

#### Comments from a Former Bank Robber

A summary of [this video](https://www.youtube.com/watch?v=Fgvdl87lqc8).

- (2:00) Where he uses tape around his gloves to make sure the gloves don't
  come off and the sleeves are kept down, hiding tattoos or other markers.
  Getting nervous before entering.
- (4:00) Getting your hands on the manuals the bank keeps for scheduling and
  when they keep safe doors open is important.
- (6:00) The money the bank keeps is often still wrapped in cellophane, fresh
  from the factory. The silent alarm only goes off after the robbers have left
  to avoid a hostage situation. This is standard procedure.
- (7:00) When asking a question, ask a question that we already know the
  answer to to know if they were going to try to lie, and if they are nervous.
  Only certain people are allowed to have silent alarms and have the
  authorization to push it.
- (9:00) Solo robbers will often hit smaller banks and hand the teller a note
  telling them that this is a robbery and that they have a weapon. The tellers
  are trained to hand the robber cash that has a tracker, tracer, or dye pack.
  This is a stack they already have to the side and is used as bait.
- (10:00) Another reason why cameras are pointed at doors is so they can tell
  the height of the person walking in relative to the door frame.


#### Comments from a Former Art Thief

A summary of [this video](https://www.youtube.com/watch?v=AzGs26X86i4)

- (8:00) There have never been art theft where someone is killed. Art stolen
  where someone was killed is too high profile, and is unsellable.
- (14:00) Museum cameras are always being repaired during a crime and there is
  no footage to hand over to police. But really this is probably because
  museums are underfunded.
- (16:00) Hitler and the Nazis stole a lot of paintings and art, and a famous
  Rembrandt was in the salt mines in Austria. The guy who found it was Jewish
  and wasn't allowed to see the painting when he was a child.
  
### Disguise

##### CIA Operatives on How Spies Use Disguises

A summary of what is important for the CIA when [they use disguises](https://www.youtube.com/watch?v=JASUsVY5YJ8&t=302s)
- (2:00) Hair (short or long, straight or curly, dark or light, coloured or
  grey, facial hair), dental facades change shape of your face and you how
  pronouce words
- (3:00) Older people are considered less threatening, women have more range and
  can look more masculine, disguises are usually additive, not subtractive
- (4:00) Behaviours like the way you eat (which hand you hold utensils), how you
  hold certain objects (between fingers or with thumb), standing with weight on
  one foot or both feet, physical change is required to change how you walk
  (like stone in shoe, tape around knee or ankle), the way you sit (slouch or
  straight)
- Possibility for quick change uses crowds while removing layers, or adding
  layers from a stored place or from surrounding areas
  
    
# Visual Design

### Rule of thirds

based on what happens in Shadow of the Colossus

For the very epic locations where the focus is on travelling, the camera can
subtly move the player to the intersection of the rule of thirds to better show
off the environment. This is in addition to other camera tricks that can be
applied.

### Toggle upper floors

One of the interesting things that we can explore in our 2D top-down perspective
is how to use the third dimension. One possibility is to allow for the
visibility of the upper floor through a toggle. Perhaps this blurs the floor the
player is currently, but allows to see the already explored space above. This
might also extend to just a higher area, like the tree canopies or roofs of
buildings.

### Top-down to bottom-up camera

One of the interesting ways we can allow the use of the third dimension is by
being able to switch between a top-down perspective and a bottom-up perspective,
where the camera is below the floor and looks upward.

Imagine that you are trying to stealth past some guards, but there doesn't seem
to be a straightforward path on the ground. Then the player can switch to the
bottom-up perspective and see that there lampposts lining the area and can climb
upward and jump across. Then they come across a large chandelier and climb up
the rope to reach the upper floor.
