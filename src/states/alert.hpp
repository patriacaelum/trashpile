/** alert.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Alert` state is a transitionary state before the `Goto` state which takes
care of finding a path to the location of the object that alerted the guard.
*/


#ifndef SRC_STATES_ALERT_HPP_
#define SRC_STATES_ALERT_HPP_


#include <vector>

#include "pathfinding.hpp"

#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../pathfinding/graph.hpp"
#include "../pathfinding/graph_node.hpp"
#include "../stage.hpp"


class Alert : public Pathfinding {
public:
    Alert(const ID id, const JSON& data, Stage& stage);

    void enter(Stage& stage) override;
    void exit(Stage& stage) override;

    State* update(Stage& stage) override;

private:
    void setGoal(Stage& stage) override;
};


#endif /* SRC_STATES_ALERT_HPP_ */
