#include "stage.hpp"


/** Creates the stage.
*/
Stage::Stage():
	camera(),
	scene(),
	menu(),
	visual_novel(),
	layers(),
	_player(0),
	_sort_layer()
{
	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Stage :: successfully created empty stage"
	);
}


/** Updates all dynamic objects in the scene.

The update currently takes three major steps.

1. All objects have their state updated. This includes changing their sprites
   and moving around the scene.
2. The camera is updated, centered around the player entity.
3. The layers that have changed are sorted.

\param input	a global input value.
*/
void Stage::update() {
	if (menu.isRunning()) {
		menu.update(*this);
		mailbox.distribute(*this, &menu);
	}
	else {
		updateStates();
		mailbox.distribute(*this);
	}

	updateCamera();
	sortLayers();
}


/** The entities are loaded in the `Scene` and the layers are sorted.

\param renderer		the `SDL_Renderer` is required to load sprites.
 */
void Stage::load(const std::string filename, SDL_Renderer* renderer) {
	_sort_layer.reset();
	_sort_layer.flip();

	mailbox.reset();

	scene.unload();
	scene.load(filename, renderer, *this);

	for (ID id: scene.ids()) {
		// Subentities are not stored in layers since they are connected to
		// their parent entities
		if (scene.superentity(id) != -1) {
			continue;
		}

		if (scene.name(id) == "kito") {
			_player = id;
		}

		show(id);
	}

	sortLayers();

	menu.load(renderer, "../../config/ui.json");

	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_A_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_B_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_X_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_DOWN_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_LEFT_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_RIGHT_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::PRESS_UP_BUTTON);

	mailbox.subscribe(&scene.pda(_player), EVENT::RELEASE_B_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::RELEASE_DOWN_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::RELEASE_LEFT_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::RELEASE_RIGHT_BUTTON);
	mailbox.subscribe(&scene.pda(_player), EVENT::RELEASE_UP_BUTTON);

	mailbox.subscribe(&menu, EVENT::PRESS_START_BUTTON);
	mailbox.subscribe(&menu, EVENT::PRESS_SELECT_BUTTON);

	mailbox.subscribe(&visual_novel, EVENT::DIALOGUE_START);
	mailbox.subscribe(&visual_novel, EVENT::DIALOGUE_STOP);

	camera.setRenderer(renderer);
	visual_novel.setRenderer(renderer);

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Stage :: successfully set test stage"
	);
}


/** Hides an entity from the screen by removing the entity from the layers.

\param id		the entity ID.
*/
void Stage::hide(const ID& id) {
	for (auto& layer: layers) {
		if (std::find(layer.begin(), layer.end(), id) != layer.end()) {
			layer.erase(std::remove(layer.begin(), layer.end(), id), layer.end());
		}
	}
}


/** Shows an entity on screen by adding the entity to the layers.

\param id		the entity ID.
*/
void Stage::show(const ID& id) {
	int layer = scene.position(id).z;

	while (layers.size() <= layer) {
		layers.push_back({});
	}

	layers[layer].push_back(id);
}


/** Checks for a collision on the layer.

\param lid		the entity ID.
\param dist		the number of pixels to expand each pixel. Default is 0.

\returns		an entity ID.
*/
ID Stage::findCollision(const ID& lid, const int dist) {
	for (ID rid: layers[scene.position(lid).z]) {
		// Ignore itself and its superentity
		if (lid == rid || lid == scene.subentity(rid)) {
			continue;
		}

		if (isCollision(lid, rid, dist)) {
			return rid;
		}
	}

	return -1;
}


/** Checks for a collision between two entities.

The calculation is done by expanding the hitboxes by the specified number of
pixels (default is 0) and checking if they intersect.

Note that this method ignores the layer the entity is on.

\param lid		the entity ID.
\param rid		the other entity ID.
\param dist		the hitbox is expanded by this many pixels. The default value
				is `0`.

\returns		`true` if there is a collision, `false` otherwise.
*/
bool Stage::isCollision(const ID& lid, const ID& rid, const int dist) {
	// Properties of left entity
	const Position& lposition = scene.position(lid);
	const SDL_Rect& lsprite = scene.sprite(lid).sprite;
	const SDL_Point lcentre = {
		(lsprite.w + 2 * dist) / 2, 
		(lsprite.w + 2 * dist) / 2
	};
	DIRECTION ldirection = DIRECTION::SOUTH;

	if (scene.property(lid).at("symmetry") == 1) {
		ldirection = lposition.d;
	}

	// Properties of right entity
	const Position& rposition = scene.position(rid);
	const SDL_Rect& rsprite = scene.sprite(rid).sprite;
	const SDL_Point rcentre = {
		(rsprite.w + 2 * dist) / 2, 
		(rsprite.w + 2 * dist) / 2
	};
	DIRECTION rdirection = DIRECTION::SOUTH;

	if (scene.property(rid).at("symmetry") == 1) {
		rdirection = rposition.d;
	}

	// Check for intersection of hitboxes
	for (SDL_Rect lbox: scene.hitbox(lid).hitbox(lsprite, ldirection, lcentre)) {
		lbox = {
			lbox.x + lposition.x - dist,
			lbox.y + lposition.y - dist,
			lbox.w + 2 * dist,
			lbox.h + 2 * dist
		};

		for (SDL_Rect rbox: scene.hitbox(rid).hitbox(rsprite, rdirection, rcentre)) {
			rbox = {
				rbox.x + rposition.x - dist,
				rbox.y + rposition.y - dist,
				rbox.w + 2 * dist,
				rbox.h + 2 * dist
			};

			if (SDL_HasIntersection(&lbox, &rbox) == SDL_TRUE) {
				return true;
			}
		}
	}

	return false;
}


/** Checks if the sprites of two entities overlaps.

Note that this method ignores the layer the entity is on.

\param lid		the entity ID.
\param rid		the other entity ID.

\returns		`true` if there is an overlap, `false` otherwise.
*/
bool Stage::isOverlap(const ID& lid, const ID& rid) {
	// Properties of left entity
	const Position& lposition = scene.position(lid);
	const SDL_Rect& lsprite = scene.sprite(lid).sprite;
	SDL_Rect lbox = {
		lposition.x,
		lposition.y,
		lsprite.w,
		lsprite.h
	};

	// Properties of right entity
	const Position& rposition = scene.position(rid);
	const SDL_Rect& rsprite = scene.sprite(rid).sprite;
	SDL_Rect rbox = {
		rposition.x,
		rposition.y,
		rsprite.w,
		rsprite.h
	};

	if (SDL_HasIntersection(&lbox, &rbox) == SDL_TRUE) {
		return true;
	}

	return false;
}



/** Connects two entities.

This method fails if either entity has already established a connection with
another entity.

\param sender		the entity ID of the initiating party.
\param receiver		the entity ID of the receiving party.
*/
void Stage::connect(const ID& sender, const ID& receiver) {
	try {
		scene.connector(sender).connect(receiver);
	}
	catch (std::logic_error& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Stage :: failed to connect '%d' and '%d': %s",
			sender,
			receiver,
			error.what()
		);

		throw std::logic_error("Stage :: failed to establish connection");
	}

	try {
		scene.connector(receiver).connect(sender);
	}
	catch (std::logic_error& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Stage :: failed to connect '%d' and '%d': %s",
			receiver,
			sender,
			error.what()
		);
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Stage :: successfully connected '%d' and '%d'",
		sender,
		receiver
	);
}


/** Disconnects two entities.

If either entity is connected to an entity other than each other, no action is
made for that entity.

\param sender		the entity ID of the initiating party.
\param receiver		the entity ID of the receiving party.
*/
void Stage::disconnect(const ID& sender, const ID& receiver) {
	try {
		scene.connector(sender).disconnect();
	}
	catch (std::out_of_range& error) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_APPLICATION,
			"Stage :: failed to disconnect '%d' from '%d': %s",
			sender,
			receiver,
			error.what()
		);
	}

	try {
		scene.connector(receiver).disconnect();
	}
	catch (std::out_of_range& error) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_APPLICATION,
			"Stage :: failed to disconnect '%d' from '%d': %s",
			receiver,
			sender,
			error.what()
		);
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Stage :: successfully disconnected '%d' from '%d'",
		sender,
		receiver
	);
}


/** The id of the player entity.

\returns	the `ID` of the player entity.
*/
ID Stage::player() const {
	return _player;
}


/** Removes the entity ID from its current layer and moves it to the one
specified.

\param id			the entity ID to be moved.
\param fromLayer	the layer the ID is from.
\param toLayer		the layer the ID is copied to.
*/
void Stage::moveLayer(const ID id, const int from_layer, const int to_layer) {
	std::vector<ID>& layer = layers.at(from_layer);
	auto pend = std::remove(layer.begin(), layer.end(), id);
	layer.erase(pend, layer.end());

	layers.at(to_layer).push_back(id);

	_sort_layer.set(to_layer);

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Stage :: moved id=%d from layer %d to %d",
		id,
		from_layer,
		to_layer
	);
}


/** Checks each layer and sorts that layer if it has changed.

Each layer first checks that each entity is on the correct layer. The z-position
of the entity may have changed during the `updateStates()` step.

Then the layer is sorted by y-position. This affects the rendering order.
*/
void Stage::sortLayers() {
	for (int layer = 0; layer < layers.size(); ++layer) {
		// Check that each entity is on the correct layer
		for (ID& id: layers.at(layer)) {
			if (scene.position(id).z != layer) {
				moveLayer(id, layer, scene.position(id).z);
			}
		}

		// Check that each entity is rendered in the correct order
		if (_sort_layer.test(layer)) {
			std::sort(
				layers[layer].begin(),
				layers[layer].end(),
				[this](const ID& lhs, const ID& rhs) {
					return compare(lhs, rhs);
				}
			);

			_sort_layer.reset(layer);
		}
	}
}


/** Updates the position of the camera.

Currently, this simply centers the camera on the player's position.
*/
void Stage::updateCamera() {
	camera.frame.x = scene.position(_player).x
		+ (scene.sprite(_player).sprite.w - camera.frame.w) / 2;
	camera.frame.y = scene.position(_player).y
		+ (scene.sprite(_player).sprite.h - camera.frame.h) / 2;
}


/** Updates the state of each object in the scene.

\param input	an integer corresponding to a game input, defined in
				`globals.cpp`.
*/
void Stage::updateStates() {
	for (int layer = 0; layer < layers.size(); ++layer) {
		for (const ID& id: layers[layer]) {
			// The y-position affects rendering order
			const Position previous = scene.position(id);
			scene.pda(id).update(*this);
			const Position current = scene.position(id);

			if (current.y != previous.y) {
				_sort_layer.set(layer);
			}
		}
	}
}


/** Comparison operation for sorting by y-coordinate. This is required to sort
entities.

This is so the entities can be sorted such that the entities closer to the
bottom of the screen are rendered last. In other words, entities with a higher
y-coordinate are sorted to the end of the list.

\param lid		an entity ID.
\param rid		the other entity ID.

\returns		`true` if the left-hand side is behind the right-hand side, and
				`false` if the left-hand side is in front of the right-hand
				side.
*/
bool Stage::compare(const ID& lid, const ID& rid) {
	return (scene.position(lid).y + scene.hitbox(lid).highpoint(scene.sprite(lid).sprite)) < (scene.position(rid).y + scene.hitbox(rid).highpoint(scene.sprite(rid).sprite));
}
