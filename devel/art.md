# Look and Feel

One of the things we have to consider is the look and feel of the game. This is
mainly attributed to the art style and sonic atmosphere.

### Dark on Dark

Since the story premise is rather dark, here are some examples of a more dark a
broody atmosphere. The key aspect to highlight is the extreme contrast of light
and darkness and strong silhouettes outlined by light.

- [Film noire](https://en.wikipedia.org/wiki/Film_noir) has a lot of good
  examples of high contrast images. For example, Citizen Kane.
  - [In a newsroom](https://1.bp.blogspot.com/_Kj2Kry7hfp0/TI6lSmKms0I/AAAAAAAAAZ4/BbggiM_l9y0/s400/citizenkane03.jpg)
    shows two figures in front of two light sources, light beams flaring out
    from their silhouettes.
  - [A face half-lit](https://3.bp.blogspot.com/_Kj2Kry7hfp0/TI6ml-cDzlI/AAAAAAAAAcI/JPd7-UDNzNg/s400/citizenkane21.jpg)
    is very obvious symbolism of inner conflict, showing both a familiar face
    and malicious intent.
  - [Walking
    downstairs](https://3.bp.blogspot.com/_Kj2Kry7hfp0/TI6m8NOqfTI/AAAAAAAAAcw/yd4MkZZocAU/s400/citizenkane26.jpg)
    shows two figures in front of a single light source, slightly less dramatic
    than the first example, but you can see more of their features on the rim of
    their silhouettes, and the low angle supplies the drama.
- [Return of the Obra Dinn](https://en.wikipedia.org/wiki/Return_of_the_Obra_Dinn)
  uses a monochromatic pixelated shader, mimicking old computers. An interesting
  visual style since it's high definition, but with pixel art shading.
- [Chiaroscuro art](https://en.wikipedia.org/wiki/Chiaroscuro) is a classical
  painting style that uses high contrast.

### Light on Dark

It may be interesting to contrast a darker story with a more cheery atmosphere.
- I can't really think of a good example? Maybe Ico or Doki Doki Literature
  Club.

### Painted Game?

One of the things that I noticed is that once 3D games became more mainstream,
there has been a nostalgia for pixel art, since that was about as far as 2D
games got before Ocarina of Time. However, there hasn't been too many games that
pushed the limits of 2D animation the same way that film has. I thought it might
be interesting to use a more painterly art style, like a moving painting, or
very abstract background. Sine the game is about memories, I also thought it
would be interesting if it sometimes merged the present and the past, creating a
surrealist feeling.

- [League of Legends Phoenix](https://www.youtube.com/watch?v=i1IKnWDecwA) has
  an amazing animation with a painterly aesthetic. Honestly like every frame a
  painting. Even the particle effects look hand-drawn. Goddamn.
- [Okami](https://en.wikipedia.org/wiki/%C5%8Ckami) is a game with a traditional
  Japanese painting aesthetic on its 3D models. The wind looks like moving
  brushstroke wisps. It's worth a play, even if just to admire its visual style.
- [Samurai Jack](https://en.wikipedia.org/wiki/Samurai_Jack), especially the
  fifth season, blends an interesting mix of minimalism in the character
  designs, abstract backgrounds, and split screens for action sequences.


# Overhead Camera

- [Opening of I Will Not Work Overtime, Period!](http://irozuku.org/fansub/drama/watashi-teiji-de-kaerimasu-ep-06/)
  has a top-down shot of the
  main character in white standing still against a sea of business men in black
  rushing past her. The contrast of direction, motion vs. stillness, and colour
  makes a really great shot.
- Tabletop events like a dining table showing only the hands, cardgames like at
  a casino, mahjong, writing or drawing, etc.
- [Axial cuts](https://en.wikipedia.org/wiki/Axial_cut) could be an interesting
  way to bring an uncomfortable emphasis on certain things.
- Formations from dance, especially musicals, synchronized swimming, and the
  military often make good use of moving patterns from a bird's eye view.
  - The movies of Busby Berkeley really made use of a the bird's eye perspective
    that was impossible on stage.
    [The human waterfall](https://www.youtube.com/watch?v=FRqcZcrgPaU) is the
    most famous sequence, but note that each person is treated like a cog in a
    machine, in particular the objectification of women.
    [Dames](https://www.youtube.com/watch?v=9bVJol3-XLo) is a pretty trippy
    sequence.
- Second person perspective? The only example I have come across is a racing
  game where you are actually controlling the car in front of you, but you are
  seeing it from the perspective of the pursuer. This might be an interesting
  angle, like if you were seeing the scene from a very high viewpoint from the
  perspective of a sniper trying to kill you, while you control the character
  below trying to weave your way through.
