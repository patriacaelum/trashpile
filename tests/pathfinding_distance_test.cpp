/** pathfinding_distance.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for the distance functions in the pathfinding module.
*/


#include <catch2/catch.hpp>

#include "../src/pathfinding/distance.hpp"
#include "../src/pathfinding/graph_node.hpp"


TEST_CASE("Pathfinding distance functions tests", "[pathfinding][distance]") {
    SECTION("Pathfinding manhattan distance") {
        GraphNode node1 = GraphNode(1, 2);
        GraphNode node2 = GraphNode(3, 5);

        REQUIRE(manhattan_distance(&node1, &node2) == 5);
    }

    SECTION("Pathfinding octinal distance") {
        GraphNode node1 = GraphNode(1, 2);
        GraphNode node2 = GraphNode(3, 5);

        REQUIRE(octinal_distance(&node1, &node2) == 3);
    }
}
