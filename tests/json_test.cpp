/** json_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for JSON loading.
*/


#include <string>

#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/json.hpp"


TEST_CASE("JSON loading tests", "[json][load][io]") {
	JSON data = R"(
		{
			"string": "foobar",
			"int": 42,
			"bool": true,
			"json": {
				"inner": 69
			},
			"point": {
				"x": 0,
				"y": 1
			},
			"bad_point": {
				"foo": "bar"
			},
			"points": {
				"point0": {
					"x": 2,
					"y": 3
				},
				"point1": {
					"x": 4,
					"y": 5
				}
			},
			"empty_points": {
			},
			"rect": {
				"x": 6,
				"y": 7,
				"w": 8,
				"h": 9
			},
			"bad_rect": {
				"foo": "bar"
			},
			"rects": {
				"rect0": {
					"x": 10,
					"y": 11,
					"w": 12,
					"h": 13
				},
				"rect1": {
					"x": 14,
					"y": 15,
					"w": 16,
					"h": 17
				}
			},
			"empty_rects": {
			}
		}
	)"_json;


	SECTION("Get non-existent key from JSON") {
		REQUIRE_THROWS(data.at("god").get<std::string>());
	}


	SECTION("Get string primitive from JSON") {
		REQUIRE(data.at("string").get<std::string>() == "foobar");
	}


	SECTION("Get integer primitive from JSON") {
		REQUIRE(data.at("int").get<int>() == 42);
	}


	SECTION("Get boolean primitive from JSON") {
		REQUIRE(data.at("bool").get<bool>());
	}


	SECTION("Get SDL_Point from JSON") {
		SDL_Point point = data.at("point").get<SDL_Point>();

		REQUIRE(point.x == 0);
		REQUIRE(point.y == 1);
	}


	SECTION("Get SDL_Point with missing arguments from JSON") {
		REQUIRE_THROWS(data.at("bad_point").get<SDL_Point>());
	}


	SECTION("Get vector of SDL_Point from JSON") {
		std::vector<SDL_Point> points = data.at("points").get<std::vector<SDL_Point>>();

		REQUIRE(points.size() == 2);

		REQUIRE(points[0].x == 2);
		REQUIRE(points[0].y == 3);

		REQUIRE(points[1].x == 4);
		REQUIRE(points[1].y == 5);
	}


	SECTION("Get empty vector of SDL_Point from JSON") {
		std::vector<SDL_Point> points = data.at("empty_points").get<std::vector<SDL_Point>>();

		REQUIRE(points.empty());
	}


	SECTION("Get SDL_Rect from JSON") {
		SDL_Rect rect = data.at("rect").get<SDL_Rect>();

		REQUIRE(rect.x == 6);
		REQUIRE(rect.y == 7);
		REQUIRE(rect.w == 8);
		REQUIRE(rect.h == 9);
	}


	SECTION("Get SDL_Rect with missing arguments from JSON") {
		REQUIRE_THROWS(data.at("bad_rect").get<SDL_Rect>());
	}


	SECTION("Get vector of SDL_Rect from JSON") {
		std::vector<SDL_Rect> rects = data.at("rects").get<std::vector<SDL_Rect>>();

		REQUIRE(rects.size() == 2);

		REQUIRE(rects[0].x == 10);
		REQUIRE(rects[0].y == 11);
		REQUIRE(rects[0].w == 12);
		REQUIRE(rects[0].h == 13);

		REQUIRE(rects[1].x == 14);
		REQUIRE(rects[1].y == 15);
		REQUIRE(rects[1].w == 16);
		REQUIRE(rects[1].h == 17);
	}


	SECTION("Get empty vector of SDL_Rect from JSON") {
		std::vector<SDL_Rect> rects = data.at("empty_rects").get<std::vector<SDL_Rect>>();

		REQUIRE(rects.empty());
	}
}
