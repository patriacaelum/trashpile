/** pushdown_automaton.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `PushdownAutomaton` controls the state machine and the connections between
those states. The state machine operates as a stack, so that when the entity
moves to a different state, if it does not return a new state, the entity will
revert back to the original state.

The data must be in the form

```
{
	"states": {
		"name of state": {
			"state variable": "state value"
		}
	}
}
```
*/


#ifndef SRC_COMPONENTS_PUSHDOWN_AUTOMATON_HPP_
#define SRC_COMPONENTS_PUSHDOWN_AUTOMATON_HPP_


#include <exception>
#include <stack>
#include <string>
#include <unordered_map>

#include "id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../postal_service/subscription.hpp"


class Scene;
class Stage;
class State;


class PushdownAutomaton : public Subscription {
public:
	PushdownAutomaton();
	~PushdownAutomaton();

	State* operator[](const STATES state);
	State* at(const STATES state);

	void load(const STATES key, const ID id, const JSON& data, Stage& stage);

	bool hasInitial() const;
	bool hasDependentStates() const;

	State* currentState();
	void notify(const EVENT event, const ID sender, Stage& stage) override;
	void update(Stage& stage);

	/// ID of the associated entity
	ID id = -1;

private:
	bool hasDependentStates(STATES state) const;

	void pushdown(State* state, Stage& stage);
	void removeFinishedStates(Stage& stage);

	/// Holds all the possible states this entity may take on
	std::unordered_map<STATES, State*> _states;

	/// The core pushdown automata
	std::stack<State*> _pda;
};


#endif /* SRC_COMPONENTS_PUSHDOWN_AUTOMATON_HPP_ */
