/** init.cpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

`InitAll` initializes all `SDL2` subsystems, including `SDL2_mixer`,
`SDL2_image`, and `SDL_TTF`.


The logging priorities should be set here in `initSDL`. For more
information, look in `logger.hpp`.
*/


#ifndef SRC_INIT_HPP_
#define SRC_INIT_HPP_


#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include "logger.hpp"


bool initAll();

bool initSDL();
bool initImage();
bool initMixer();
bool initTTF();

void quitAll();

void quitSDL();
void quitImage();
void quitMixer();
void quitTTF();


#endif /* SRC_INIT_HPP_ */
