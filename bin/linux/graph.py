import numpy as np
import matplotlib.pyplot as plt

from matplotlib.animation import FuncAnimation


with open("graph.txt", "r") as file:
    lines = file.readlines()

traversable = np.ones([500, 500])
g_score = np.zeros([500, 500])
h_score = np.zeros([500, 500])

for line in lines:
    x, y, t, g, h = line.split(",")

    x = int(x)
    y = int(y)
    t = int(t)
    g = int(g) if g != "inf" else 0
    h = int(h) if h != "inf" else 0

    traversable[y,x] = t
    g_score[y,x] = g
    h_score[y,x] = h

# path = []

# with open("path.txt", "r") as file:
#     lines = file.readlines()

# n_step = 1
# for line in lines:
#     x, y = line.split(",")

#     x = int(x)
#     y = int(y)

#     traversable[y,x] = n_step
#     n_step += 1

plt.title("traversable")
plt.imshow(traversable)
plt.colorbar()
plt.savefig("traversable.png")

# plt.clf()
# plt.title("g-score")
# plt.imshow(g_score)
# plt.colorbar()
# plt.show()

# plt.clf()
# plt.title("h-score")
# plt.imshow(h_score)
# plt.colorbar()
# plt.show()