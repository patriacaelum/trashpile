/** logger.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The custom logging function that prints to the terminal and writes to a
daily log file.


A standard log takes the format:

    SDL_Log$PRIORITY (
        SDL_LOG_CATEGORY_$CATEGORY,
        $MESSAGE
    );

$PRIORITY describes how important the action is. The priority can be one
of the following, from least to greatest:
  - Verbose
  - Debug
  - Info
  - Warn
  - Error
  - Critical

The priority to be logged can be set, currently in `main.cpp`, using the
command:

    SDL_LogSetAllPriority (SDL_LOG_PRIORITY_$PRIORITY);

$CATEGORY describes the function of the code being executed. The
category can be one of the following:
  - APPLICATION
  - ASSERT
  - AUDIO
  - ERROR
  - INPUT
  - RENDER
  - SYSTEM
  - TEST
  - VIDEO

$MESSAGE is a string containing information about the code being
executed. This can also be a formatted string followed by its formatted
arguments, for example:

    SDL_LogError(
        SDL_LOG_CATEGORY_ERROR,
        "Something happened: %s",
        SDL_GetError ()
    );


\author    Austin Nhung
\date      2018-06-19
*/


#ifndef SRC_LOGGER_HPP_
#define SRC_LOGGER_HPP_


#include <ctime>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <map>
#include <SDL2/SDL.h>


/// Log category types.
extern std::map <int, std::string> CATEGORIES;

/// Log priorities, from least to greatest.
extern std::map <int, std::string> PRIORITIES;


void Logger(
		void* userdata,
		int category,
		SDL_LogPriority priority,
		const char *message
	);


#endif /* SRC_LOGGER_HPP_ */
