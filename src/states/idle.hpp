/** idle.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

A generic idle state for characters.

The state process is

```
Idle -> Walk -> ShortAttack -> LongAttack
     -> ShortAttack -> LongAttack
     -> Listen <-> Speak
     -> Speak <-> Listen
```
*/


#ifndef SRC_STATES_IDLE_HPP_
#define SRC_STATES_IDLE_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../components/velocity.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../scene.hpp"


class Idle: public State {
public:
	Idle(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_IDLE_HPP_ */
