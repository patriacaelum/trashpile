#include "pushdown_automaton.hpp"
#include "../states/state.hpp"


PushdownAutomaton::PushdownAutomaton()
:	_states(),
	_pda()
{}


/** Destroy all the states of the entity.
*/
PushdownAutomaton::~PushdownAutomaton() {
	for (auto& state: _states) {
		delete state.second;
	}
}


/** Return the associated state.

\param state
	the global state value.

\returns
	a `State` pointer.
*/
State* PushdownAutomaton::operator[](const STATES state) {
	return _states[state];
}


/** Return the associated state.

This method is the same as the `[]` operators except it throws an error if the
key does not exist.

\param state
	the global state value.

\returns
	a `State` pointer.
*/
State* PushdownAutomaton::at(const STATES state) {
	return _states.at(state);
}


/** Load a state for the entity.

This associates a state pointer to a global state value.

\param key
	the global state value.
\param id
	the id of the entity this state belongs to.
\param data
	the JSON data for the state.
\param stage
	the currently loaded stage.
*/
void PushdownAutomaton::load(
	const STATES key,
	const ID id,
	const JSON& data,
	Stage& stage
) {
	if (_states[key] != NULL) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"PushdownAutomaton :: failed to assign state %d for id=%d: already assigned",
			key,
			id
		);

		throw std::invalid_argument(
			"PushdownAutomaton :: failed to assign state: already assigned"
		);
	}

	State* state = State::create(key, id, data, stage);
	_states[key] = state;

	if (state->isInitial()) {
		if (_pda.empty()) {
			_pda.push(state);
			_pda.top()->enter(stage);
		}
		else {
			SDL_LogWarn(
				SDL_LOG_CATEGORY_ASSERT,
				"PushdownAutomaton :: failed to set '%s' as initial state for id=%d: initial state already set to '%s'",
				state->name().c_str(),
				id,
				_pda.top()->name().c_str()
			);
		}
	}
}


/** Check if the entity has been given an initial state.

\returns
	`true` if an initial state has been set, `false` otherwise.
*/
bool PushdownAutomaton::hasInitial() const {
	return _pda.size() > 0;
}


/** Check if all dependent states of the inital state have been loaded.

This method should be called after all the states have been loaded and the
initial state has been set.

\returns
	`true` if all dependent states have been loaded, `false` otherwise.
*/
bool PushdownAutomaton::hasDependentStates() const {
	if (_pda.size() == 0) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_APPLICATION,
			"PushdownAutomaton :: id=%d has no initial state set, failed to check for dependent states",
			id
		);

		return false;
	}
	else if (_pda.size() > 1) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_APPLICATION,
			"PushdownAutomaton :: id=%d has more than one state set, failed to check for dependent states",
			id
		);

		return false;
	}

	return hasDependentStates(STATES_STRINGS[_pda.top()->name()]);
}


/** Checks if the PDA has the dependent states stored.

This is a recursive method that checks all the dependent states of substates as
well.

\returns
	`true` if all dependent states are stored, `false` otherwise.
*/
bool PushdownAutomaton::hasDependentStates(STATES state) const {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Looking for dependencies of %d", state);
	for (STATES dependency: _states.at(state)->dependentStates()) {
		State* p_dependency = nullptr;

		try {
			p_dependency = _states.at(dependency);
		}
		catch (std::out_of_range& error) {
			SDL_LogWarn(
				SDL_LOG_CATEGORY_APPLICATION,
				"PushdownAutomata :: '%s' state is missing its dependencies",
				_states.at(state)->name().c_str()
			);

			return false;
		}

		if (p_dependency == nullptr) {
			SDL_LogWarn(
				SDL_LOG_CATEGORY_APPLICATION,
				"PushdownAutomata :: '%s' state is missing its dependencies",
				_states.at(state)->name().c_str()
			);

			return false;
		}

		if (!hasDependentStates(STATES_STRINGS[p_dependency->name()])) {
			return false;
		}
	}

	return true;
}


/** Returns the current state.
*/
State* PushdownAutomaton::currentState() {
	return _pda.top();
}


/** Notifies the state machine of an event.

One of the core methods of the top-down automata (the other being `update()`).
The `State` is notified that there is has been an event and performs actions
accordingly. If the returned pointer is not null, this indicates that the entity
is entering a different state. Otherwise, we check if the current state is
finished. Some states loop infinitely and will be be finished.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.
*/
void PushdownAutomaton::notify(
	const EVENT event, 
	const ID sender, 
	Stage& stage
) {
	State* state = _pda.top()->notify(event, sender, stage);
	pushdown(state, stage);
}


/** Updates the state machine.

One of the core methods of the top-down automata (the other being `notify()`).
The `State` continues its action, as if there was no input given. If the
returned pointer is not null, this indicates that the entity is entering a 
different state. Otherwise, we check if the current state is finished.  Some 
states loop infinitely and will never be finished.

\param stage
	the currently loaded stage.
 */
void PushdownAutomaton::update(Stage& stage) {
	State* state = _pda.top()->update(stage);
	pushdown(state, stage);
}


/** Completes the pushdown operation by removing finished states and pushing
on the new state.

\param state
	a pointer to the new state.
\param stage
	the currently loaded stage.
*/
void PushdownAutomaton::pushdown(State* state, Stage& stage) {
	if (state != NULL) {
		removeFinishedStates(stage);
		_pda.push(state);
		_pda.top()->enter(stage);
	}
	else if (_pda.top()->isFinished()) {
		removeFinishedStates(stage);
		_pda.top()->enter(stage);
	}
}


/** Remove all finished states from the top-down automata.

\param stage
	the currently loaded stage.
*/
void PushdownAutomaton::removeFinishedStates(Stage& stage) {
	while (_pda.size() > 1) {
		if (!_pda.top()->isFinished()) {
			break;
		}

		_pda.top()->exit(stage);
		_pda.pop();
	}
}
