/** hitbox_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `Hitbox` component.
*/


#include <algorithm>

#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/hitbox.hpp"
#include "../src/json.hpp"


TEST_CASE("Hitbox component tests", "[hitbox][component]") {
	JSON data = R"(
		{
			"hitbox": {
				"idle0": {
					"sprite": {
						"x": 0,
						"y": 1,
						"w": 2,
						"h": 3
					},
					"boxes": {
						"head": {
							"x": 4,
							"y": 5,
							"w": 6,
							"h": 7
						},
						"body": {
							"x": 8,
							"y": 9,
							"w": 10,
							"h": 11
						}
					}
				},
				"walk0": {
					"sprite": {
						"x": 12,
						"y": 13,
						"w": 14,
						"h": 15
					},
					"boxes": {
						"head": {
							"x": 16,
							"y": 17,
							"w": 18,
							"h": 19
						},
						"body": {
							"x": 20,
							"y": 21,
							"w": 22,
							"h": 23
						}
					}
				}
			},
			"bad_hitbox": {
				"foo": "bar"
			}
		}
	)"_json;

	SECTION("Hitbox component default constructor") {
		Hitbox hitbox = data.at("hitbox").get<Hitbox>();

		JSON idle_data = data.at("hitbox").at("idle0");

		SDL_Rect idle_key = idle_data.at("sprite").get<SDL_Rect>();
		SDL_Rect idle_head = idle_data.at("boxes").at("head").get<SDL_Rect>();
		SDL_Rect idle_body = idle_data.at("boxes").at("body").get<SDL_Rect>();
		int idle_highpoint = std::max(
			idle_head.y + idle_head.h,
			idle_body.y + idle_body.h
		);

		for (SDL_Rect& rect: hitbox.hitbox(idle_key)) {
			REQUIRE((rect == idle_head || rect == idle_body));
		}

		REQUIRE(hitbox.highpoint(idle_key) == idle_highpoint);

		JSON walk_data = data.at("hitbox").at("walk0");

		SDL_Rect walk_key = walk_data.at("sprite").get<SDL_Rect>();
		SDL_Rect walk_head = walk_data.at("boxes").at("head").get<SDL_Rect>();
		SDL_Rect walk_body = walk_data.at("boxes").at("body").get<SDL_Rect>();
		int walk_highpoint = std::max(
			walk_head.y + walk_head.h,
			walk_body.y + walk_body.h
		);

		for (SDL_Rect& rect: hitbox.hitbox(walk_key)) {
			REQUIRE((rect == walk_head || rect == walk_body));
		}

		REQUIRE(hitbox.highpoint(walk_key) == walk_highpoint);

		REQUIRE(hitbox.unionbox().x == idle_head.x);
		REQUIRE(hitbox.unionbox().y == idle_head.y);
		REQUIRE(hitbox.unionbox().w == walk_body.x + walk_body.w - idle_head.x);
		REQUIRE(hitbox.unionbox().h == walk_body.y + walk_body.h - idle_head.y);
	}

	SECTION("Hitbox component bad key") {
		REQUIRE_THROWS(data.at("bad_hitbox").get<Hitbox>());
	}
}
