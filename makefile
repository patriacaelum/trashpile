# makefile
#
#      ||          __  __  ___  ____   ___  ____  _____ ____
#      --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
# -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
# -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
#      --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
#      ||
#
# The script that uses GNU Make to automate compilation.
#
# The main commands are:
#
#   - make all
#   - make build
#   - make tests
#   - make linux
#   - make windows
#   - make linux_test
#   - make windows_test
#   - make dox
#   - make reset
#   - make clean
#
# The syntax for each make function is:
#
#     target: prerequisites
#         script
#
# where any change to the prerequisites requires recomilation of the target.
# Other shorthands:
#
#   - $@ is the target filename
#   - $< is the name of the first prerequisite.
#   - $^ are the filenames of all the prerequisites separated by spaces,
#     disregarding duplicates
#   - % is a pattern matching filename
#
# To run with debugger, use
#
#    valgrind --leak-check=yes ./trashpile.exe
#
# TODO: add -Wextra, -Og or -Ofast, -pthread


# Directories
builds_dir = ./builds
linux_include_dir = ./include/linux
windows_include_dir = ./include/x86_64-w64-mingw32
linux_dir = $(builds_dir)/linux
windows_dir = $(builds_dir)/windows
linux_test_dir = $(builds_dir)/linux_test
windows_test_dir = $(builds_dir)/windows_test


# Output files
linux_output = ./bin/linux/trashpile.sh
windows_output = ./bin/windows/trashpile.exe


# Build files
component_files = $(wildcard ./src/components/*.cpp) 
pathfinding_files = $(wildcard ./src/pathfinding/*.cpp)
postal_service_files = $(wildcard ./src/postal_service/*.cpp)
state_files = $(wildcard ./src/states/*.cpp) 
source_files = $(wildcard ./src/*.cpp)

linux_components = $(component_files:./src/components/%.cpp=./builds/linux/%.o)
linux_pathfinding = $(pathfinding_files:./src/pathfinding/%.cpp=./builds/linux/%.o)
linux_postal_service = $(postal_service_files:./src/postal_service/%.cpp=./builds/linux/%.o)
linux_states = $(state_files:./src/states/%.cpp=./builds/linux/%.o)
linux_sources = $(source_files:./src/%.cpp=./builds/linux/%.o)

windows_components = $(component_files:./src/components/%.cpp=./builds/windows/%.o)
windows_pathfinding = $(pathfinding_files:./src/pathfinding/%.cpp=./builds/windows/%.o)
windows_postal_service = $(postal_service_files:./src/postal_service/%.cpp=./builds/windows/%.o)
windows_states = $(state_files:./src/states/%.cpp=./builds/windows/%.o)
windows_sources = $(source_files:./src/%.cpp=./builds/windows/%.o)


# Test files
log_file = ./logs/test_log.txt
test_files = $(wildcard ./tests/*.cpp)
linux_tests = $(test_files:./tests/%.cpp=./builds/linux_test/%.o)
windows_tests = $(test_files:./tests/%.cpp=./builds/windows_test/%.o)


# Test output files
linux_test_output = ./bin/linux_test/test.sh
windows_test_output = ./bin/windows_test/test.exe


# Flags
cpp_flags = -std=c++17 -w -Wall -Werror -MD -MP
libs = -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf
catch2_flags = --abort --durations yes --use-colour yes


##############
# MAIN RULES #
##############

all:
	make tests
	make build
	make dox
	@tput setaf 2
	@echo
	@echo "-------------------------------------------"
	@echo "FINISHED MAKING ALL TESTS, BUILDS, AND DOCS"
	@echo "-------------------------------------------"
	@echo
	@tput setaf 7

build:
	make clean
	make linux
	make windows
	@tput setaf 2
	@echo
	@echo "--------------------------"
	@echo "FINISHED MAKING ALL BUILDS"
	@echo "--------------------------"
	@echo
	@tput setaf 7


clean:
	rm -f $(linux_output) $(windows_output) $(linux_test_output) $(windows_test_output)
	rm -rf $(builds_dir)
	@tput setaf 2
	@echo
	@echo "--------------------"
	@echo "FINISHED CLEANING UP"
	@echo "--------------------"
	@echo
	@tput setaf 7


dox:
	rm -rf ./docs/html/
	rm -rf ./docs/latex/
	doxygen ./docs/Doxyfile
	@tput setaf 2
	@echo
	@echo "--------------------"
	@echo "FINISHED MAKING DOCS"
	@echo "--------------------"
	@echo
	@tput setaf 7


linux: $(builds_dir) $(linux_output)
	@tput setaf 2
	@echo
	@echo "---------------------------"
	@echo "FINISHED MAKING LINUX BUILD"
	@echo "---------------------------"
	@echo
	@tput setaf 7


linux_test: $(builds_dir) $(linux_test_output)
	$(linux_test_output) $(catch2_flags)
	@tput setaf 2
	@echo
	@echo "---------------------------"
	@echo "FINISHED MAKING LINUX TESTS"
	@echo "---------------------------"
	@echo
	@tput setaf 7


tests:
	make clean
	make linux_test
	make windows_test
	@tput setaf 2
	@echo
	@echo "-------------------------"
	@echo "FINISHED MAKING ALL TESTS"
	@echo "-------------------------"
	@echo
	@tput setaf 7


windows: $(builds_dir) $(windows_output)
	@tput setaf 2
	@echo
	@echo "------------------------------"
	@echo "FINISHED MAKING WINDOWS BUILD"
	@echo "------------------------------"
	@echo
	@tput setaf 7


windows_test: $(builds_dir) $(windows_test_output)
	$(windows_test_output) $(catch2_flags) | cat
	@tput setaf 2
	@echo
	@echo "-----------------------------"
	@echo "FINISHED MAKING WINDOWS TEST"
	@echo "-----------------------------"
	@echo
	@tput setaf 7


################
# HELPER RULES #
################

builds_env:
	@tput setaf 2
	@echo
	@echo "--------------------------------"
	@echo "BUILD ENVIRONMENT DOES NOT EXIST"
	@echo "--------------------------------"
	@echo
	@echo "--------------------------"
	@echo "CREATING BUILD ENVIRONMENT"
	@echo "--------------------------"
	@echo
	@tput setaf 7
	mkdir $(builds_dir)
	mkdir $(linux_dir)
	mkdir $(windows_dir)
	mkdir $(linux_test_dir)
	mkdir $(windows_test_dir)
	@tput setaf 2
	@echo
	@echo "----------------------------------"
	@echo "FINISHED MAKING BUILD ENVIRONMENT"
	@echo "----------------------------------"
	@echo
	@tput setaf 7


# Creates builds environment if build directory does not exist
$(builds_dir):
	make builds_env


#################
# LINKING RULES #
#################

$(linux_output): $(linux_components) $(linux_pathfinding) $(linux_postal_service) $(linux_states) $(linux_sources)
	@tput setaf 2
	@echo
	@echo "----------------------------"
	@echo "FINISHED COMPILING FOR LINUX"
	@echo "----------------------------"
	@echo
	@tput setaf 7
	g++ $^ $(libs) -o $@


$(windows_output): $(windows_components) $(windows_pathfinding) $(windows_postal_service) $(windows_states) $(windows_sources)
	@tput setaf 2
	@echo
	@echo "------------------------------"
	@echo "FINISHED COMPILING FOR WINDOWS"
	@echo "------------------------------"
	@echo
	@tput setaf 7
	x86_64-w64-mingw32-g++ $^ -L$(windows_include_dir)/lib $(libs) -std=c++17 -static-libgcc -static-libstdc++ -mwindows -o $@


$(linux_test_output): $(linux_components) $(linux_pathfinding) $(linux_postal_service) $(linux_states) $(linux_sources) $(linux_tests)
	@tput setaf 2
	@echo
	@echo "----------------------------------"
	@echo "FINISHED COMPILING TESTS FOR LINUX"
	@echo "----------------------------------"
	@echo
	@tput setaf 7
	rm -f $(linux_dir)/main.o
	g++ $(linux_dir)/*.o $(linux_test_dir)/*.o $(libs) -o $@
	make $(linux_dir)/main.o


$(windows_test_output): $(windows_components) $(windows_pathfinding) $(windows_postal_service) $(windows_states) $(windows_sources) $(windows_tests)
	@tput setaf 2
	@echo
	@echo "------------------------------------"
	@echo "FINISHED COMPILING TESTS FOR WINDOWS"
	@echo "------------------------------------"
	@echo
	@tput setaf 7
	rm -f $(windows_dir)/main.o
	x86_64-w64-mingw32-g++ $(windows_dir)/*.o $(windows_test_dir)/*.o -L$(windows_include_dir)/lib $(libs) -std=c++17 -static-libgcc -static-libstdc++ -mwindows -o $@
	make $(windows_dir)/main.o


#####################
# COMPILATION RULES #
#####################

./builds/linux/%.o: ./src/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $< -o $@

	
./builds/linux/%.o: ./src/components/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $< -o $@


./builds/linux/%.o: ./src/pathfinding/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $< -o $@


./builds/linux/%.o: ./src/postal_service/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $< -o $@


./builds/linux/%.o: ./src/states/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $< -o $@


./builds/windows/%.o: ./src/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $< -o $@


./builds/windows/%.o: ./src/components/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $< -o $@


./builds/windows/%.o: ./src/pathfinding/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $< -o $@


./builds/windows/%.o: ./src/postal_service/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $< -o $@


./builds/windows/%.o: ./src/states/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $< -o $@


./builds/linux_test/%.o: ./tests/%.cpp
	g++ $(cpp_flags) -I$(linux_include_dir) -c $^ -o $@


./builds/windows_test/%.o: ./tests/%.cpp
	x86_64-w64-mingw32-g++ $(cpp_flags) -I$(windows_include_dir)/include -c $^ -o $@
	