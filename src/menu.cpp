#include "menu.hpp"

#include "stage.hpp"


Menu::Menu() : 
    _is_running(false), 
    _window_width(0),
    _window_height(0),
    _current(nullptr),
    _ui_base(nullptr),
    _ui_success(nullptr),
    _ui_failure(nullptr),
    _ui_map(nullptr)
{
}


Menu::~Menu() {
    unload();
}


/** Loads the UI.

The `unload()` method should be called before loading.
*/
void Menu::load(SDL_Renderer* renderer, const std::string filename) {
    const JSON data = loadJSONFile(filename);

    const std::string ui_base_filename = data.at("base");
    const std::string ui_success_filename = data.at("success");
    const std::string ui_failure_filename = data.at("failure");
    const std::string ui_map_filename = data.at("map");

    _ui_base = loadTexture(renderer, ui_base_filename);
    _ui_success = loadTexture(renderer, ui_success_filename);
    _ui_failure = loadTexture(renderer, ui_failure_filename);
    _ui_map = loadTexture(renderer, ui_map_filename);

    SDL_LogInfo(
        SDL_LOG_CATEGORY_RENDER,
        "Menu :: successfully loaded UI base from '%s'",
        ui_base_filename.c_str()
    );
}


void Menu::unload() {
    _current = nullptr;

    if (_ui_base != nullptr) {
        SDL_DestroyTexture(_ui_base);

        _ui_base = nullptr;

        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: successfully destroyed ui base"
        );
    }

    if (_ui_success != nullptr) {
        SDL_DestroyTexture(_ui_success);

        _ui_success = nullptr;

        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: successfully destroyed ui success"
        );
    }

    if (_ui_failure != nullptr) {
        SDL_DestroyTexture(_ui_failure);

        _ui_failure = nullptr;

        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: successfully destroyed ui failure"
        );
    }

    if (_ui_map != nullptr) {
        SDL_DestroyTexture(_ui_map);

        _ui_map = nullptr;

        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: successfully destroyed ui map"
        );
    }
}


void Menu::start(Stage& stage) {
    stage.mailbox.subscribe(this, EVENT::PRESS_A_BUTTON);

    _is_running = true;

    SDL_LogDebug(
        SDL_LOG_CATEGORY_SYSTEM,
        "Menu :: successfully started menu"
    );
}


void Menu::stop(Stage& stage) {
    stage.mailbox.unsubscribe(this, EVENT::PRESS_A_BUTTON);

    _is_running = false;
    _current = nullptr;

    SDL_LogDebug(
        SDL_LOG_CATEGORY_SYSTEM,
        "Menu :: successfully stopped menu"
    );
}


bool Menu::isRunning() const {
    return _is_running;
}


void Menu::notify(const EVENT event, const ID sender, Stage& stage) {
    switch (event) {
    case EVENT::PRESS_A_BUTTON:
        if (_is_running) {
            stop(stage);
        }

        break;

    case EVENT::PRESS_START_BUTTON:
        if (_is_running) {
            stop(stage);
        }
        else {
            _current = _ui_base;
            start(stage);
        }

        break;

    case EVENT::PRESS_SELECT_BUTTON:
        if (!_is_running) {
            _current = _ui_map;
            start(stage);
        }

        break;

    case EVENT::UI_SUCCESS:
        if (!_is_running) {
            _current = _ui_success;
            start(stage);
        }

        break;

    case EVENT::UI_FAILURE:
        if (!_is_running) {
            _current = _ui_failure;
            start(stage);
        }

        break;
    }
}


void Menu::update(Stage& stage) {
    // Update graphics
}


void Menu::render(SDL_Renderer* renderer) {
    SDL_RenderCopy(renderer, _current, NULL, NULL);
}


/** Loads an image as an `SDL_Texture` and returns its pointer.

\param filename     the path to the image file.

\returns            a pointer to the `SDL_Texture`.
*/
SDL_Texture* Menu::loadTexture(SDL_Renderer* renderer, const std::string filename) {
    SDL_Surface* surface = IMG_Load(filename.c_str());

    if (surface == nullptr) {
        SDL_LogError(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: failed to load SDL_Surface from '%s': %s",
            filename.c_str(),
            IMG_GetError()
        );

        throw std::runtime_error(
            "Menu :: failed to load SDL_Surface"
        );
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

    SDL_FreeSurface(surface);

    if (texture == nullptr) {
        SDL_LogError(
            SDL_LOG_CATEGORY_RENDER,
            "Menu :: failed to load SDL_Texture from '%s': %s",
            filename.c_str(),
            SDL_GetError()
        );

        throw std::runtime_error(
            "Menu :: failed to load SDL_Texture"
        );
    }

    return texture;
}
