#include "guard.hpp"


Guard::Guard(const ID id, const JSON& data, Stage& stage)
:   State(
        "guard",
        true,
        {STATES::ALERT, STATES::LISTEN, STATES::SPEAK},
        id,
        data
    )
{
    stage.mailbox.subscribe(&stage.scene.pda(id), EVENT::NOISE);
}


/** The guard is notified by a noise event, and will be alerted if the noise
event is within the guard's hearing range.

\param event
    the `EVENT` that the guard is subscribed to. The guard is subscribed to
    `NOISE` events.
\param sender
    the `ID` of the sender entity.
\param stage
    the currently loaded stage.

\returns
    the `Alert` state if the `NOISE` event was within the guard's hearing
    range, otherwise returns a `nullptr`.
*/
State* Guard::notify(const EVENT event, const ID sender, Stage& stage) {
    switch (event) {
    case EVENT::NOISE:
        if (in_hearing_range(sender, stage)) {
            stage.scene.pda(id)[STATES::ALERT]->setReferenceId(sender);

            return stage.scene.pda(id)[STATES::ALERT];
        }

        break;
    
    case EVENT::SPEAK: {
        Connector connector = stage.scene.connector(id);

        if (connector.isConnected() && sender == connector.connection()) {
            return stage.scene.pda(id).at(STATES::SPEAK);
        }

        break;
    }

    case EVENT::LISTEN: {
        Connector connector = stage.scene.connector(id);

        if (connector.isConnected() && sender == connector.connection()) {
            return stage.scene.pda(id).at(STATES::LISTEN);
        }

        break;
    }
    }

    return nullptr;
}


/** Determines if the position of an entity is within the hearing range of this
entity.

The hearing range is a square expanded from the sprite of this entity in all
four cardinal directions by the hearing range.

\param sender       the entity id of the noise event.
\param stage        the currently loaded stage.

\returns            `true` if the sender is within hearing range, `false`
                    otherwise.
*/
bool Guard::in_hearing_range(const ID sender, Stage& stage) {
    const Position& position = stage.scene.position(id);
    const SDL_Rect& sprite = stage.scene.sprite(id).sprite;
    const int hearing_range = stage.scene.property(id).at("hearing_range");

    const SDL_Rect range = {
        position.x - hearing_range,
        position.y - hearing_range,
        sprite.w + 2 * hearing_range,
        sprite.h + 2 * hearing_range
    };

    const Position& s_position = stage.scene.position(sender);
    const SDL_Rect& s_sprite = stage.scene.sprite(sender).sprite;

    const SDL_Rect target = {
        s_position.x,
        s_position.y,
        s_sprite.w,
        s_sprite.h
    };

    if (SDL_HasIntersection(&range, &target) == SDL_TRUE) {
        return true;
    }

    return false;
}
