/** globals.hpp

\verbatim
       ||          __  __  ___  ____   ___  ____  _____ ____
       --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
  -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
  -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
       --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
       ||
\endverbatim

Dictionaries of global variables. This includes input commands, audio
files, and image files.
*/


#ifndef SRC_GLOBALS_HPP_
#define SRC_GLOBALS_HPP_


#include <string>
#include <unordered_map>
#include <utility>

#include <SDL2/SDL.h>


namespace PROPERTIES {
	/// The maximum number of entities that can be created in a `Scene`.
	const int MAX_ENTITIES = 50;
}


enum class DIRECTION {
	NORTH,
	NORTHEAST,
	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST,
	NORTHWEST
};


enum class EVENT {
	INACTION,
    AUDIO,

    /* Button presses */
	// Face buttons
    PRESS_A_BUTTON,
    PRESS_B_BUTTON,
	PRESS_X_BUTTON,
	PRESS_Y_BUTTON,
	// Directional buttons
    PRESS_DOWN_BUTTON,
    PRESS_LEFT_BUTTON,
    PRESS_RIGHT_BUTTON,
    PRESS_UP_BUTTON,
	// Bumpers and triggers
	PRESS_RIGHT_BUMPER,
	PRESS_LEFT_BUMPER,
	PRESS_RIGHT_TRIGGER,
	PRESS_LEFT_TRIGGER,
	// Menu buttons
	PRESS_START_BUTTON,
	PRESS_SELECT_BUTTON,

    /* Button releases */
	// Face buttons
    RELEASE_A_BUTTON,
    RELEASE_B_BUTTON,
	RELEASE_X_BUTTON,
	RELEASE_Y_BUTTON,
	// Directional buttons
    RELEASE_DOWN_BUTTON,
    RELEASE_LEFT_BUTTON,
    RELEASE_RIGHT_BUTTON,
    RELEASE_UP_BUTTON,
	// Bumpers and triggers
	RELEASE_RIGHT_BUMPER,
	RELEASE_LEFT_BUMPER,
	RELEASE_RIGHT_TRIGGER,
	RELEASE_LEFT_TRIGGER,
	// Menu buttons
	RELEASE_START_BUTTON,

	// Menu events
	UI_SUCCESS,
	UI_FAILURE,

    // Internal events
    DIALOGUE_START,
    DIALOGUE_STOP,
	NOISE,
	QUIT,

	// State change events
    CONNECT,
    DISCONNECT,
	INITIATE,
	TERMINATE,
	SPEAK,
	LISTEN,
    ROOM_CHANGE,

    // Render events
    RENDER
};


enum class STATES {
	// Static objects
	DORMANT,
	ROOM,

	// Character-like objects
	IDLE,
	WALK,
	SNEAK,
	ROLL,
	ATTACK_SHORT,
	ATTACK_LONG,
	LISTEN,
	SPEAK,

	GUARD,
	ALERT,
	GOTO,
	PURSUE,

	// Doors
	DOOR_OPEN,
	DOOR_CLOSE,

	// Grappling hook
	GRAPPLING_HOOK_INACTIVE,
	GRAPPLING_HOOK_SHORT,
	GRAPPLING_HOOK_LONG,

	// Noisemakers
	NOISEMAKER_OFF,
	NOISEMAKER_ON
};


enum class KNOWLEDGE {
	FULL,
	PARTIAL,
	EMPTY
};


/// Map of background music files.
extern std::unordered_map<std::string, std::string> BGM;


/// Map of strings that map to the `DIRECTION` enum.
extern std::unordered_map<std::string, DIRECTION> DIRECTION_STRINGS;


/// Map TTF font files.
extern std::unordered_map<std::string, std::pair<std::string, int>> FONTS;


/// Map of image files.
extern std::unordered_map<std::string, std::string> IMG;


/// Map of JSON scene files.
extern std::unordered_map<std::string, std::string> SCENES;


/// Map of strings that map to the `STATES` enum.
extern std::unordered_map<std::string, STATES> STATES_STRINGS;


#endif /* SRC_GLOBALS_HPP_ */
