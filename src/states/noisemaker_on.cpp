#include "noisemaker_on.hpp"


NoisemakerOn::NoisemakerOn(const ID id, const JSON& data, Stage& stage)
:   State("noisemaker_on", false, {}, id, data),
    _n_repetitions(0),
    _max_repetitions(data.value("max_repetitions", 1))
{}


void NoisemakerOn::enter(Stage& stage) {
    State::enter(stage);

    _n_repetitions = 0;
}


/** Updates the noisemaker while it is on.

The noisemaker will emit a sound for the specified number of repetitions, then
finishes.
*/
State* NoisemakerOn::update(Stage& stage) {
    if (stage.scene.sprite(id).sprite.x == 0 && _hframes == _max) {
        stage.mailbox.post(EVENT::NOISE, id);

        _n_repetitions += 1;
    }
    else if (_n_repetitions == _max_repetitions && _hframes == _max) {
        _finished = true;
    }

    return State::update(stage);
}
