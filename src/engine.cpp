#include "engine.hpp"


/** Initializes the `Renderer`, `Scene`, and `Stage`.

The `InputManager` takes the player inputs and translates them to global input
values. The `Renderer` creates the window where everything is displayed and
controls how they are displayed. The `Stage` stores all the entities in the
`Scene` and handles how they are updated.
*/
Engine::Engine():
	_inputman(),
	_renderer(),
	_stage(),
	_high_refresh(false),
	_skip(false)
{
	load("../../scenes/pilot.json");

	if (getRefreshRate() > 60) {
		_high_refresh = true;
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Engine :: successfully initialized engine"
	);
}


/** Updates everything for a single frame.

\returns	`true` if everything was successfully updated, `false` if a quit
			input was detected.
*/
bool Engine::run() {
	if (_skip) {
		// Skip computing this next frame
		_skip = false;
	}
	else {
		EVENT event = _inputman.getInput();

		if (event == EVENT::QUIT) {
			SDL_LogWarn(
				SDL_LOG_CATEGORY_INPUT,
				"Engine :: quit input received"
			);

			return false;
		}

		if (event != EVENT::INACTION) {
			_stage.mailbox.post(event);
		}

		_stage.update();

		if (_high_refresh) {
			// Assuming high-refresh monitors are 120Hz and we want to render at
			// 60Hz
			_skip = true;
		}
	}

	_renderer.render(_stage);

	return true;
}


/** Loads the entities into the `Scene` and registers them on the `Stage`.

\param filename		the JSON file where the scene data is stored.
*/
void Engine::load(const std::string filename) {
	_stage.load(filename, _renderer.renderer);
}


/** Gets the refresh rate of the current display monitor. This will be the main
monitor if there are multiple monitors.

\returns	the refresh rate of the main monitor.
*/
int Engine::getRefreshRate() {
	int index = 0;
	SDL_DisplayMode display;

	int success = SDL_GetCurrentDisplayMode(index, &display);

	if (success != 0) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Stage :: failed to get display mode for index=%d, assuming 60fps",
			index
		);

		return 60;
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_VIDEO,
		"Stage :: display index=%d has resolution %dx%d, refresh rate=%dfps",
		index,
		display.w,
		display.h,
		display.refresh_rate
	);

	return display.refresh_rate;
}
