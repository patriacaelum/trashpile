#include "idle.hpp"


Idle::Idle(const ID id, const JSON& data, Stage& stage)
:	State(
		"idle",
		true,
		{STATES::ATTACK_SHORT, STATES::LISTEN, STATES::SPEAK, STATES::WALK},
		id,
		data
	)
{}


/** The `Velocity` of the idle entity is set to zero.

\param stage
	the currently loaded stage.
*/
void Idle::enter(Stage& stage) {
	Velocity& velocity = stage.scene.velocity(id);

	velocity.dx = 0;
	velocity.dy = 0;

	State::enter(stage);
}


/** Notifies the state of an event.

The following events can be posted:

- a `DIALOGUE_START` event is posted if there is a `PRESS_A_BUTTON` event and a
  dialogue is found with a nearby entity.

The following states can be returned:

- a `Walk` state is returned given a `PRESS_DOWN_BUTTON`, `PRESS_LEFT_BUTTON`,
  `PRESS_RIGHT_BUTTON`, or `PRESS_UP_BUTTON` event.
- a `Speak` state is returned given a `PRESS_A_BUTTON` event only if a 
  connection to another entity and dialogue is found with the other entity.
- a `ShortAttack` state is returned given a `PRESS_B_BUTTON` event.
- a `Listen` state is returned given a `CONNECT` event.

\param event
	the event trigger.
\param sender
	the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a `State` pointer.
*/
State* Idle::notify(const EVENT event, const ID sender, Stage& stage) {
	switch (event) {
	case EVENT::PRESS_A_BUTTON: {
		ID connection = stage.findCollision(id, 2);

		if (connection == -1) {
			break;
		}

		// Pickup entity if possible
		const int can_pickup = stage.scene.property(connection).at("pickup");

		if (can_pickup != -1) {
			stage.scene.inventory(id).add(connection);
			stage.hide(connection);

			break;
		}

		// Ignore dormant entities
		const std::string state_name = stage.scene.pda(connection).currentState()->name();

		if (state_name == "dormant") {
			break;
		}

		// Establish connection
		try {
			stage.connect(id, connection);
			stage.scene.pda(connection).notify(EVENT::CONNECT, id, stage);
		}
		catch (std::logic_error& error) {
			SDL_LogError(
				SDL_LOG_CATEGORY_APPLICATION,
				"State::Idle :: failed to connect id=%d with id=%d",
				id,
				connection
			);

			throw std::runtime_error(
				"State::Idle :: failed to connect entities"
			);
		}

		// Retrieve dialogue, otherwise listen
		try {
			stage.scene.dialogue(id);
		}
		catch (std::out_of_range& id_error) {
			try {
				stage.scene.dialogue(connection);
			}
			catch (std::out_of_range& connection_error) {
				return stage.scene.pda(id).at(STATES::LISTEN);
			}
		}

		stage.mailbox.post(EVENT::DIALOGUE_START, id);

		return stage.scene.pda(id).at(STATES::SPEAK);
	}

	case EVENT::PRESS_X_BUTTON:
		return stage.scene.pda(id).at(STATES::ATTACK_SHORT);

	case EVENT::PRESS_Y_BUTTON: {
		Inventory& inventory = stage.scene.inventory(id);

		if (inventory.begin() == inventory.end()) {
			break;
		}

		// Drop noisemaker and start effect
		const ID noisemaker_id = *(inventory.begin());
		Position& noisemaker_position = stage.scene.position(noisemaker_id);

		noisemaker_position = stage.scene.position(id);
		stage.scene.pda(noisemaker_id).notify(EVENT::INITIATE, id, stage);
		inventory.remove(noisemaker_id);
		stage.show(noisemaker_id);

		break;
	}

	case EVENT::PRESS_DOWN_BUTTON:
		stage.scene.position(id).d = DIRECTION::SOUTH;

		return stage.scene.pda(id).at(STATES::WALK);

	case EVENT::PRESS_LEFT_BUTTON:
		stage.scene.position(id).d = DIRECTION::WEST;

		return stage.scene.pda(id).at(STATES::WALK);

	case EVENT::PRESS_RIGHT_BUTTON:
		stage.scene.position(id).d = DIRECTION::EAST;

		return stage.scene.pda(id).at(STATES::WALK);

	case EVENT::PRESS_UP_BUTTON:
		stage.scene.position(id).d = DIRECTION::NORTH;

		return stage.scene.pda(id).at(STATES::WALK);

	case EVENT::CONNECT: {
		Connector connector = stage.scene.connector(id);

		if (connector.isConnected() && sender == connector.connection()) {
			return stage.scene.pda(id).at(STATES::LISTEN);
		}

		break;
	}

	case EVENT::SPEAK: {
		Connector connector = stage.scene.connector(id);

		if (connector.isConnected() && sender == connector.connection()) {
			return stage.scene.pda(id).at(STATES::SPEAK);
		}

		break;
	}

	case EVENT::LISTEN: {
		Connector connector = stage.scene.connector(id);

		if (connector.isConnected() && sender == connector.connection()) {
			return stage.scene.pda(id).at(STATES::LISTEN);
		}

		break;
	}
	}

	return update(stage);
}
