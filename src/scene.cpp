#include "scene.hpp"


/** Creates an empty scene.
*/
Scene::Scene() {
	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: successfully initialized empty scene"
	);
}


/** Deletes all objects in the current scene.
*/
Scene::~Scene() {
	unload();

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: successfully destroyed scene"
	);
}


/** Gives a list of the IDs currently in use.
*/
std::vector<ID> Scene::ids() const {
	std::vector<ID> ids;

	for (ID id = 0; id < PROPERTIES::MAX_ENTITIES; ++id) {
		if (_idman.test(id)) {
			ids.push_back(id);
		}
	}

	return ids;
}


/** Gives the name of the entity with the given ID.
*/
std::string Scene::name(const ID id) const {
	return _idman.name(id);
}


/** Loads the specified scene into memory. If another scene is still in memory,
it will be unloaded.

The `Dialogue` and `Knowledge` components need to be loaded separately after all
the entities have been created so that the correct IDs are mapped.

\param filename		the JSON file specifying the entities in the scene.
\param renderer		the `SDL_Renderer` is required to load sprites.
\param stage		the currently loaded stage.
*/
void Scene::load(const std::string filename, SDL_Renderer* renderer, Stage& stage) {
	const JSON data = loadJSONFile(filename);

	// Load essential components
	for (const auto& edata: data.items()) {
		loadEntity(edata, renderer, stage);
	}

	// Load components that require IDs of other entities
	for (const auto& edata: data.items()) {
		const std::string name = edata.key();
		const ID id = _idman.id(name);

		loadDialogue(id, edata, renderer);
		loadInventory(id, edata);
		loadKnowledge(id, edata);
	}

	offsetPositions();

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: successfully created scene '%s'",
		filename.c_str()
	);
}


/** Resets the `IDManager` and clears all the `ComponentManager` instances.
*/
void Scene::unload() {
	_idman.reset();

	_conman.clear();
	_hitman.clear();
	_pathman.clear();
	_posman.clear();
	_velman.clear();

	_portman.clear();
	_spriteman.clear();

	_subman.clear();
	_superman.clear();

	_pdaman.clear();

	_dialman.clear();

	_map_size = {0, 0, 0, 0};

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: successfully unloaded current scene"
	);
}


/** Return the `Connector` component.

\param id		the entity ID.

\returns		a `Connector` reference.
*/
Connector& Scene::connector(const ID& id) {
	try {
		return _conman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find connector component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find the connector"
		);
	}
}


/** Return the `Dialogue` component.

\param id		the entity ID.

\returns 		a `Dialogue` reference.
*/
Dialogue& Scene::dialogue(const ID& id) {
	try {
		return _dialman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find dialogue component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find dialogue component"
		);
	}
}


/** Return the `Hitbox` component.

\param id		the entity ID.

\returns		a `Hitbox` reference.
*/
Hitbox& Scene::hitbox(const ID& id) {
	try {
		return _hitman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find hitbox component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find hitbox component"
		);
	}
}


/** Return the `Inventory` of the entity.

\param id		the entity ID.

\returns		a `Inventory` component reference.
*/
Inventory& Scene::inventory(const ID& id) {
	try {
		return _inventman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find inventory component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find inventory component"
		);
	}
}


/** Return the `Knowledge` of the entity.

\param id		the entity ID.

\returns		a `Knowledge` reference.
*/
Knowledge& Scene::knowledge(const ID& id) {
	try {
		return _knowman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find knowledge component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find knowledge component"
		);
	}
}


/** Return the `Path` component.

\param id		the entity ID.

\returns		a `Path` reference.
*/
Path& Scene::path(const ID& id) {
	try {
		return _pathman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find path component for id=%d, creating one now",
			id
		);

		_pathman.add(id, {});

		return _pathman.get(id);
	}
}


/** Return the `Position` component.

\param id		the entity ID.

\returns		a `Position` reference.
*/
Position& Scene::position(const ID& id) {
	try {
		return _posman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find position component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find position component"
		);
	}
}


/** Return a `Sprite` component containing the portraits.

\param id		the entity ID.

\returns		a `Sprite` reference.
*/
Sprite& Scene::portrait(const ID& id) {
	try {
		return _portman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find portrait component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find portrait component"
		);
	}
}


/** Return the position of the portrait relative to the camera.

\param id		the entity ID.

\returns		a `SDL_Rect` reference.
*/
SDL_Rect& Scene::portraitPosition(const ID& id) {
	try {
		return _portposman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find portrait position for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find portrait position component"
		);
	}
}


/** Return the properties map of the entity.

\param id		the entity ID.

\returns		a `Property` reference.
*/
Property& Scene::property(const ID& id) {
	try {
		return _propman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find property for id=%d: %s",
			id,
			error.what()
		);
	}

	throw std::out_of_range(
		"Scene :: failed to find property component"
	);
}


/** Return the `Sprite` component containint the spritesheet.

\param id		the entity ID.

\returns		a `Sprite` reference.
*/
Sprite& Scene::sprite(const ID& id) {
	try {
		return _spriteman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find sprite component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find sprite component"
		);
	}
}


/** Return the `Velocity` component.

\param id		the entity ID.

\returns		a `Velocity` reference.
*/
Velocity& Scene::velocity(const ID& id) {
	try {
		return _velman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find velocity component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find velocity component"
		);
	}
}


/** Return the `TopDownAutomata` of the entity.

\param id		the entity ID.

\returns		a `TopDownAutomata` reference.
*/
PushdownAutomaton& Scene::pda(const ID& id) {
	try {
		return _pdaman.get(id);
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_ERROR,
			"Scene :: failed to find top-down automata component for id=%d: %s",
			id,
			error.what()
		);

		throw std::out_of_range(
			"Scene :: failed to find top-down automata component"
		);
	}
}


/** Return the ID of the subentity, if one exists.

If the entity does not have a subentity, an ID of `-1` is returned.

\param id		the entity ID.

\returns		the ID of the subentity.
*/
ID Scene::subentity(const ID& id) {
	try {
		return _subman.get(id);
	}
	catch(...) {
		return -1;
	}
}


/** Return the ID of the owner of this entity, if one exists.

If the entity does not have a superentity, and ID of `-1` is returned.

\param id		the entity ID.

\returns		the ID of the superentity.
*/
ID Scene::superentity(const ID& id) {
	try {
		return _superman.get(id);
	}
	catch(...) {
		return -1;
	}
}


/** Returns the size of the map for the currently loaded scene.

\returns		an `SDL_Rect` that defines the size of the map.
*/
SDL_Rect Scene::mapSize() {
	return _map_size;
}


/** Loads a single entity and its subentities.

Each of the components specified in the JSON file is loaded. An error is thrown
if any of the necessary components are not specified, and any optional 
components are given generic values if not specified.

The exception are the `Dialogue` components because they require all the IDs of
each entity to be known before they can be loaded.

\param data			the JSON data.
\param renderer		the `SDL_Renderer` is required to load the sprites.
\param stage		the currently loaded stage.

\returns			the ID of the entity loaded.
*/
ID Scene::loadEntity(const auto& data, SDL_Renderer* renderer, Stage& stage) {
	const std::string name = data.key();
	const JSON& cdata = data.value();

	ID id = _idman.add(name);

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: loading entity id '%s'=%d",
		name.c_str(),
		id
	);

	loadConnector(id, data);
	loadHitbox(id, data);
	loadPosition(id, data);
	loadProperty(id, data);
	loadVelocity(id, data);

	loadSprite(id, data, renderer);
	loadPortrait(id, data, renderer);
	loadPortraitPosition(id, data);

	loadPDA(id, data, stage);

	// This method is used recursively to load the subentity.
	try {
		for (const auto& sdata: cdata.at("subentity").items()) {
			ID subid = loadEntity(sdata, renderer, stage);
			_subman.add(id, subid);
			_superman.add(subid, id);

			SDL_LogInfo(
				SDL_LOG_CATEGORY_APPLICATION,
				"Scene :: '%s' with id=%d is assigned subentity id=%d",
				name.c_str(),
				id,
				subid
			);
		}
	}
	catch(std::runtime_error& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: failed to create subentity '%s': %s",
			name.c_str(),
			error.what()
		);

		throw(std::runtime_error("Scene :: failed to create subentity"));
	}
	catch(std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no subentity: %s",
			name.c_str(),
			error.what()
		);
	}

	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Scene :: successfully created '%s' with id=%d",
		name.c_str(),
		id
	);

	return id;
}


/** Loads a single entity's connector.

Each entity's connector is loaded to be none by default and does not need to be
specified by JSON.

\param id		the entity ID.
\param data		the JSON data.
*/
void Scene::loadConnector(const ID id, const auto& data) {
	Connector connector = Connector();
	_conman.add(id, connector);
	_conman.get(id).id = id;
}


/** Loads a single entity's hitbox.

Each entity's hitbox component is loaded from JSON. The hitbox component is
optional.

\param data		the JSON data.
*/
void Scene::loadHitbox(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		_hitman.add(id, cdata.at("hitbox").get<Hitbox>());
		_hitman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no hitbox, using empty list: %s",
			data.key().c_str(),
			error.what()
		);

		// An empty `Hitbox` component is created if one is not specified.
		Hitbox hitbox = Hitbox();
		_hitman.add(id, hitbox);
		_hitman.get(id).id = id;
	}
}


/** Loads a single entity's position.

Each entity's position component is loaded from JSON. The position component is
required.

\param data		the JSON data.
*/
void Scene::loadPosition(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		_posman.add(id, cdata.at("position").get<Position>());
		_posman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no position: %s",
			data.key().c_str(),
			error.what()
		);

		throw(std::runtime_error("Scene :: entity is missing position"));
	}
}


/** Loads a single entity's property component.

Each entity's property component is loaded from JSON. The property component
is optional.

\param data		the JSON data.
*/
void Scene::loadProperty(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		_propman.add(id, cdata.at("property").get<Property>());
		_propman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: %s has no property: %s",
			data.key().c_str(),
			error.what()
		);

		// An empty `Property` component is created if one is not specified
		_propman.add(id, Property());
		_propman.get(id).id = id;
	}
}


/** Loads a single entity's velocity component.

Each entity's velocity component is loaded from JSON. The velocity component
is optional.

\param data		the JSON data.
*/
void Scene::loadVelocity(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		_velman.add(id, cdata.at("velocity").get<Velocity>());
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no velocity: %s",
			data.key().c_str(),
			error.what()
		);

		// An empty `Velocity` component is created if one is not specified.
		Velocity velocity = Velocity();
		velocity.dx = 0;
		velocity.dy = 0;
		_velman.add(id, velocity);
	}

	_velman.get(id).id = id;
}


/** Loads a single entity's portrait sprites.

Each entity's portrait sprites are loaded from JSON. The portrait component is
necessary.

\param data			the JSON data.
\param renderer		the renderer is required to load the portraits.
*/
void Scene::loadPortrait(const ID id, const auto& data, SDL_Renderer* renderer) {
	const JSON& cdata = data.value();

	try {
		Sprite portrait = cdata.at("portrait").get<Sprite>();
		_portman.add(id, portrait);
		// _portman.add(id, cdata.at("portrait").get<Sprite>());
		_portman.get(id).load(renderer);
		_portman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no portrait: %s",
			data.key().c_str(),
			error.what()
		);
	}
}


/** Loads a single entity's portrait position.

Each entity's portrait position is statically set and is to be changed by the
`VisualNovel`.

\param id		the entity ID.
\param data		the JSON data.
*/
void Scene::loadPortraitPosition(const ID id, const auto& data) {
	try {
		SDL_Rect portrait_position = {0, 0, 256, 256};
		_portposman.add(id, portrait_position);
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no portrait position: %s",
			data.key().c_str(),
			error.what()
		);
	}
}


/** Loads a single entity's spritesheet.

Each entity's spritesheet component is loaded from JSON. The spritesheet
component is necessary.

\param data			the JSON data.
\param renderer		the renderer is required to load the spritesheet.
*/
void Scene::loadSprite(const ID id, const auto& data, SDL_Renderer* renderer) {
	const JSON& cdata = data.value();

	try {
		Sprite sprite = cdata.at("sprite").get<Sprite>();
		_spriteman.add(id, sprite);
		_spriteman.get(id).load(renderer);
		_spriteman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no sprite: %s",
			data.key().c_str(),
			error.what()
		);
	}
}


/** Loads a single entity's top-down automata component and its states.

Each entity's states are loaded from JSON and stored into the top-down automata
component. Each entity requires at least one state to be the initial state.

\param id		the entity ID.
\param data		the JSON data.
\param stage	reference to the stage where the scene is loaded.
*/
void Scene::loadPDA(const ID id, const auto& data, Stage& stage) {
	const JSON& cdata = data.value();

	try {
		_pdaman.add(id, PushdownAutomaton());
		_pdaman.get(id).id = id;

		JSON pdata = loadJSONData(cdata.at("states"), "states");

		for (const auto& item: pdata.items()) {
			const std::string key = item.key();
			const JSON& value = item.value();

			_pdaman.get(id).load(STATES_STRINGS.at(key), id, value, stage);
		}
	}
	catch(std::exception& error) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' is missing states: %s",
			data.key().c_str(),
			error.what()
		);

		throw(std::runtime_error("Scene :: entity is missing states"));
	}

	if (!_pdaman.get(id).hasInitial()) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no initial state set",
			data.key().c_str()
		);

		throw(
			std::runtime_error("Scene :: entity has no initial state set")
		);
	}

	if (!_pdaman.get(id).hasDependentStates()) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has is missing dependent states",
			data.key().c_str()
		);

		throw(
			std::runtime_error("Scene :: entity is missing dependent states")
		);
	}
}


/** Loads a single entity's dialogue.

Each entity's dialogue component is loaded from JSON. This method should be
called after all the entities have been loaded so that the correct entity IDs
are mapped to the correct dialogue.

\param data			the JSON data.
\param renderer		the `SDL_Renderer` is required to kern the text.
*/
void Scene::loadDialogue(const ID id, const auto& data, SDL_Renderer* renderer) {
	const JSON& cdata = data.value();

	try {
		Dialogue dialogue = Dialogue();
		dialogue.id = id;

		for (const auto& ddata: cdata.at("dialogue").items()) {
			const std::string dname = ddata.key();
			const JSON& sdata = ddata.value();
			const ID did = _idman.id(dname);

			dialogue.scripts[did] = sdata.get<DialogueScript>();
		}

		_dialman.add(id, dialogue);

		SDL_LogInfo(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: successfully loaded dialogue for id=%d",
			id
		);
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: id=%d has no dialogue: %s",
			id,
			error.what()
		);
	}
}


/** Loads a single entity's inventory component.

Each entity's inventory is loaded from JSON and stored into the inventory
component. If there is no inventory specified, then the entity's inventory will
be empty.

\param id		the entity ID.
\param data		the JSON data.
*/
void Scene::loadInventory(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		Inventory inventory = Inventory();

		for (const auto& item: cdata.at("inventory").items()) {
			const std::string item_name = item.key();
			const ID item_id = _idman.id(item_name);

			inventory.add(item_id);
		}

		_inventman.add(id, inventory);
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: id=%d has no inventory, creating empty inventory: %s",
			id,
			error.what()
		);

		_inventman.add(id, Inventory());
	}

	_inventman.get(id).id = id;
}


/** Loads a single entity's knowledge component.

Each entity's knowledge is loaded from JSON and stored into the knowledge
component. If there is no knowledge specified, then the entity's knowledge
index will be empty.

\param id		the entity ID.
\param data		the JSON data.
*/
void Scene::loadKnowledge(const ID id, const auto& data) {
	const JSON& cdata = data.value();

	try {
		Knowledge knowledge = Knowledge();

		for (const auto& item: cdata.at("knowledge").items()) {
			const std::string item_name = item.key();
			const ID item_id = _idman.id(item_name);
			const std::string level = item.value();
			
			if (level == "FULL") {
				knowledge.set(item_id, KNOWLEDGE::FULL);
			}
			else if (level == "PARTIAL") {
				knowledge.set(item_id, KNOWLEDGE::PARTIAL);
			}
			else if (level == "EMPTY") {
				knowledge.set(item_id, KNOWLEDGE::EMPTY);
			}
			else {
				SDL_LogDebug(
					SDL_LOG_CATEGORY_APPLICATION,
					"Scene :: id=%d has invalid knowledge level: '%s'",
					id,
					level
				);
			}
		}

		_knowman.add(id, knowledge);
		_knowman.get(id).id = id;
	}
	catch (std::exception& error) {
		SDL_LogDebug(
			SDL_LOG_CATEGORY_APPLICATION,
			"Scene :: '%s' has no knowledge, creating with empty knowledge: %s",
			data.key().c_str(),
			error.what()
		);

		_knowman.add(id, Knowledge());
	}

	_knowman.get(id).id = id;
}


/** Defines the map size and offsets from (0, 0).

Setting the minimum x and y coordinates to be zero helps with the pathfinding.
*/
void Scene::offsetPositions() {
	int x_min = 0;
	int x_max = 0;
	int y_min = 0;
	int y_max = 0;

	// Find global minima and maxima
	for (const ID id: ids()) {
		const Position& position_ = position(id);
		const SDL_Rect& sprite_ = sprite(id).sprite;
		const SDL_Rect hitbox_ = hitbox(id).unionbox();

		// Find local minima and maxima
		const int x_local_min = std::min(position_.x, position_.x + hitbox_.x);
		const int x_local_max = position_.x + std::max(sprite_.w, hitbox_.x + hitbox_.w);

		const int y_local_min = std::min(position_.y, position_.y + hitbox_.y);
		const int y_local_max = position_.y + std::max(sprite_.h, hitbox_.y + hitbox_.h);

		// Check if local minima and maxima are candidates to be global
		// minima and maxima
		x_min = std::min(x_min, x_local_min);
		x_max = std::max(x_max, x_local_max);

		y_min = std::min(y_min, y_local_min);
		y_max = std::max(y_min, y_local_max);
	}

	// Offset positions for all entities from zero
	for (const ID id: ids()) {
		Position& position_ = position(id);

		position_.x += SIZE_BUFFER - x_min;
		position_.y += SIZE_BUFFER - y_min;
	}

	_map_size = {
		0,
		0,
		x_max - x_min + (2 * SIZE_BUFFER),
		y_max - y_min + (2 * SIZE_BUFFER)
	};

	SDL_LogInfo(
		SDL_LOG_CATEGORY_INPUT,
		"Scene :: successfully loaded map with size (x=%d, y=%d, w=%d, h=%d)",
		_map_size.x, _map_size.y,
		_map_size.w, _map_size.h
	);
}
