/** camera.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Camera` is used relational to the position of the objects in the
`Scene`.

TODO: add a critically damped spring to the position of the camera
*/


#ifndef SRC_CAMERA_HPP_
#define SRC_CAMERA_HPP_


#include <SDL2/SDL.h>

#include "globals.hpp"


class Camera {
public:
	Camera();
	Camera(const SDL_Rect frame);

	void update(const SDL_Rect& spritebox);

     void setRenderer(SDL_Renderer* renderer);

	/// Stores the position and size of the camera.
	SDL_Rect frame;
};


#endif /* SRC_CAMERA_HPP_ */
