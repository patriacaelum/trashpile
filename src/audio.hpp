/** audio.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The audio manager deals with everything related to music and sound.


\author    Ronald Tran
\author    Austin Nhung
\date      2018-07-21
*/


#ifndef SRC_AUDIO_HPP_
#define SRC_AUDIO_HPP_


#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <vector>


class Audio {
private:
	/// Total number of allocated channels for BGM.
	int totalBGM;
	/// Total number of allocated channles for SFX.
	int totalSFX;
	/// Group number for BGM tracks.
	int BGMtag = 0;
	/// Group number for SFX tracks.
	int SFXtag = 1;

public:
	Audio();
	void close(int chan);

	int numBGM ();
	int numSFX ();

	void changeVol (int chan, int vol);
	bool checkChannel (int chan);
	int playLoop (const char* track, int vol);
	int playSFX (const char * track, int vol);
	void replace (int chan, const char* track);
	void stopBGM ();
	void stopSFX ();
};


#endif /* SRC_AUDIO_HPP */
