#include "grappling_hook_inactive.hpp"


GrapplingHookInactive::GrapplingHookInactive(
	const ID id,
	const JSON& data,
	Stage& stage
) :	State(
		"grappling_hook_inactive",
		true,
		{STATES::GRAPPLING_HOOK_SHORT},
		id,
		data
	)
{}


/** Sets the sprite size to zero so that it does not show on screen.

\param stage
	the currently loaded stage.
*/
void GrapplingHookInactive::enter(Stage& stage) {
	State::enter(stage);

	stage.scene.sprite(id).sprite.w = 0;
	stage.scene.sprite(id).sprite.h = 0;
}


/** Notifies the state of an event.

Given an `INITIATE` input, a `ShortGrapplingHook` state is returned.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a `State` pointer.
*/
State* GrapplingHookInactive::notify(
	const EVENT event, 
	const ID sender, 
	Stage& stage
) {
	switch (event) {
	case EVENT::INITIATE:
		const ID superentity = stage.scene.superentity(id);

		if (superentity != -1 && sender == superentity) {
			return stage.scene.pda(id)[STATES::GRAPPLING_HOOK_SHORT];
		}

		break;
	}

	return nullptr;
}
