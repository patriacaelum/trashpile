/** velocity.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Velocity` denotes the movement of the `Entity` in the `Scene`.
*/


#ifndef SRC_COMPONENTS_VELOCITY_HPP_
#define SRC_COMPONENTS_VELOCITY_HPP_


#include "id.hpp"


class Velocity {
public:
	/// The ID of the associated entity
	ID id;

	/// The number of pixels per frame to the right of the window.
	int dx;

	/// The number of pixels per frame to the bottom of the window.
	int dy;
};


#endif /* SRC_COMPONENTS_VELOCITY_HPP_ */
