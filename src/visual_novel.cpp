#include "visual_novel.hpp"
#include "stage.hpp"


/** Initializes the TTF fonts and checks if initialization is successful.
*/
VisualNovel::VisualNovel() :
    _is_running(false),
    _speaker(-1),
    _listener(-1),
    _script(NULL),
    _rendered_text(NULL),
    _text_position({0, 600, 1280, 120}),
    _textbox({0, 600, 1280, 120}),
    _renderer(NULL),
    _window_width(0),
    _window_height(0),
    _normal_font(NULL),
    _normal_font_name(""),
    _normal_font_size(0)
{
    _normal_font_name = FONTS["normal"].first;
    _normal_font_size = FONTS["normal"].second;
    _normal_font = TTF_OpenFont(_normal_font_name.c_str(), _normal_font_size);

    if (_normal_font == NULL) {
        SDL_LogError(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: failed to load normal TTF %s: %s",
            _normal_font_name.c_str(),
            TTF_GetError()
        );
    }
    else {
        SDL_LogInfo(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: successfully loaded normal TTF %s",
            _normal_font_name.c_str()
        );
    }
}


/** Destroys the TTF fonts and checks if they are destroyed successfully.
*/
VisualNovel::~VisualNovel() {
    unloadText();

    if (_normal_font != NULL) {
        TTF_CloseFont(_normal_font);
        _normal_font = NULL;

        SDL_LogInfo(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: successfully destroyed normal TTF %s",
            _normal_font_name.c_str()
        );
    }
    else {
        SDL_LogError(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: failed to destroy normal TTF %s",
            _normal_font_name.c_str()
        );
    }
}


/** Sets the size of the renderer for rendering the text.

\param renderer     a pointer to the current renderer.
*/
void VisualNovel::setRenderer(SDL_Renderer* renderer) {
    _renderer = renderer;

    SDL_GetRendererOutputSize(_renderer, &_window_width, &_window_height);
}


void VisualNovel::start(const ID speaker, const ID listener, Stage& stage) {
    try {
        _script = &stage.scene.dialogue(speaker).at(listener);
        _speaker = speaker;
        _listener = listener;
    }
    catch (...) {
        try {
            _script = &stage.scene.dialogue(listener).at(speaker);
            _speaker = listener;
            _listener = speaker;
        }
        catch (std::exception& error) {
            SDL_LogError(
                SDL_LOG_CATEGORY_ERROR,
                "VisualNovel :: failed to find script for entities with ID '%d' and '%d': %s",
                speaker,
                listener,
                error.what()
            );

            throw std::runtime_error("VisualNovel:: failed find script");
        }
    }

    _script->reset();

    renderText(_script->currentLine());

    stage.scene.portrait(_speaker).sprite = {0, 0, 256, 256};
    stage.scene.portrait(_listener).sprite = {0, 0, 256, 256};

    stage.scene.portraitPosition(_speaker) = {0, 88, 512, 512};
    stage.scene.portraitPosition(_listener) = {768, 88, 512, 512};

    stage.scene.pda(_speaker).notify(EVENT::SPEAK, _listener, stage);
    stage.scene.pda(_listener).notify(EVENT::LISTEN, _speaker, stage);

    stage.mailbox.subscribe(this, EVENT::PRESS_A_BUTTON);

    _is_running = true;

    SDL_LogInfo(
        SDL_LOG_CATEGORY_SYSTEM,
        "VisualNovel :: successfully started visual novel for script with speaker='%d' and listener='%d'",
        _speaker,
        _listener
    );
}


/** Checks if the `VisualNovel` is running.

\returns    `true` if the `VisualNovel` is running, `false` otherwise.
*/
bool VisualNovel::isRunning() {
    return _is_running;
}


/** Stops the visual novel engine.

The entities speaking to one another are given `DISCONNECT` notifications and
rendered text is unloaded.

\param stage    the currently loaded stage.
*/
void VisualNovel::stop(Stage& stage) {
    stage.mailbox.unsubscribe(this, EVENT::PRESS_A_BUTTON);

    stage.scene.pda(_speaker).notify(EVENT::DISCONNECT, _listener, stage);
    stage.scene.pda(_listener).notify(EVENT::DISCONNECT, _speaker, stage);

    _speaker = -1;
    _listener = -1;

    _script = NULL;

    unloadText();

    _is_running = false;

    SDL_LogInfo(
        SDL_LOG_CATEGORY_SYSTEM,
        "VisualNovel :: successfully stopped visual novel"
    );
}


/** Notifies the visual novel engine.

\param event    the event trigger.
\param sender   the id of the entity that posted the event.
\param stage    the currently loaded stage.
*/
void VisualNovel::notify(const EVENT event, const ID sender, Stage& stage) {
    switch (event) {
    case EVENT::PRESS_A_BUTTON:
        update(stage);

        break;

    case EVENT::DIALOGUE_START: {
        const ID connection = stage.scene.connector(sender).connection();
        start(sender, connection, stage);

        break;
    }

    case EVENT::DIALOGUE_STOP:
        stop(stage);

        break;
    }
}


void VisualNovel::update(Stage& stage) {
    if (_script->current_node_id == _script->last_node_id) {
        stop(stage);
    }
    else {
        renderText(_script->nextLine());
    }
}


SDL_Texture* VisualNovel::text() {
    return _rendered_text;
}


SDL_Rect& VisualNovel::textPosition() {
    return _text_position;
}


SDL_Rect& VisualNovel::textbox() {
    return _textbox;
}


std::vector<ID> VisualNovel::portraitIDs() {
    return {_listener, _speaker};
}


void VisualNovel::renderText(std::string text) {
    unloadText();

    int text_width;
    int text_height;
    TTF_SizeText(_normal_font, text.c_str(), &text_width, &text_height);

    int wraps = std::ceil((float)text_width / (float)_window_width);

    _text_position = {
        0,
        600,
        std::min(_window_width, text_width),
        text_height * wraps
    };

    SDL_Color white = {255, 255, 255};
    SDL_Surface* surface;
    surface = TTF_RenderText_Blended_Wrapped(
        _normal_font,
        text.c_str(),
        white,
        _window_width
    );

    _rendered_text = SDL_CreateTextureFromSurface(_renderer, surface);

    SDL_FreeSurface(surface);

    if (_rendered_text == NULL) {
        SDL_LogError(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: failed to render text: %s",
            TTF_GetError()
        );

        throw std::runtime_error("VisualNovel :: failed to render text");
    }
    else {
        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER, 
            "VisualNovel :: successfully rendered text"
        );
    }
}


void VisualNovel::unloadText() {
    if (_rendered_text != NULL) {
        SDL_DestroyTexture(_rendered_text);

        _rendered_text = NULL;

        SDL_LogDebug(
            SDL_LOG_CATEGORY_RENDER,
            "VisualNovel :: successfully destroyed rendered text"
        );
    }
}
