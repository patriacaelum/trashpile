# Thematic Structure

While Austin was researching story structure, especially in film, there was an
interesting idea on how the thematic material of the story was integrated into
the actual structure. Some examples are:

- Rashomon (1950) is a film about questioning our memories of the past. The film
  takes the testimony of four people on a rape crime. However, each testimony
  contradicts another in some way, and also reveals that person's motivation and
  true character. The thief's testimony depicts himself as strong and herioc,
  the woman's testimony depicts herself as the helpless victim, and the
  samurai's testimony depicts himself as rightous against a cheating woman.
- Mememto (2000) is about a man who has reoccurring amnesia. To put the viewer
  through the same experience of only knowing the present and desparate to
  discover the past, the story is told in reverse, slowly revealing the roots of
  the crisis.
  
Below are some possibilities on how a theme of stealth/theft may be reflected in
the structure.

### A Stolen Past

The classic amnesiac protagonist may be the simplest example of a protagonist
who has had their past and identity stolen from them. So then the quest begins
to take it back.

```plantuml
@startuml

component StolenPast
component MemoryRetrieval

StolenPast --|> MemoryRetrieval

@enduml
```

### A Quest to Forget

The reversal of the classic amnesiac. In this case, the protagonist is actually
trying to forget who they are and their past.

This is an interesting scenario because there is an inherent clash of
motivations between the protagonist and the player. The protagonist wants to
forget about their past, and is probably reluctant to bring it up, yet, as the
player, we are curious about there past and will will them into situations that
forces them to recount and reflect on his past.

```plantuml
@startuml

component HiddenPast
component QuestToForget

HiddenPast --|> QuestToForget

@enduml
```

As an example, perhaps in the protagonist's quest to forget, the protagonist
must first go through heists and assassinations to get rid of the remnants of
his past. In the process of destroying these remnants, the player learns of his
past. He begins to rely on his future amnesia and revenge as justification for
his terrible actions, only causing him to become more eager to forget his guilt.

The challenge with this appoach is making the story of his past compelling
enough to continue destroying everything related to the protaganist.

### A Quest to Forget A Stolen Past

This would be a combination of the previous two ideas. It might be interesting
to tell the story of the man who wants to forget everything and the story of the
man who remember everything in parallel.

```plantuml
@startuml

frame StolenPast {
  component Reconstruction1
  component Reconstruction2
  component Reconstruction3
}

frame QuestToForget {
  component Destruction1
  component Destruction2
  component Destruction3
}

Reconstruction1 --|> Destruction1
Destruction1 --|> Reconstruction2
Reconstruction2 --> Destruction2
Destruction2 --|> Reconstruction3
Reconstruction3 --|> Destruction3

@enduml
```

The biggest weakness in this approach is probably that there is very little
tension for the reconstruction segments since we already know that it is our
actions that us to forget. Perhaps we could take a page out of the structure of
memento and tell the story in reverse, beginning with the latest reconstruction,
and slowly revealing the roots of the destruction.

### Recurring Amnesia

Although this may sound very similar to Memento (2000), I was actually thinking
of it more like reading a book but some of the chapters are missing. This could
be either the protaganist is missing chapters, or perhaps the protagonist is
skipping over parts of their life, witholding information from the player.

```plantuml
@startuml

component Chapter1
component MissingChapter2
component Chapter3
component MissingChapter4
component Chapter5
component MissingChapter6

Chapter1 --|> MissingChapter2
MissingChapter2 --|> Chapter3
Chapter3 --|> MissingChapter4
MissingChapter4 --|> Chapter5
Chapter5 --|> MissingChapter6

@enduml
```


# Austin's Pitch: A Quest to Forget

based on the thematic structure of the protagonist causing his own amnesia. The
actual theme is based on the idea that at the heart of stealth and theft is
loss. This pitch is constructed around the question of, "what if the thief was
the one who had the most to lose?"

### The Elevator Pitch

A man with a troubled past sets his mind on finding the Oracle, who is said is
able to wipe a person's memory clean. However, before he can leave his past
behind, he sets out to destroy all remnants of his past, leaving no trace that
he ever existed for his future amnesiac self to rediscover.

### Possible Developments

There is an interesting conflict of interest between the protagonist and the
player. The Protagonist is resistant to reminisce, yet the player guides him
through tasks to learn about his past.

Some possible motivations for performing these heists are:

- He lost custody of his child, who was taken away and put in a foster home.
  However, the foster home takes in many orphaned children, and due to their own
  biases, tend to neglect children of common criminals, and the child dies due
  to disease and malnutrition. The loose end to clean up here are the foster
  parents who knew about his criminal deeds and destroy his child's body/grave.
- His wife was sickly, but he did not have to money to pay for her operation and
  the doctor refused to do it for free. He had to resort to a life of crime to
  get money faster, but in the end he was too late. This loose end is more of a
  revenge story, avenging his wife's death.
- Perhaps he or his wife were an artist or an architect, so he seeks to steal
  their paintings and destroy them or burn their buildings to the ground.
- Some of his valuable belongings that he had to pawn off are now in used
  jewelery stores or in the vault of a bank.
  
To be consistent with leaving no trace of his past, he probably dons disguises
regularly and the crimes are designed to seem like the target was something
larger. He would probably also have to raid any place that has any written
record of himself.

### Towards the End

By the end of the heists, he must find the Oracle. However, since he has
destroyed all traces of his existence, he is the last evidence of his own
existence. By wiping his memory, he would have effectively erased himself from
history. Even though he started with the mindset that the world had taken away
everything he cared about, it was he who destroyed all the remaining connections
to the world. Paradoxically, his actions has also made the player aware of his
life's story.

When he does reach the Oracle, what choice does he make? He could go through
with the memory wipe, leaving a husk of a person who will always wonder about
his lost past but never find it. He could choose to not go through with it, and
live with his guilt in a new life, choosing to remain the sole retainer of his
memories. A last possibility would be to end his life, now that he has cut off
all remaining connections to the world, and now has nothing left to live for.

# Austin's Pitch V2: A Quest to Forget

### The Elevator Pitch

A young woman with a troubled past sets her mind on finding the Oracle, who is said is
able to wipe a person's memory clean. However, before she can leave her past
behind, she sets out to destroy all remnants of her past, leaving no trace that
she ever existed for her future amnesiac self to rediscover.

### Character Profiles

##### Kito

Kito what the main protagonist calls herself. She is a paradox in nearly every way. She is confidently hopeful about her hopeless future. She is open and loving to others, yet is stubborn and will not allow herself to be loved. She is strong and skilled, but she is unwilling to see herself as such.

Her real name is unknown to all, but she calls herself Kito because she she views herself as a pest: unnoticed, unwanted, and disposable. Yet, she is here and alive, finding her place among the fringe and outskirted areas of society. She plans to take what she needs and disappear. She is a mosquito.

##### Kito's Parents

Kito's parents weren't very important people. Her mother was a modestly successful painter. She has one or two paintings that are in a small part of a museum exhibit as a ode to the unknown painters of the rural areas. Other than that, she would often be hired as a painter for productions that don't necessarily publish credits to their staff. She painted sets for the theatre and posters for advertisements. Things like that. Kito's father was a hardy labourer. He probably worked in construction or mining, and he was good at it. He was a leader and he was an organizer.

Kito's father was overworked and worked in rather unsafe conditions. This led to a rather sharp decline in his health, and it was at that point Kito's parents schemed to have a divorce. Kito's father had a poorly hidden affair and Kito's mother used this as a means for divorce before Kito's father's health got much worse. Though there was not a lot documented on this disease, it was known among the miner communities. They knew that he would need to take out loans in order to afford any healthcare when his body would start to fail him, and they both knew it was better to be a divorced woman than an indebted widow.

Unbeknownst to them, Kito's mother was pregnant a little before the fiasco and Kito was born a little after the divorce. Though being a divorced woman may be skirting social acceptability, having a child as a single woman means social outcasting for both the woman and the child. Kito is their second child, be she remains undocumented. Poorly documented citizens are pretty common in rural areas, but Kito remains a secret from everyone.

As Kito grows older, Kito has grown up in relative isolation. When the family attends church, she is left alone and she uses the opportunity to explore the surrounding forests and town while most people aren't home, sneaking around, stealing snacks, and getting back before anyone notices. At the same time, Kito's father's health gets worse. He was already noticeably weaker and was only working in a manager capacity and trying to get better conditions from the higher ups in the city, but now he's stopped working altogether, and Kito's older brother decides to take up the mantle.

By the time Kito is in her tweens, she is ready to get out and experience life, and both her father and brother die from the same disease. It seems that whatever the cause, it affects younger people faster than older people. Kito's mother reacts badly, and moves back to the city where she used to work.

### Plot Points

##### The Museum Heist

##### The Government Archives

##### The Graveyard Desecration

It was common practice to bury the dead who died of illness and burn the bodies of those who did not. It was believed that if you burn the bodies of the ill, the sickness will spread through the air. However, Kito needs to destroy the graves of her father and brother and the records that state who is buried where. When she takes the bodies out of the grave, she laments, "They shouldn't have buried you. You spent enough of your life underground."

Then she uses her knowledge of how pet cremations are often faked to cremate her father and brother, finally sets them free from the Earth.

##### The Bank Safebox

Upon arrival in the city, Kito's mother pawned off her wedding ring. It was made of a rare element that Kito's father mined and stole from the company, and is inscribed with her name. Kito heads to the old pawn shop to search through the records and find out who bought the ring. It wasn't too difficult because the buyer has a wife with the same name.

Kito finds their home and searches through the jewelery box, but realizes that they are having marital troubles and the wife is keeping the ring in a safebox in the bank, in case she needs it to sell off later. This makes things much harder, but Kito infiltrates the bank under the guise of a more elaborate bank heist in order to take the ring and destroy it.

##### A Rich Man's Murder

# Austin's Pilot Pitch

Building off the characters and plot outlined in the previous pitches, I propose that we start the pilot on a more lighthearted note by starting at the beginning of Kito's story.

This is a typical weekend. Kito's parents and older brother are heading into town for the regular town gathering. This may be a church event or a farmer's market, or the day where the trains pass through town. Kito's parents have packed their fruits and veggies from their large backyard garden and orchard. They head into town on their small and worn out automobile or horse carriage. 

Meanwhile, Kito is at the front window seeing them off until they are out of sight. She was told to stay at home and take care of the house. Anytime she asks to tag along, she is told she isn't old enough yet, and that she can come in a few years. Her parents haven't quite figured out what to tell her when Kito is old enough to understand she can never show her face in town without being outcast. But Kito is a smart girl. She's heard the rumours and the way her parents talk. She knows she can't let herself be seen by the townsfolk. Nonetheless, Kito takes a small backpack with her and heads into the forest, following a path between the hills that separate the farmland from town, as she jumps across streams and kicks rocks on the way. This is the tutorial area, where the player learns how to move and jump.

As Kito enters the backwoods of the town, she checks that the town is mostly quiet and empty. The only people around are those who are taking care of the elderly and young who are also unable to attend the event, and those who are late and hurrying on their way. This is her chance to explore town at her own pace, but she still needs to stay in the shadows. Now, her imagination runs wild.

Kito is careful as she goes through the rounds. Looting the bakery for some day-old cookies, too stale to sell to train passengers, but still soft and sweet. Looking through the lost and found, mostly of old broken toys that other kids on the train have left behind, but won't be missed. Browsing through the library for a new picture book to fill her mind with more stories from all over the world. And in each little mini-mission, Kito imagines a high-stakes scenario with a generous amount of tropes and archetypes mishmashed together. This lets us experiment with different art styles and different mission types that are small and compact.

Kito realizes she's been reading in the library for too long. When she exits the library, the town square where all the festivities were present from what seemed like moments ago are gone. There are a few people still cleaning up, but the people are gone and the trash cans are full and train line is empty. She's late. She rushes back through the forest as the sun sets and makes it back home, expecting her parents to scold her for playing outside in the dark. She was already making up excuses and planning on where to hide her new treasures. When she gets home, nobody is home. Kito feels a pit in her stomach, but sits by the window and waits. An hour later, still nobody comes home. Something is urgently wrong.