#include "roll.hpp"


Roll::Roll(const ID id, const JSON& data, Stage& stage)
:   State("roll", false, {}, id, data)
{}


void Roll::enter(Stage& stage) {
    State::enter(stage);

    _diff_index = 0;
}


State* Roll::notify(const EVENT event, const ID sender, Stage& stage) {
    // Release directional buttons
    std::array<EVENT, 4> rdb = {
        EVENT::RELEASE_DOWN_BUTTON,
        EVENT::RELEASE_LEFT_BUTTON,
        EVENT::RELEASE_RIGHT_BUTTON,
        EVENT::RELEASE_UP_BUTTON
    };

    if (std::find(rdb.begin(), rdb.end(), event) != rdb.end()) {
        // Finish walk state
        stage.scene.pda(id)[STATES::WALK]->notify(EVENT::TERMINATE, id, stage);
    }

    return nullptr;
}


State* Roll::update(Stage& stage) {
    Position& position = stage.scene.position(id);
    Velocity& velocity = stage.scene.velocity(id);

    if (_hframes == _max) {
        if (_diff_index == _speed_diff.size()) {
            _finished = true;

            return nullptr;
        }

        if (velocity.dx != 0) {
            velocity.dx += std::copysign(_speed_diff[_diff_index], velocity.dx);
        }

        if (velocity.dy != 0) {
            velocity.dy += std::copysign(_speed_diff[_diff_index], velocity.dy);
        }

        ++_diff_index;

        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Roll :: velocity is now (%d, %d)", velocity.dx, velocity.dy);
    }

    position.x += velocity.dx;
    position.y += velocity.dy;

    if (stage.findCollision(id) != -1) {
        position.x -= velocity.dx;
        position.y -= velocity.dy;

        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Roll :: collision found");
    }

    return State::update(stage);
}
