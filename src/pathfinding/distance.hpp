/** distance.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

These are simple functions that calculate the distance between two nodes on a
graph.
*/


#ifndef SRC_PATHFINDING_DISTANCE_HPP_
#define SRC_PATHFINDING_DISTANCE_HPP_


#include <algorithm>

#include "graph_node.hpp"


float manhattan_distance(const GraphNode* start, const GraphNode* goal);


float octinal_distance(const GraphNode* start, const GraphNode* goal);


#endif /* SRC_PATHFINDING_DISTANCE_HPP_ */
