/** grappling_hook_long.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The long grappling hook animation that corresponds to the `LongAttackState`.
*/


#ifndef SRC_STATES_GRAPPLING_HOOK_LONG_HPP_
#define SRC_STATES_GRAPPLING_HOOK_LONG_HPP_


#include <string>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class GrapplingHookLong: public State {
public:
	GrapplingHookLong(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;
	void exit(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_GRAPPLING_HOOK_LONG_HPP_ */
