#include "hitbox.hpp"


/** Return the list of hitboxes associated with the sprite position.

\param key		The position of the sprite on the spritesheet.

\returns		A list of rectangles.
*/
std::vector<SDL_Rect> Hitbox::hitbox(
		const SDL_Rect key, 
		const DIRECTION direction,
		const SDL_Point& centre
	) const {
	std::vector<SDL_Rect> hitbox = {};

	try {
		hitbox = _hitboxes.at(key);
	}
	catch (std::out_of_range& error) {
		if (_hitboxes.size() == 1) {
			hitbox = _hitboxes.begin()->second;
		}
	}
	catch (...) {
	}

	// Rotate hitboxes
	for (SDL_Rect& box: hitbox) {
		SDL_Rect old = {box.x - centre.x, box.y - centre.y, box.w, box.h};

		// Rotate hitboxes
		switch(direction) {
			case DIRECTION::SOUTH:
				break;

			case DIRECTION::WEST:
				box.x = - (old.y + old.h) + centre.x;
				box.y = old.x + centre.y;
				box.w = old.h;
				box.h = old.w;

				break;

			case DIRECTION::EAST:
				box.x = old.y + centre.x;
				box.y = old.x + centre.y;
				box.w = old.h;
				box.h = old.w;

				break;

			case DIRECTION::NORTH:
				box.y = - (old.y + old.h) + centre.y;

				break;
		}
	}

	return hitbox;
}


/** The highest point in the group of hitboxes.

Finding the highpoint is important to define the render order.

\param key		The position of the sprite on the spritesheet.

\returns		The y-coordinate of the highest hitbox.
*/
int Hitbox::highpoint(const SDL_Rect& key) const {
	int highpoint = 0;

	try {
		highpoint = _highpoints.at(key);
	}
	catch (...) {
	}

	return highpoint;
}


/** The union hitbox that encompasses all of the hitboxes of this entity.
*/
SDL_Rect Hitbox::unionbox() const {
	return _unionbox;
}


/** Set a sprite position to a list of hitboxes.

This method also sets the highpoints and unionbox.

\param key		The position of the sprite on the spritesheet.
\param value	A list of rectangles.
*/
void Hitbox::set(const SDL_Rect& key, const std::vector<SDL_Rect>& value) {
	if (_hitboxes.count(key) != 0) {
		SDL_LogWarn(
			SDL_LOG_CATEGORY_APPLICATION,
			"Hitbox :: failed to assign hitbox for id=%d: already assigned",
			id
		);
	}

	_hitboxes[key] = value;

	int highpoint = 0;

	for (const SDL_Rect& hitbox: value) {
		// Set highpoint
		int hp = hitbox.y + hitbox.h;

		if (hp > highpoint) {
			highpoint = hp;
		}

		// Set unionbox
		// The maximum widths and heights must be one, the other, or a
		// combination of both
		SDL_Rect old_unionbox = _unionbox;

		_unionbox.x = std::min(old_unionbox.x, hitbox.x);
		_unionbox.y = std::min(old_unionbox.y, hitbox.y);
		_unionbox.w = std::max({
			old_unionbox.w,
			hitbox.w,
			old_unionbox.x + old_unionbox.w - hitbox.x,
			hitbox.x + hitbox.w - old_unionbox.x
		});
		_unionbox.h = std::max({
			old_unionbox.h,
			hitbox.h,
			old_unionbox.y + old_unionbox.h - hitbox.y,
			hitbox.y + hitbox.h - old_unionbox.y
		});
	}

	_highpoints[key] = highpoint;
}
