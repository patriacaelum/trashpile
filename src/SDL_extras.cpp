#include "SDL_extras.hpp"


/** Checks if the x and y coordinates are equal.
*/
bool operator==(const SDL_Point& lhs, const SDL_Point& rhs) {
	return std::tie(lhs.x, lhs.y) == std::tie(rhs.x, rhs.y);
}


/** Checks if the x, y, w, and h coordinates are equal.
*/
bool operator==(const SDL_Rect& lhs, const SDL_Rect& rhs) {
	return std::tie(lhs.x, lhs.y, lhs.w, lhs.h) == std::tie(rhs.x, rhs.y, rhs.w, rhs.h);
}
