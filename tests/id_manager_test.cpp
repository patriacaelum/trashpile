/** id_manager_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `IDManager`.
*/


#include <string>

#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/id.hpp"
#include "../src/id_manager.hpp"
#include "../src/globals.hpp"


TEST_CASE("ID manager tests", "[id]") {
	IDManager idman = IDManager();

	SECTION("Test ID") {
		REQUIRE_FALSE(idman.test(0));
	}

	SECTION("Add ID") {
		ID id = idman.add("foo");

		REQUIRE(idman.test(id));
		REQUIRE(idman.id("foo") == id);
		REQUIRE(idman.name(id) == "foo");
	}

	SECTION("Remove ID") {
		ID id = idman.add("foo");
		idman.remove(id);

		REQUIRE_FALSE(idman.test(id));
		REQUIRE_THROWS(idman.id("foo"));
		REQUIRE_THROWS(idman.name(id));
	}

	SECTION("Overflow entities") {
		for (int i = 0; i < PROPERTIES::MAX_ENTITIES; ++i) {
			ID id = idman.add(std::to_string(i));
		}

		REQUIRE_THROWS(idman.add("foo"));
	}
}
