# General

I don't enjoy fetch quests where I have to walk across the map to deliver
someone's love letter (i.e. medieval painting of nudes) to their loved one and
get some money/experience. I do, however, enjoy this particular quest in Dragon
Age 3, where an old man wants to place flowers on his late wife's shrine but he
cannot make the trip himself for there are many demons on the road. If you agree
to help him, you will have to fight a bunch of demons on your way to the wife's
grave. In the end you get to read a touching poem he wrote for her, and when you
return to him you will recite that poem and he will burst into tears. To me,
reading the poem the man made for his wife was more rewarding than any XP I
would get out of the quest.

I believe side quests should not be treated as mere means to an end, as if they
are chores that I have to grind through to be strong enough to defeat the final
boss. We can take the followings into considerations when making a side quest:

- **Reward system**: surely we will have to include rewards in the form of XP,
  items, and money, because after all side quests are opportunities for our
  character to be stronger and better equiped to solve the main problem in game.
  However, there are other types of rewards, like one I mentioned in the example
  about old-man-and-dead-wife. First of all, it is a feeling of satisfaction
  that I have done a good deed. Secondly, I had the opportunity to witness their
  love for each other. Thirdly, with all the deaths and destruction from the
  war, I got reminded about how simple things such as bringing flowers to
  someone's grave would also be affected. The side quest should allow us learn
  about the environment, its occupants, and even our main character, in a
  meaningful way that _you in real life_ would be interested in.

- **Choice**: this does not stop at choosing whether to do the quest but how the
  quest ends. A simple example: you found a great sword during your adventuring.
  Later on as you progress, you find people who would be in need of such a
  sword. A little boy who wants to go to martial art school. A smuggler who
  would pay you a hefty sum of money. A magic cow who promises he will aid you
  in a future quest. Or, you can keep the sword for yourself for it has great
  stats. These choices should have consequences.

- **Ah shit**: "Please deliver this love letter to my wife for I am too lazy to
  walk to her on my own!". Okay, fine, I will do that. You accept the letter and
  start finding the said wife. For some reason (unknown to the player), the
  character attracts more demons, or entire villages start chasing after them,
  or their attacks do no damage, or all of the above. The point is, that was no
  ordinary letter, your character has been cursed and now you have to find a way
  to get yourself out of this unexpected situation. This would also be a good
  opportunity for the player to try a new style of playing. How would you play
  if you cannot deal any damage? Or if you move so much slower than you normal
  speed? How about your enemies are now innocent people?
  
  
# Mission Structures

These diagrams are meant to aid in forming the narrative structures of missions.
There are, of course, infinite variations in the details, but it may be an
interesting challenge to create contrasting mission structures.

### Basic Mission

The basic mission involves infiltrating a guarded area, performing an action,
and then escaping.

The timing and setting of the guarded area creates a number of variations on
methods of infiltration and escaping. Methods of entering and exiting are

The action can create an interesting narrative motivation. There are three
basic actions

- **Positive actions** are added the area. This usually comes in the form of
  planting an object such as a bomb, incriminating evidence, poison, etc.
- **Negative actions** are taken from the area. This usually comes in the form
  of stealing an object, such as a valuable, medicine, or even a person.
- **Neutral actions** do not change the area. This usually comes in the form of
  a replacement or something that is replicable, such as information or a
  recording.

```plantuml
@startuml

frame External {
  component Preparation
}

frame Internal {
  component Infiltration
  component Action
  component Escape
}

Preparation --|> Infiltration
Infiltration --|> Action
Action --|> Escape

@enduml
```

### Multiple Infiltrations

The cycle of entering the guarded area and escaping can be repeated multiple
times to arbitrarily extend the length and complexity of a mission. In some
ways, an entire game could be seen as some variation of this structure.

The multiple infiltrations could be over the course of multiple areas, similar
to a scavenger hunt, each area leading to another in some way. The multiple
infiltrations could also take place in the same area over a longer period of
time, allowing the oppurtunity for the player to learn about an area and its
inhabitants in more depth and experimenting with their personal lives.

```plantuml
@startuml

frame External {
  component Preparation
}

frame Internal {
  component Infiltration
  component Action
  component Escape
}

Preparation --|> Infiltration
Infiltration --|> Action
Action --|> Escape
Escape --|> Preparation

@enduml
```

### Breakout

The main difference in this structure is the lack of an infiltration as the
focus is on the method of escape. The inclusion of this structure may create an
additional component of the external preparation, since the player should be
wary of the possibility of capture, and should have a contingency plan in the
event they are captured. However, the capture may be inside or outside of the
guarded area.

A small twist on the formula may be that the capture is intentional and
*becomes* the method of infiltration.

```plantuml
@startuml

component Action
component Capture
component Breakout

Action --|> Capture
Capture --|> Breakout

@enduml
```


# Ideas

### Angel of Death

based on a known [type of serial killer](https://en.wikipedia.org/wiki/Angel_of_mercy_(criminology))
an angel of death of usually someone who works in a hospital setting who kills
patients who have incurable diseases, are in constant pain, or are comatose. The
angel of death claims to kill patients out of mercy, but in reality, it is more
often the power of having another person's life in their hands.

This mission could be an intersection of interests. In one sphere, the hospital
could have a suspected angel of death, but obviously the hospital cannot simply
stop its operations. All the authorities can do is look over previous evidence
and wait for the angel to make a mistake. In another sphere, perhaps there is a
patient at the same hospital who is wanted dead for one reason or another. A
possible mission is to murder the patient and frame it on the angel, thereby
completing the job and placing blame on a known murderer.

Perhaps this only slows the authorities down, since the authorities may suspect
the evidence is planted, as the angel has never made such a sloppy mistake
before, and that the angel will confess to all suspected cases except for the
most recent one.

### Annoying Hostage

A hostage situation where the hostage is weirdly needy but also kind of into it,
saying they're training to be an actress and can cry on command if they're going
to film a ransom video, and other things she's seen on TV.

### Baby Moniters

based on the story of a mother who had a two-way baby moniter, but someone
hacked into it and control its camera, and started talking to her through the
moniter.

As people rely on baby moniters to surveil their children, they don't realize
that they have opened up their private homes so others can listen and prey. A
man with perverse desire can listen to the baby, to the mother, and talk to the
child when the mother isn't around. They may occasionally hear the baby crying,
and decide for themselves that the baby is being neglected and goes to take care
of the child themselves, thinking that they are doing the right thing. The
companies that sell these moniters market it as something designed to help
parents protect their children, but really only serve to facilitate their
failures.

### Bee Colony

based on how a colony of bees survives.

I guess this is kind of a matriarchial society, but I found it really
interesting that since bees don't migrate for the winter, they have a different
method for surviving the cold winter. They kill off all the workers that aren't
necessary for the hive, namely the males.

Applied to a human society, I think it would make more sense that the males are
competing or have some ritual to decide who remains to serve the clan and which
are sacraficed to the gods for a good harvest or whatever.

### Bottle Farms

A farm sells specialty bottle shaped fruit by attaching clear glass bottles to
sapling fruit so that the fruit grows into the shape of the bottle. How they get
the fruit out of the bottle afterwards is a mystery to all.

### Brain Food

There is a certain special food that is very expensive to produce and generally
eaten by the rich and aristocratic, somewhat like caviar. This makes it easy to
track the food for poisoning. Unfortunately, one of the poorer chefs in the
kitchen brings some home for his kids. He mistakenly believes the wivestale that
the special expensive food is what makes the aristocrats smarter and more
spiritual, which is some form of justifying their social, economic, and
religious status. This means that there is a chance that the poisoned food is
heading to the poor chef's child.

### The Cleaning Lady Inc.

There exists a cleaning service where the "cleaning ladies" specialize in
cleaning up crime scenes. Usually their services are only called for when the
assassination is expected to be messy or for when there are multiple bodies to
clean up. They make most of their revenue from cleaning up mass murders,
whether purposeful or accidental.

One interesting aspect for this group might be that The Cleaning Lady Inc. is a
very secretive group, and nobody should know who these people are or what
happens to all of the evidence. The fact that nobody knows, even their clients,
is a key part of the service they provide. The authorities don't have much proof
that The Cleaning Lady Inc. is an entity that actually exists, but one day, the
internal politics of The Cleaning Lady Inc. has broken apart. Probably due to
poor management, worsening work conditions, and unpaid paychecks.

The Cleaning Lady Inc. have become aware that there is a mole in their ranks,
and it is your job sniff out the mole and eliminate them.

### Cremation Fraud

based on an actual case of cremation fraud where the crematory was stashing
human remains in the woods to avoid the time and costs of cremating them,
returning the families concrete dust and ashes from other people.

The assignment would basically consist of either: stealing a corpse and
replacing the cremains with someone else's or stealing a specific person's
cremains and replacing it with someone else's.

The reason could be something practical, such as needing to take a scarce
material that used to be used for replacing parts of the body, or the body may
be needed for an external purpose, such as being part of a deal to return the
body to his home.

Either way, there can also be an emphasis on the how uncomfortable people may
be when friends and family are honouring the life of someone with the wrong
cremains.

Incidently, the cremation fraud may also aid in a special, very expensive secret
service of helping people disappear, faking their deaths and using the wrong
cremains.

### Coal Mining Town

based on how towns based on a specific industry, like coal or steel, begin to
die as the technology changes, and how they fail to deal with it.

This is taken from a game I was writing before this project. The main idea is
creating motivation for the player. Taking a philosophy from the _Phoenix
Wright_ games, a good way to develop motivation is to change the thinking from
"Why would the character _want_ leave home? Why would they _want_ to become a
thief?" to "Why would the character _need_ to leave home? Why _must_ the
character become a thief?" The second point was that I like to see the evolution
of people, and I like having a sense of home, a place where the characters I'm
familiar with can grow and see the changes in their lives.

We start in the home town, which has been prosperous from coal mining (or
whatever resource in our world) since time immemorial. Like most of the able
people in town, you work in the mines. Times are tough for everyone, as new
technologies threaten the livelihoods of the entire town. One day, there is a
massive explosion in the mines. A horrible accident as the caves crumble, and
many of the workers are trapped and die. The fires spread to some of the
surrounding forest and smoke seeps through the air, dissipating the sun.

The town meets at the local community centre (like a church or something), and
the adults decide that they must accept that there is no future here, yet cannot
bring themselves to leave the only home they have ever known. They know too that
there is no future for their children here, so the able young minds are to set
off on a journey to the surrounding cities in hopes of being able to learn new
skills that can bring work back home, while the adults focus on rebuilding and
creating the facilities that will be needed in the future. This format allows
for the character to have suitable motivation to leave home, but also the
oppurtunity to come back and visit family and friends you've known since the
beginning, and watch the progress of the homes being rebuilt, remaining
structures being renovated, a greater focus on agriculture to be
self-sustaining, etc. It gives the player a home to go back to, and a more
meaningful goal.

This can easily tie into a story about a thief as our hero journeys to the city,
and finds that he's not skilled enough to be employed, and cannot afford
education, seeing as he left with little more than the ashes of home and dust
of coal. Not being able to go back empty, he stays, but in order to survive, he
must resort to being exploited or, perhaps, gets caught up in a small group of
pickpockets. He didn't _want_ to steal, but he _needed_ to. He _must_ become a
thief.

Obviously this can develop further, as the more he steals and realizes his
talent for it, the more he worries about what he can really bring back to his
family. Maybe he starts to realize that he loves what he does, but it becomes
his greatest shame as he tries to hide it when he goes back to visit.

A nice twist might be that as the town begins to near completion, as the
finishing touches are being put on the homes, and vegetables growing in size,
the replanted trees have sprouted into little saplings, the people vanish. As
you walk through the homes that are so nostalgic of the very beginning, all that
is in sight is silence. Where have the people gone? Why have they left? Have
your actions had any impact on their fate?

### Colour Out of Space

based on the short story _Colour Out of Space_ by H. P. Lovecraft where a
meteor lands in the forest, and starts to turn the surrounding land white and
plastic, the water becomes tainted and affects the people living nearby, driving
them mad.

### Commercialization Invasion

based on how China and India have changed the lives of the Indigenous people of
Nepal, as they are trying to make a highway through the Himalayas for trade, and
this disrupts the way of life of the Nepali people, who have lived off the land
and livestock for generations. However, the people also find selling to the
Chinese and Indians much easier to make money, despite it destroying their
culture.

In a similar way, a local isolated town has a very distinct culture and way of
life, but two surrounding powers decide to create a trade path that passes right
through the town. The people from the village get hired to build the roads, and
are able to sell goods to the visiting people. This is an easier way to make
money, but they begin to lose their local culture as they try to appease these
foreigners, adapting to the foods they like and begin to provide the
accomodations they expect. The town becomes commercialized, and you watch the
evolution as it transforms. Some of the locals move away, those who want to
maintain the old ways are discarded and left behind. The people want to believe
they're happier, and perhaps they are, but it's still very sad that they've lost
their identity, and don't realized they are being exploited.

The player can interact with the local people, as well as the foreign powers. As
you interact with these people and become friends with some of them, both the
locals and the foreigners, the main idea is that you watch them change as they
succumb to their own greed, this universal desire to live a better life, but
being too short sighted to see the value of what you have, the effects foreign
influence on a people. The people are the same people, but you can see that they
are different in how they act, what they begin to prioritize.

### Debtors Prison

based on the life of Charles Dickens.

I learned that when Dickens was about 10 or 11, his family was quite poor, and
his parents were sent to debtor's prison (which already ties in nicely with the
idea on capitalism
[taking over](devel/sidequests.md#commercialization-invasion)). However, at the
time, debtors prison didn't provide food, so that meant that Dickens had to drop
out of school to work, pay for cheapr housing, buy food for himself, his
siblings and his parents, and help pay of the debt. He would have to actually go
to prison and feed his parents.

Additionally, prisoners were forced to work until their labour paid off their
debts, but of course they still had to pay for rent (of the prison), food, and
clothing. This meant that a lot of families became even poorer after the father
was taken to prison. Even if a prisoner was found to be innocent of charges,
they could remain in jail due to the rent that was not paid for staying in jail.
Of course, people would often die during their confinement during the extreme
heat or cold or illness from living in filth.

This is an excerpt from the
[Wikipedia page](https://en.wikipedia.org/wiki/Marshalsea) on the prison
Dickens' family stayed at
>>>
The first case against Acton, before Mr. Baron Carter, was for the murder in
1726 of Thomas Bliss, a carpenter and debtor. Unable to pay the prison fees,
Bliss had been left with so little to eat that he had tried to escape by
throwing a rope over the wall, but his pursuers severed it and he fell 20 feet
into the prison yard. Wanting to know who had supplied the rope, Acton beat him
with a bull's pizzle, stamped on his stomach, placed him in the hole (a damp
space under the stairs), then in the strong room.

Originally built to hold pirates, the strong room was just a few yards from the
prison's sewer. It was never cleaned, had no drain, no sunlight, no fresh air
-the smell was described as "noisome"-and was full of rats and sometimes
"several barrow fulls of dung". Several prisoners told the court that it
contained no bed, so that prisoners had to lie on the damp floor, possibly next
to corpses awaiting burial. But a group of favoured prisoners Acton had paid to
police the jail told the hearing there was indeed a bed. One of them said he
often chose to lie in there himself, because the strong room was so clean; the
"best room on the Common side of the jail", said another. This despite the
court's having heard that one prisoner's left side had mortified from lying on
the wet floor, and that a rat had eaten the nose, ear, cheek and left eye of
another.

Bliss was left in the strong room for three weeks wearing a skullcap (a heavy
vice for the head), thumb screws, iron collar, leg irons, and irons round his
ankles called sheers. One witness said the swelling in his legs was so bad that
the irons on one side could no longer be seen for overflowing flesh. His wife,
who was able to see him through a small hole in the door, testified that he was
bleeding from the mouth and thumbs. He was given a small amount of food but the
skullcap prevented him from chewing; he had to ask another prisoner, Susannah
Dodd, to chew his meat for him. He was eventually released from the prison, but
his health deteriorated and he died in St. Thomas's Hospital.
>>>

Like most prisons, since sometimes the entire family was moved to the prison,
as not doing so would mean paying for rent for jail and outside, small local
ecnomies would develop within the prison. They'd also be fined for breaking
some of the completely arbitrary rules the prison set up.

### Economics

The connections between different areas and influences on the economy can be
explored.

For a simple example, let's say that the usual passage between two cities is
very treacherous, so the merchandise between them is scarce and expensive, and
shipments are slow. The player can influence the situation in a number of ways

- Innovate along the existing path, such as by stealing merhandise and reselling
  it to make a quick buck, or stashing it away to drive up the price
- Innovate along the treacherous path, perhaps even for an unrelated problem
  (like the guy in Pokemon who digs a tunnel to see his girlfriend), thus
  drastically decreasing the shipment time and price of goods between both
  cities. This may also cause other unintended effects, such as
  - More migrants between cities or a mass migration away from an undesired
    environment or to a more desirable environment
  - More foreign goods in homes
  - Possible degradation of culture, becoming a tourist trap on the road to a
    larger attraction rather than a destination in and of itself
    
In either case, the price of goods would affect both the consumer and the
manufacturer, making one's fortunes to be much better and utterly crippling the
other
  
### An Escalating Magic Trick

An escalating magic trick where people suddenly start disappearing.

- It starts with a simple disappearing person's case. Mysterious, but not quite
  catching the public eye.
- The entire family vanishes. Their house looks like they were just there that
  morning, with all their belongings left behind, but is void of people.
- An entire community disappears. Their row of homes is eerily quiet, their
  interiors abandoned, and their businesses closed without notice, not even
  turning off the machines. Their absence is felt in the local economy.
- An entire people disappears. It is now a ghost town, with no leads on where
  they might've gone off to nor a reason.
- A disappearing city. The greatest magic trick of all. All the buildings and
  belongings are gone, like their people. The land is barren, with no roads and
  no trees. Just a barren landscape, making it hard to believe there were any
  people who lived there at all.
  
### Firefighters

The robber exchanges the contents of the fire extiguishers with something that
makes the flames worse. For example, gasoline filled extinguishers spread the
flames even faster, or filled with gaseous poison, spreading the toxin under the
cloud of smoke.

The idea is to use the procedures of the emergency responders to aid in our
nefarious purposes.

### A Floating City

A practical imagining of a floating city. A river flows downward from the
mountains, splitting into smaller rivers before they run over the cliff. The
multiple waterfalls over the face of the cliff and the mist from the water vapour
create the illusion of a floating city.

The city has also created a vast underground at the cliff and created large
waterwheels that turn and generate energy as long as the water flows.

The areas where the river widens are slow moving and shallow, and make popular
swimming areas. The deeper portions of the river are used as canals, but also
create a bridge aesthetic.

### The Insurance Scam

A failing jewelery store store hires you in a last ditch effort to steal their
jewels for insurance money. The idea is that you can keep all their inventory
that you'll resell on the black market, and they get insurance money, which
they will use to start a new life or business.

### The Lazy Boxer

Based on Nicolino Locche, he is a lazy boxer who tries his best to not train at
all, and thusly he has terrible punching power and doesn't really win often or
have many knockouts. However, he has perfected the art of dodging, so in most
fights, he just avoids their punches and either tries to get lucky with
counters or tires them out to get a tie.

In terms of gameplay, this mixes how I like dodging in Dark Souls while also
feeling weak and building up the endurance to fight much tougher opponents, and
adding in a light-hearted story reason for this gameplay.

### Living without a Trace

For our thieving main character, how do they keep a low profile? What precautions
have they made to evade detection and leave no evidence?

To leave no fingerprints, they have the option of wearing leather gloves, burning
off their fingerprints, or dipping their fingertips in wax.

To make plans and notes without leaving evidence, they write in their notebook
using a code. Caesar ciphers are easily broken, but perhaps a phonetic cipher,
like used in shorthand, is used. A blackboard may be used to draw maps, but are
always erased. This also helps in memorizing the area and creating a mental map.

### Locked Room Simulator

in order to pull off a certain job, the player must create a locked room mystery
to buy time for their escape. Perhaps there are multiple solutions based on how
they use the given items, and better solutions buy more time.

### Mass Flowering

Bamboo blossom is a natural phenomenon where bamboo will flower once over a very
long time period. Despite bamboo usually having a lifespan of aroun 40 to 80
years, the interval between flowering may be 65 to 120 years. The flowering 
typically occurs all at once regardless of location or climate conditions, and
then die. Since there are no environmental indicators, it's like the plant has
an alarm clock in each of the cells to signal all of them to wake up at the same 
time. The mechanism and evolutionary cause are a mystery.

### Murdering the Messiah

based off of Fred Hampton and Cointelpro.

AN especially charismatic person is spearheading a major revolutionary movement,
uniting the poor against the capitalist system across racial lines. The
government or capitalist authorities view the Messiah as the greatest threat and
believe they can kill the snake by cutting off its head. You are hired to murder
the Messiah. Alternatively, while investigating the Messiah and easily
infiltrating the group since you, yourself, are a poor person, the Messiah may
turn to you to hatch a different plan that would fake his death and bring him
back, turning him from the Messiah to the prophet.

### Murder: A Family Business

based off a dream Austin had. Sorry if it doesn't make sense, I wrote it down
while half asleep.

There were three siblings that worked together to specialize in murder. The
youngest brother was the one who did the actual killing. The older sister was
the surveillance specialist, learning the target's location and schedule. The
eldest brother specialized in cleaning up the body and the scene of the crime,
leaving no trace behind.

The police inspector who was tracing them always suspected them but never had
enough evidence to take them in.

Years later, the two older siblings have died, leaving the younger,
temperamental brother behind. He is also beginning to show early dementia. The
police inspector intends to use his memory loss and confusion to try to get a
confession.

The inspector leaves a conspicuous van that looks like the one his sister used
for surveillance outside his home. After a few weeks, he becomes confused about
whose van that is, and cannot remember how long it has been there. He also
consults his neighbours, but they thought it was his old van.

The police come visit him, pretending that his siblings are still alive. The
inspector tells him that they checked out his sister's van, and that they are
going to use it as evidence to put him and his siblings away. Since he is the
youngest, and they need more evidence, they are willing to give him a lesser
sentence should he agree to confess his crimes.

He is a little confused because he feels like he hasn't seen his sister in a
long time, and doesn't know where she is. The inspector says they'll prove it to
him, and they check out the van together. Inside the van are photographs and
maps for one the crimes they had done together. However, he notices a small
obscure mistake in one of the photographic reproductions (like a picture with
the wrong angle or lens, or the personal affects of the victim are incorrect,
like a dresser instead of the shoerack). This mistake makes him realize the van
is a ruse, and suddenly remembers his siblings are dead. He erupts in anger, and
the inspector panics, telling him that his siblings are actually in custody.

He doesn't listen, however, because he's not angry with the inspector. He's
angry at his siblings. He's angry that he's alone. They should have stayed. They
should have fought. They should be alive.

### Music Town

The basic idea is how the musicians in a town are affected changes the music you
hear while you're in town.

Perhaps the musicians have become too poor to maintain their instruments, or
they've been seized. As you begin to loot their instruments back, they start to
play in town again, and people start to come out more. The town becomes more
lively, there are people just hanging out and talking, and the music is
enhanced. As you help more and more musicians, the music becomes more complete
and the town comes back to life.

Alternatively, this may work in the opposite direction as well. Perhaps there
are some court musicians that are really conceited and annoying, so you steal
their instruments. Then as the musicians lose their livelihood, the soundscape
of the town dies down as the number of playing musicians diminishes until you
reduce it to absolute silence.

### Obsessed Artist

I like the idea of obsession. I find it kinda creepy. This is based on a
screenplay I wrote in highschool.

The basic concept was the artist loved someone very deeply, and something
happened to her (she left him or died). Since the artist can't get over her,
he begins to paint images of her endlessly so he can still see her.

In my variation, he paints her along the walls doing the things she normally
does around the house, and creates sculptures of her around the house
- One sitting at the dining table, ready to eat.
- One sitting outside, her arm arched like a bird is perched on her fingertips
- One sitting in the living room, ready to hold hands
- One bed, ready to sleep and cuddle

Continuing on with the obsession, he continues to repaint and resculpt her
everyday so that they can grow old together, the way he dreamed living out his
life. This was kinda taken from a magnet at my piano teacher's house that read,
"Grow old with me, The best is yet to be."

### Painted Town

the basic idea is that the town becomes slowly drawn in or painted in as things
in the town progress. The opposite could also be true, where the town begins to
become erased or thinned out as the town and its people deteriorate. This would
obviously be more of a fantasy setting or a dream-like sequence. The main point
would be to follow the development of the character arcs.

Some examples might be:

- A person who literally builds walls around them, becoming more and more of a
  recluse. This would be reflected in their character by becoming lonelier and
  increasingly isolated. They might be fearful of others and even of themselves,
  thus trying to block off all contact and hiding away the deepest part of
  themselves. This concept might be interesting if it were an old master thief
  who was amassing his fortune, but he lost sight of what was important to him,
  thus becoming increasingly paranoid and isolated.
- A person who is ageing and coming closer to the brink of death has their home
  slowly fading to white. This is reflected as their dementia becomes more
  apparent, and when they look at previously occupied parts of their home,
  like pictures and reminder notes, they become blank and are slowly erased from
  existence until all that is left is the little section they occupy, their
  immediate surroundings.
  
### A Painter's Daughter

based on a weird dream Austin had.

A young lady is growing a bed of sunflowers on her terrace along with some toys
and other objects ubiquitously placed. In the connecting studio, there is also
an unfinished painting of the scene. The edge of the canvas is noticably
discoloured due to age and there is a layer of dust that sits on top of the
frame.

The young lady's late father was a painter. When she was a little girl (perhaps
around ten years old), he was becoming more sickly, and was unable to paint the
long hours he used to. As the ambitious and optimistic girl she was, she would
work as his assitant and often help with the paintings under his instruction,
and her skill improved significantly over the years.

When she was a teenager, she came home and saw the unfinished painting of
sunflowers in the studio. However, her father was nowhere in sight. Worried that
he pushed himself too hard, as she had been busy with other trivial affairs of a
typical teenager, she rushed to the hospital, fearing the worst. She doesn't
remember much more from that day, only haunted by the memory of being taken to
the morgue and being asked to identify the man under the cloth. She remembers
his face, his pale skin that adhered closer to his skull, but unmistakably her
father.

Back at home, she recognized that the objects in the painting were some of her
old things, like toys and trinkets around the house that she had forgotten
about. The last few days before his passing, they had not been speaking a lot,
so she was taking this last painting as a sort of message to her. She thinks
that her father would want her to finish his last work, but she had no idea how
he could see the world outside their window in such a beautiful way when she
found it so dull. They didn't even have a bed of sunflowers there at the time,
just a single sunflower in her father's room. She also never learned the
technique he used, that layered colours in a very intricate way, so that even
the seeds of the sunflowers seemed like they were blooming with reds, oranges,
greens, and purples. Thus, she took the single sunflower and bred them,
recreating the scene on the terrace with the toys and trinkets, hoping that
she'll see what her father saw.

Of course, this is all a thin veil for her own insecurities. She blames herself
for neglecting her father in his last few days and blames herself for not taking
better care of him. She also believes that since she had been neglecting helping
her father, he felt the need to work harder and pushed himself too hard. Thus,
she feels perpetually unprepared and incapable of finishing the painting, which
would be her redemption.

There is also a nice symbolism here, because she is obviously the sunflower. To
her father, she is the light of his life, the little seed that bloomed into a
beautiful bed of flowers, along with all the memories that she keeps with her.
The beauty of the painting is a reflection of the beauty her father saw in her,
the same beauty that she is incapable of seeing in herself. It was never true
that she was unprepared and incapable of finishing the painting because for her
father, she was always enough. She was enough from the beginning.

The most obvious path of character development is that she finally accumulates
the technical skill required to finish the painting, after years of learning
from other artists, and it is obvious that she has become a decent artist in her
own right. Now she just needs the confidence and ability to forgive herself for
not taking care of her father, and finish their last painting together, and
finally accept what happened.

### Talent vs. Hard Work

based on the anime _Revue Starlight_ where a character is desparate to reach
the same heights as the others who are very talented in the field. However, they
refuse to believe that talent is important because, in their mind, that is
equivalent to giving up. However, this is not just ignorance, but a deeply
seeded fear. Instead, they constantly overwork themselves in order to compensate
for the difference, and yet, ultimately, never reach the same heights. Whether
they are able to accept this fate is up to them.

### Rats in the Walls

based on the short story _Rats in the Walls_ by H. P. Lovecraft where the sound
of rats in the walls leads to the discovery of an underground city beneath the
castle of the local lord where humans were farmed and bred into quadrapedal
humanoids to satisfy the family's thirst for human flesh. The city had long ago
been abandoned since the family died, and the rats came to feast on the rotting
flesh.

### Uzumaki

based on the comic _Uzumaki_ by Junji Ito where everything and everyone becomes
obsessed with, fear, and literally transform into spirals. 

You learn that there is some danger to the people of a town, you head there to
try to implore them hide in their homes. Of course, they don't listen. After all, who would listen to a shady thief? A foreign one at that. At this point,
you can let the danger rampage through town and watch them get slaughtered, and
then steal their stuff after, or you can try to find a different way to help
them. 

Perhaps you use your [saving ability](./mechanics.md#saving) multiple
times so they begin to see your death everywhere around the town and _encourage_
them to stay indoors. This saves them from the immediate danger, but now they
are too afraid to leave their homes. Eventually, the weak and ill begin to die,
and what else can the townsfolk do but to open their windows for just a second,
and slump the bodies over onto the streets. Look what you did. Maybe you're
better off not helping people at all. Maybe if you just leave them be for a
while, they'll start to open their doors again.

### When You Wish Upon a Star

What if, when you make a wish on a shooting star, the shooting star is
actually a banished angel from the heavens, and all of the wishes that are made
during their fall become their burdens? The angel is then only allowed to
return to heaven after they have fulfilled all the wishes. Many of the wishes
are impossible or too difficult to fulfill, and the fallen angels often die on
the surface as mortals.

The story of the angel starts in heaven, being taken out of their prison cell
for their sentencing. They see a small opening to escape and take it, stealing a
weapon off one of the guards and tries to escape. This is the player's first
taste of combat. Of course, the angel is quickly overpowered and stripped of
their wings and starts tumbling towards the surface.

As the angel tumbles toward the surface, we see some of the people who are
making wishes on the star, and they become the quests that the player must
complete to return to the heavens. However, the player is scheming and
cut-throat, and they find shortcuts or clever ways to complete the quest by
twisting the words of the wishes and often fulfill the wishes in the worst
ways imaginable.

### Yakuza series

The general theme for our game seems very dark. I feel like funny/comedic side 
quests could benefit the atmosphere of our game as a kind of 'break' from the 
main story. 

For instance in Yakuza 0, the main story revolves around the story of two
ex-yakuza caught up in a inner gang conflict over a plot of land worth 1 billion
yen. Many characters die and the player learns about graphic, dark secrets
behind that plot of land.

Meanwhile, various sidestories or sidequests involve:
1. Building a pocket circuit race car and becoming the fastest pocket circuit
   racer by beating kids.
2. Going to a telephone club and trying to score a hot date.
3. Joining a cult and stopping it from the inside
4. Helping Michael Jackson dance his way through town as zombie actors attack
   them.

A lot of the substories do not involve the main game mechanics (fighting) but
rather have different mechanics such as 1. and 2. Some substories only have
funny dialogue choices but still the story seems fulfilling. Others like 3 and 4
involve the main game mechanic.

I feel like we should not be limited by our game mechanics for the substories or
side quests. Escort missions, delivery missions, etc are generally boring but
can be made fun if there is an element of ridiculousness to them.
