#include "renderer.hpp"


/** Initializes `SDL_Renderer` and checks if initialization is successful.

The `SDL_Renderer` is created with the flags for 2D hardware acceleration and so
that the `SDL_Renderer.present()` method is automatically synchronized with the
refresh rate of the monitor.

\param title		name of the window.
\param width		width of the window, in pixels.
\param height		height of the window, in pixels.
*/
Renderer::Renderer(const std::string title, const int width, const int height) :
	_window(NULL),
	_width(width),
	_height(height),
	renderer(NULL)
{
	// TODO find best flags for creating window
	_window = SDL_CreateWindow(
		title.c_str(),
		SDL_WINDOWPOS_UNDEFINED,	// x position
		SDL_WINDOWPOS_UNDEFINED,	// y position
		_width,
		_height,
		SDL_WINDOW_SHOWN			// Flag for other options
	);

	if (_window == NULL) {
		SDL_LogError(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: failed to initialize window: %s",
			SDL_GetError()
		);
	}
	else {
		SDL_LogInfo(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: successfully initialized window"
		);
	}

	// -1 indicates to use the first available rendering driver
	renderer = SDL_CreateRenderer(
		_window,
		-1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
	);

	if (renderer == NULL) {
		SDL_LogError(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: failed to initialize renderer: %s",
			SDL_GetError()
		);
	}
	else {
		SDL_RenderSetLogicalSize(renderer, width, height);

		SDL_LogInfo(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: successfully initialized renderer"
		);
	}
}


/** Destroys the `SDL_Renderer` and checks if it destroyed successfully.
*/
Renderer::~Renderer() {
	if (renderer != NULL) {
		SDL_DestroyRenderer(renderer);
		renderer = NULL;

		SDL_LogInfo(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: successfully destroyed renderer"
		);
	}
	else {
		SDL_LogError(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: failed to destroy renderer: %s",
			SDL_GetError()
		);
	}

	if (_window != NULL) {
		SDL_DestroyWindow(_window);
		_window = NULL;

		SDL_LogInfo(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: successfully destroyed window"
		);
	}
	else {
		SDL_LogError(
			SDL_LOG_CATEGORY_RENDER,
			"Renderer :: failed to destroy window: %s",
			SDL_GetError()
		);
	}

}


/** Renders the objects in the `Scene`.

The `Renderer` is first cleared. The layers in the `Stage` are iterated in the
order they are sorted.

\param stage	the currently loaded stage.
*/
void Renderer::render(Stage& stage) {
	// Reset renderer and set draw colour to black
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

	// Iterate through all layers and render all entities in each layer
	for (const auto& layer: stage.layers) {
		for (const auto& id: layer) {
			renderEntity(id, stage);
		}
	}

	// Render visual novel on top
	if (stage.visual_novel.isRunning()) {
		renderVisualNovel(stage);
	}

	// Render menu on top
	if (stage.menu.isRunning()) {
		stage.menu.render(renderer);
	}

	SDL_RenderPresent(renderer);
}


/** Renders the entity sprite and its subentity, hitbox, and interaction box.

\param id		the entity ID.
\param stage	the currently loaded stage.
*/
void Renderer::renderEntity(const ID& id, Stage& stage) {
	// The position on the spritesheet
	SDL_Rect source = stage.scene.sprite(id).sprite;

	// The position on screen relative to the camera
	SDL_Rect target = {
		stage.scene.position(id).x - stage.camera.frame.x,
		stage.scene.position(id).y - stage.camera.frame.y,
		source.w,
		source.h
	};

	// Render the entity sprite
	if (stage.scene.property(id).at("symmetry") == 1) {
		DIRECTION direction = stage.scene.position(id).d;

		// Default direction is right
		double angle = 0;

		// Rotational pivot is centre of super entity
		SDL_Point centre = {source.w / 2, source.w / 2};

		// Most directions don't need a flip
		SDL_RendererFlip flip = SDL_FLIP_NONE;

		switch (direction) {
		case DIRECTION::SOUTH:
			break;

		case DIRECTION::WEST:
			angle = 90;
			flip = SDL_FLIP_HORIZONTAL;

			break;

		case DIRECTION::NORTH:
			angle = 180;

			break;

		case DIRECTION::EAST:
			angle = 270;

			break;
		}

		SDL_RenderCopyEx(
			renderer,
			stage.scene.sprite(id).spritesheet,
			&source,
			&target,
			angle,
			&centre,
			flip
		);

		// Render hitboxes
		for (const SDL_Rect& box: stage.scene.hitbox(id).hitbox(source, direction, centre)) {
			SDL_Rect hitbox = {
				target.x + box.x,
				target.y + box.y,
				box.w,
				box.h
			};

			SDL_RenderDrawRect(renderer, &hitbox);

			// Render the interaction box
			int dist = 2;
			SDL_Rect connectbox = {
				box.x + target.x - dist,
				box.y + target.y - dist,
				box.w + 2 * dist,
				box.h + 2 * dist
			};

			SDL_RenderDrawRect(renderer, &connectbox);
		}
	}
	else {
		SDL_RenderCopy(
			renderer,
			stage.scene.sprite(id).spritesheet,
			&source,
			&target
		);

		// Render the hitboxes
		for (const SDL_Rect& box: stage.scene.hitbox(id).hitbox(source)) {
			SDL_Rect hitbox = {
				target.x + box.x,
				target.y + box.y,
				box.w,
				box.h
			};

			SDL_RenderDrawRect(renderer, &hitbox);

			// Render the interaction box
			int dist = 2;
			SDL_Rect connectbox = {
				box.x + target.x - dist,
				box.y + target.y - dist,
				box.w + 2 * dist,
				box.h + 2 * dist
			};

			SDL_RenderDrawRect(renderer, &connectbox);
		}
	}

	// Render the subentity
	try {
		ID subentity = stage.scene.subentity(id);
		
		if (subentity != -1) {
			renderEntity(stage.scene.subentity(id), stage);
		}
	}
	catch(...) {
	}
}


/** Renders the textbox and text on the `Window`.

This method should only be called when the `Scene.show_text` flag is set
to `true`. The textbox currently takes up half the screen and is drawn
first. The text from the `Scene` is then resized to fit the textbox and
rendered.

\param msg		the message to be displayed.
*/
void Renderer::renderVisualNovel(Stage& stage) {
	// Render portraits
	for (ID id: stage.visual_novel.portraitIDs()) {
		SDL_RenderCopy(
			renderer,
			stage.scene.portrait(id).spritesheet,
			&stage.scene.portrait(id).sprite,
			&stage.scene.portraitPosition(id)
		);
	}

	// Render textbox
	SDL_Rect textbox = stage.visual_novel.textbox();

	SDL_SetRenderDrawColor(renderer, 100, 100, 100, 0);

	SDL_RenderFillRect(renderer, &textbox);

	// Render text
	SDL_RenderCopy(
		renderer, 
		stage.visual_novel.text(), 
		NULL, 
		&stage.visual_novel.textPosition()
	);
}
