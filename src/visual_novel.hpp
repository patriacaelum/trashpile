/** visual_novel.hpp

\verbatim
       ||          __  __  ___  ____   ___  ____  _____ ____
       --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
  -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
  -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
       --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
       ||
\endverbatim

The `VisualNovel` is the top layer on the stage that renders the text boxes and
character portraits.
*/


#ifndef SRC_VISUAL_NOVEL_HPP_
#define SRC_VISUAL_NOVEL_HPP_


#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "components/id.hpp"
#include "components/dialogue_script.hpp"
#include "globals.hpp"
#include "postal_service/subscription.hpp"


class Stage;


class VisualNovel : public Subscription {
public:
    VisualNovel();
    ~VisualNovel();

    void setRenderer(SDL_Renderer* renderer);

    void start(const ID speaker, const ID listener, Stage& stage);
    bool isRunning();
    void stop(Stage& stage);

    void notify(const EVENT event, const ID id, Stage& stage) override;
    void update(Stage& stage);

    SDL_Texture* text();
    SDL_Rect& textPosition();
    SDL_Rect& textbox();

    std::vector<ID> portraitIDs();

private:
    void renderText(const std::string text);
    void unloadText();

    bool _is_running;

    ID _speaker;
    ID _listener;

    DialogueScript* _script;

    SDL_Texture* _rendered_text;
    SDL_Rect _text_position;
    SDL_Rect _textbox;

    SDL_Renderer* _renderer;
    int _window_width;
    int _window_height;

    TTF_Font* _normal_font;
    std::string _normal_font_name;
    int _normal_font_size;
};

#endif // SRC_VISUAL_NOVEL_HPP_
