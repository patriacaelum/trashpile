/** graph.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The graph contains a collection of nodes and uses the A* algorithm to find the
shortest path between two nodes.

TODO: it may be required to change the vector to a deque so that negative values
can be handled dynamically, but this may come at a performance drop
*/


#ifndef SRC_PATHFINDING_GRAPH_HPP_
#define SRC_PATHFINDING_GRAPH_HPP_


#include <atomic>
#include <fstream>
#include <functional>
#include <queue>
#include <stdexcept>
#include <unordered_set>
#include <vector>

#include <SDL2/SDL.h>

#include "distance.hpp"
#include "graph_node.hpp"


class Graph {
public:
    Graph(const int w = 0, const int h = 0);

    GraphNode& at(const int x, const int y);
    std::vector<GraphNode> path() const;
    bool path_found() const;

    void draw();
    void find_path(
        GraphNode start, 
        GraphNode goal,
        std::function<float(GraphNode*, GraphNode*)> h,
        const SDL_Point start_size = {1, 1},
        const SDL_Point goal_size = {1, 1}
    );
    std::vector<GraphNode*> neighbours(
        const GraphNode* current, 
        const int dx = 1, 
        const int dy = 1
    );
    void resize(const int w, const int h);

private:
    float d(const GraphNode* current, const GraphNode* neighbour);
    GraphNode* pointer(const int x, const int y);
    GraphNode& reference(const int x, const int y);
    std::vector<GraphNode> reconstruct_path(GraphNode* goal);

    /// The width of the graph, or the number of nodes along the horizontal
    /// direction.
    int _w;
    /// The height of the graph, or the number of nodes along the vertical
    /// direction.
    int _h;

    /// The nodes are stored along a single horizontal vector instead of in a
    /// matrix.
    std::vector<GraphNode> _nodes;

    /// Set to `true` if a path has been found, `false` otherwise.
    std::atomic<bool> _path_found;
    /// The path in order from the start node to the goal node.
    std::vector<GraphNode> _path;
};


#endif /* SRC_PATHFINDING_GRAPH_HPP_ */
