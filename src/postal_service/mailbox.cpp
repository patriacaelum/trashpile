#include "mailbox.hpp"


/** Initializes the `Mailbox`.
*/
Mailbox::Mailbox() : _head(0), _tail(0), _event_queue(), _subscriptions() {
}


/** Posts an event to the event queue.

\param event    the event trigger.
\param id       the id of the entity posting the event.
*/
void Mailbox::post(const EVENT event, const ID id) {
    int tail = wrapIncrement(_tail);

    if (tail == _head) {
        SDL_LogWarn(
            SDL_LOG_CATEGORY_APPLICATION, 
            "Mailbox :: event queue is full, event=%d from id=%d dropped",
            static_cast<int>(event),
            id
        );
    }
    else {
        _event_queue[_tail] = event;
        _id_queue[_tail] = id;

        _tail = tail;
    }
}


/** Distributes the queued events to all subscribers.

Note that if the optional `subscription` parameter is specified, all posted
events are sent to that subscriber and that subscriber only, and should only be
specified if all other subscribers are meant to not receive any events.

\param stage            the currently loaded stage.
\param subscription     the subscribed entity. If this is not a `nullptr`, then
                        the distributed events will only be distributed to this
                        subscription.
*/
void Mailbox::distribute(Stage& stage, const Subscription* subscription) {
    while (_head != _tail) {
        EVENT event = _event_queue[_head];
        ID id = _id_queue[_head];

        if (id != -1) {
            distributeFor(event, id, id, subscription, stage);
        }

        distributeFor(event, -1, id, subscription, stage);

        _head = wrapIncrement(_head);
    }
}


/** Subscribe to an event.

If the `id` parameter is specified, then the subscriber will only be notified
when the specified entity posts the specified event. If the `id` parameter is
not supplied, then the subscriber will be notified of the specified event no
matter which entity posted the event.

\param subscription     the subscribing entity.
\param event            the event trigger.
\param id               (optional) the id of the posting entity.
*/
void Mailbox::subscribe(
    Subscription* subscription, 
    const EVENT event, 
    const ID id
) {
    _subscriptions[event][id].push_back(subscription);
}


/** Unsubscribe to an event.

\param subscription     the subscribed entity.
\param event            the event trigger.
\param id               (optional) the id of the posting entity.
*/
void Mailbox::unsubscribe(
    const Subscription* subscription,
    const EVENT event,
    const ID id
) {
    std::vector<Subscription*>& subs = _subscriptions.at(event).at(id);

    subs.erase(std::remove(subs.begin(), subs.end(), subscription), subs.end());
}


/** Clears the event queue and all subscriptions.
*/
void Mailbox::reset() {
    _head = 0;
    _tail = 0;
    _subscriptions.clear();
}


/** Distributes an event from a specified poster.

Note that if the `special_sub` parameter is not a `nullptr`, all posted events
are sent to that subscriber and that subscriber only, and should only be
specified if all other subscribers are meant to not receive any events.

\param event            the event trigger.
\param list_id          the id of the subscription list.
\param poster_id        the id of the entity that posted the event.
\param special_sub      a subscribed entity.
\param stage            the currently loaded stage.
*/
void Mailbox::distributeFor(
    const EVENT event, 
    const ID list_id, 
    const ID poster_id, 
    const Subscription* special_sub,
    Stage& stage
) {
    try {
        for (Subscription* subscription: _subscriptions.at(event).at(list_id)) {
            if (special_sub == nullptr || special_sub == subscription) {
                subscription->notify(event, poster_id, stage);
            }
        }
    }
    catch (std::out_of_range& error) {
        SDL_LogDebug(
            SDL_LOG_CATEGORY_APPLICATION,
            "Mailbox :: no subscriptions to notify for event=%d from id=%d: %s",
            static_cast<int>(event),
            poster_id,
            error.what()
        );
    }
}


/** Used to wrap the event queue.

\param x    the current index of the event queue.

\returns    the incremented index. If the increment is the end of the queue,
            then the index is wrapped to zero.
*/
int Mailbox::wrapIncrement(const int x) {
    int y = x + 1;

    if (y == PROPERTIES::MAX_ENTITIES) {
        y = 0;
    }

    return y;
}