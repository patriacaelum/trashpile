#include "graph.hpp"


/** A graph is a collection of nodes and can find the shortest path along those
nodes.

\param w
    the width of the graph, or the number of nodes along the horizontal
    direction.
\param h
    the height of the graph, or the number of nodes along the vertical
    direction.
*/
Graph::Graph(const int w, const int h) : 
    _w(w),
    _h(h),
    _nodes({}),
    _path({})
{
    resize(w, h);
}


/** Returns the node at the specified location in the graph.

\param x
    the horizontal position of the node.
\param y
    the vertical position of the node.

\returns
    a reference to the `GraphNode` at the specified location in the graph.
*/
GraphNode& Graph::at(const int x, const int y) {
    if (x < 0 || x >= _w || y < 0 || y >= _h) {
        SDL_LogError(
            SDL_LOG_CATEGORY_ERROR,
            "Pathfinding :: index (x=%d, y=%d) is out of range for graph size (w=%d, h=%d)",
            x, y,
            _w, _h
        );

        throw std::out_of_range("Pathfinding :: index is out of range");
    }

    return _nodes[_h * x + y];
}


/** Returns the path if one has been found.

\returns
    a vector of nodes in the order of the found path. If no path has been found,
    an empty vector is returned.
*/
std::vector<GraphNode> Graph::path() const {
    if (path_found()) {
        return _path;
    }

    return {};
}


/** Checks if a path has been found in the current graph.

\returns
    `true` if a path has been found, `false` otherwise.
*/
bool Graph::path_found() const {
    return _path_found.load();
}


/** A* finds a path from start to goal.

The `dx` and `dy` parameters are used for entities that are larger than a single
node, extending in the positive x and y directions.

\param start
    the first node in the path. This should be the top left corner of the entity
    moving toward the target.
\param goal
    the last node in the path. This should be the top left corner of the target
    area.
\param h
    the heuristic function. `h(n, goal)` estimates the cost to reach `goal` from
    node `n`.
\param start_size
    defines the number of nodes extending in the horizontal and vertical
    directions that needs to be traversable. The default value is `{1, 1}` for
    the entity that is moving toward the target.
\param goal_size
    defines the number of nodes extending in the horizontal and vertical
    directions that defines the area of the target entity. The default value is
    `{1, 1}` for the target.
*/
void Graph::find_path(
    GraphNode start, 
    GraphNode goal, 
    std::function<float(GraphNode*, GraphNode*)> h,
    const SDL_Point start_size,
    const SDL_Point goal_size
) {
    GraphNode* start_node = pointer(start.x, start.y);
    GraphNode* goal_node = pointer(
        goal.x + (goal_size.x / 2),
        goal.y + (goal_size.y / 2)
    );

    SDL_Rect current_box = {
        start_node->x,
        start_node->y,
        start_size.x,
        start_size.y
    };
    const SDL_Rect goal_box = {
        goal_node->x - (goal_size.x / 2),
        goal_node->y - (goal_size.y / 2),
        goal_size.x,
        goal_size.y
    };

    // `open_set` is the set of discovered nodes that may need to be
    // (re-)expanded. Initially, only the `start` node is known. This is usually
    // implemented as a min-heap or priority queue rather than a hash-set.
    std::unordered_set<GraphNode*> open_set = {start_node};

    std::priority_queue<GraphNode*, std::vector<GraphNode*>, NodeComparator> open_queue;
    open_queue.push(start_node);

    start_node->g = 0;
    start_node->h = h(start_node, goal_node);

    while (!open_queue.empty()) {
        // This operation can occur in O(log(N)) time if `open_set` is a
        // min-heap or a priority queue.
        GraphNode* current = open_queue.top();

        current_box.x = current->x;
        current_box.y = current->y;

        // TODO instead of checking if the current node is equal to the goal
        // node, check if the current node + start size intersects with goal
        // node + goal size
        if (SDL_HasIntersection(&current_box, &goal_box)) {
            SDL_LogInfo(
                SDL_LOG_CATEGORY_SYSTEM,
                "Pathfinding :: successfully found path from node (x=%d, y=%d) to node (x=%d, y=%d)",
                start.x, start.y,
                goal.x, goal.y
            );

            _path_found.store(true);
            _path = reconstruct_path(current);

            return;
        }

        open_queue.pop();
        open_set.erase(current);

        for (GraphNode* neighbour: neighbours(current, start_size.x, start_size.y)) {
            // `tentative_g_score` is the distance from `start` to `neighbour`
            // through `current`.
            float tentative_g_score = current->g + d(current, neighbour);

            if (tentative_g_score < neighbour->g) {
                // This path to `neighbour` is better than any previous one.
                // Record it!
                neighbour->came_from = current;
                neighbour->g = tentative_g_score;
                neighbour->h = h(neighbour, goal_node);

                if (open_set.find(neighbour) == open_set.end()) {
                    // `neighbour` not in `open_set`
                    open_queue.push(neighbour);
                    open_set.emplace(neighbour);
                }
            }
        }
    }

    // `open_set` is empty but `goal` was never reached.
    SDL_LogInfo(
        SDL_LOG_CATEGORY_SYSTEM,
        "Graph :: failed to find path from node (x=%d, y=%d) to node (x=%d, y=%d)",
        start.x, start.y,
        goal.x, goal.y
    );
    _path_found.store(true);
    _path = {};

    return;
}


/** Returns the neighbouring nodes of the given node.

NOTE: at the moment, only the next nearest nodes in the four cardinal directions
are considered neighbours.

TODO: If diagonal input is allowed, then this method will have to be expanded.

\param current
    the current node.
\param dx
    the number of nodes extending in the horizontal direction that needs to be
    traversable. The default value is `1`.
\param dy
    the number of nodes extending in the vertical direction that needs to be
    traversable. The default value is `1`.

\returns
    a vector of neighbouring nodes.
*/
std::vector<GraphNode*> Graph::neighbours(
    const GraphNode* current, 
    const int dx,
    const int dy
) {
    const int x_min = current->x;
    const int x_max = x_min + dx;
    const int y_min = current->y;
    const int y_max = y_min + dy;

    std::vector<GraphNode*> nodes;

    // Right nearest neighbour
    if (x_max < _w) {
        bool traversable = true;

        for (int y = y_min; y < y_max; ++y) {
            if (!reference(x_max, y).traversable) {
                traversable = false;

                break;
            }
        }

        if (traversable) {
            nodes.push_back(pointer(x_min + 1, y_min));
        }
    }

    // Left nearest neighbour
    if (x_min > 0) {
        const int x = x_min - 1;
        bool traversable = true;

        for (int y = y_min; y < y_max; ++y) {
            if (!reference(x, y).traversable) {
                traversable = false;

                break;
            }
        }

        if (traversable) {
            nodes.push_back(pointer(x_min - 1, y_min));
        }
    }

    // Downward nearest neighbour
    if (y_max < _h) {
        bool traversable = true;

        for (int x = x_min; x < x_max; ++x) {
            if (!reference(x, y_max).traversable) {
                traversable = false;

                break;
            }
        }

        if (traversable) {
            nodes.push_back(pointer(x_min, y_min + 1));
        }
    }

    // Upward nearest neighbour
    if (y_min - 1 > 0) {
        const int y = y_min - 1;
        bool traversable = true;

        for (int x = x_min; x < x_max; ++x) {
            if (!reference(x, y).traversable) {
                traversable = false;

                break;
            }
        }

        if (traversable) {
            nodes.push_back(pointer(x_min, y_min - 1));
        }
    }

    return nodes;
}


/** Resizes the graph.

\param w
    the width of the graph, or the number of nodes along the horizontal
    direction.
\param h
    the height of the graph, or the number of nodes along the vertical
    direction.
*/
void Graph::resize(const int w, const int h) {
    // Clear existing nodes
    _nodes = {};

    // Set internal parameters
    _w = w;
    _h = h;
    _path_found.store(true);

    // Create nodes
    for (int x = 0; x < _w; ++x) {
        for (int y = 0; y < _h; ++y) {
            _nodes.push_back(GraphNode(x, y));
        }
    }
}


/** The weight of the edge between the two nodes.

The weight of the edge added to the g score and can be used to indicate
difficulty in traversing from one node to another.

NOTE: at the moment, the weight of every edge is currently set to `1`.

\param current
    the left node.
\param neighbour
    the right node.

\returns
    the weight of the edge between the two nodes.
*/
float Graph::d(const GraphNode* current, const GraphNode* neighbour) {
    return 1;
}


/** Returns a pointer to the node at the specified location in the graph.

This is similar to the `at()` method, but this meant to only be used used
internally to set the `came_from` parameter of a `GraphNode` when finding a
path.

\param x
    the horizontal position of the node.
\param y
    the vertical position of the node.

\returns
    a pointer to the `GraphNode` at the specified location in the graph.
*/
GraphNode* Graph::pointer(const int x, const int y) {
    return &_nodes[_h * x + y];
}


/** Returns a reference to the node at the specified location in the graph.

This is similar to the `at()` method, but this method is meant to be used
internally only and the parameters specify the location in the graph rather than
the location in the map.

\param x
    the horizontal position of the node.
\param y
    the vertical position of the node.

\returns
    a reference to the `GraphNode` at the specified location in the graph.
*/
GraphNode& Graph::reference(const int x, const int y) {
    return _nodes[_h * x + y];
}


/** Reconstructs the path from the goal node.

It is assumed that the each node along the path has its `came_from` parameter
filled pointing to another node. It is also assumed that the `came_from`
parameter of the start node is `nullptr`.

\param goal
    the last node in the path.

\returns
    a vector of nodes in the order of the path found.
*/
std::vector<GraphNode> Graph::reconstruct_path(GraphNode* goal) {
    std::vector<GraphNode> path;

    GraphNode current = (*goal);

    while (current.came_from != nullptr) {
        path.push_back(current);

        current = (*current.came_from);
    }

    // Add the start node
    path.push_back(current);

    std::reverse(path.begin(), path.end());

    // Assert path is all traversable
    for (GraphNode node: path) {
        if (!node.traversable) {
            SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Graph :: node (x=%d, y=%d) is not traversable!", node.x, node.y);
        }
    }

    return path;
}


void Graph::draw() {
    std::ofstream file;
    file.open("graph.txt");
    for (int x = 0; x < _w; ++x) {
        for (int y = 0; y < _h; ++y) {
            GraphNode& node = reference(x, y);
            file << x << "," << y << "," << node.traversable << "," << node.g << "," << node.h << "\n";
        }
    }
    file.close();
}
