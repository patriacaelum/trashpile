/** path.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Path` defines where an entity needs to move.
*/


#ifndef SRC_COMPONENTS_PATH_HPP_
#define SRC_COMPONENTS_PATH_HPP_


#include <queue>

#include <SDL2/SDL.h>


using Path = std::queue<SDL_Point>;


#endif /* SRC_COMPONENTS_PATH_HPP_ */
