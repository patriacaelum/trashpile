#include "dialogue_script.hpp"


std::string DialogueScript::nextLine(std::string choice) {
    nextNode(choice);

    while (nodes[current_node_id].node_type == "setter") {
        nextNode();
    }

    return nodes[current_node_id].text;
}


std::string DialogueScript::currentLine() {
    return nodes[current_node_id].text;
}


void DialogueScript::reset() {
    current_node_id = first_node_id;
}


void DialogueScript::nextNode(std::string choice) {
    ID next_id = current_node_id;

    if (current_node_id != last_node_id) {
        DialogueNode current_node = nodes[current_node_id];
        std::string node_type = current_node.node_type;

        if (node_type == "choices") {
            int index = std::find(
                current_node.choices.begin(),
                current_node.choices.end(),
                choice
            ) - current_node.choices.begin();
            next_id = current_node.next_id[index];
        }
        else {
            if (node_type == "setter") {
                setValue(current_node.key, current_node.value);
            }

            // Both line and setter nodes should only have one next id
            next_id = current_node.next_id[0];
        }
    }

    current_node_id = next_id;
}


void DialogueScript::setValue(std::string key, std::string value) {

}
