/** pathfinding_graph_node.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for a graph node in the pathfinding module.
*/


#include <catch2/catch.hpp>

#include "../src/pathfinding/graph_node.hpp"


TEST_CASE("Pathfinding graph node tests", "[pathfinding][node]") {
    const int x = 1;
    const int y = 2;
    const bool traversable = true;

    const float g = 3.5;
    const float h = 7.13;

    GraphNode node = GraphNode(x, y, traversable);
    node.g = g;
    node.h = h;

    SECTION("Pathfinding graph node constructor") {
        REQUIRE(node.x == x);
        REQUIRE(node.y == y);
        REQUIRE(node.traversable == traversable);
    }

    SECTION("Pathfinding graph node f score") {
        REQUIRE(node.f_score() == g + h);
    }
}
