#include "alert.hpp"


Alert::Alert(const ID id, const JSON& data, Stage& stage)
:   Pathfinding("alert", false, {STATES::GOTO}, id, data)
{}


/** When entering the alert state, the pathfinding is set up to run on a
separate thread over the animation.

\param stage
    the currently loaded stage.
*/
void Alert::enter(Stage& stage) {
    Pathfinding::enter(stage);

    startPathfinding(stage);
}


/** When exiting the alert state, the target entity is reset.

\param stage
    the currently loaded stage.
*/
void Alert::exit(Stage& stage) {
    _reference_id = -1;

    Pathfinding::exit(stage);
}


State* Alert::update(Stage& stage) {
    // If the animation is on the last frame
    if (_hframes == _max) {
        if (_graph.path_found()) {
            setPath(stage);

            _finished = true;

            // TODO sometimes the pathfinding fails
            if (stage.scene.path(id).empty()) {
                return nullptr;
            }

            return stage.scene.pda(id)[STATES::GOTO];
        }
    }

    return Pathfinding::update(stage);
}


/** Sets the goal location from the connection and disconnects.

\param stage
    the currently loaded stage.
*/
void Alert::setGoal(Stage& stage) {
    const SDL_Rect map = stage.scene.mapSize();
    const Position position = stage.scene.position(_reference_id);
    const SDL_Rect hitbox = stage.scene.hitbox(_reference_id).unionbox();

    _goal = {
        (position.x - map.x + hitbox.x) / _scale,
        (position.y - map.y + hitbox.y) / _scale,
        (hitbox.w / _scale) + 2,
        (hitbox.h / _scale) + 2
    };
}
