/** speak.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The speaking state uses the `Dialogue` component to decide which text to
display. This state is meant to be connected to another entity that is in the
`Listen` state.

The state process is

```
Speak <-> Listen
```
*/


#ifndef SRC_STATES_SPEAK_HPP_
#define SRC_STATES_SPEAK_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../components/velocity.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Speak : public State {
public:
	Speak(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_SPEAK_HPP_ */
