#include "door_open.hpp"


/** Creates a state that is open.

This state moves its connected entity to the exit location.

\param exit		the `Position` of where the connected entity is moved to.
 */
DoorOpen::DoorOpen(const ID id, const JSON& data, Stage& stage)
:	State("door_open", false, {}, id, data),
	_exit(data.at("exit").get<Position>())
{}


/** Updates the state.

This is a one frame state.

\param input
	a global input value.

\returns
	a null pointer.
 */
State* DoorOpen::update(Stage& stage) {
	_finished = true;

	const Connector& connector = stage.scene.connector(id);

	if (connector.isConnected()) {
		const ID connection = connector.connection();

		stage.scene.position(connection) = _exit;
		stage.scene.pda(connection).notify(EVENT::DISCONNECT, id, stage);

		stage.disconnect(id, connection);
		
		// Lie and post the event from the entity moving through the door
		stage.mailbox.post(EVENT::ROOM_CHANGE, connection);
	}

	return nullptr;
}
