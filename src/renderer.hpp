/** renderer.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Renderer` creates the window where everything is rendered and controls how
they are rendered. The entities are rendered in relation to the `Camera`.
*/


#ifndef SRC_RENDERER_HPP_
#define SRC_RENDERER_HPP_


#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "globals.hpp"
#include "scene.hpp"
#include "stage.hpp"


class Renderer {
public:
	Renderer(
		const std::string title = "trashpile",
		const int width = 1280,
		const int height = 720
	);
	~Renderer();

	void render(Stage& stage);

	/// The SDL_Renderer that renders textures to the window.
	SDL_Renderer* renderer;

private:
	void renderEntity(const ID& id, Stage& Stage);
	void renderVisualNovel(Stage& stage);
	void renderTextbox();

	/// The window where things are rendered.
	SDL_Window* _window;

	/// Size of the window (in pixels)
	int _width;
	int _height;
};


#endif /* SRC_RENDERER_HPP_ */
