/** pursue.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Pursue` state locks onto a target entity and pursues them until they are
out of the seeing range.
*/


#ifndef SRC_STATES_PURSUE_HPP_
#define SRC_STATES_PURSUE_HPP_


#include "pathfinding.hpp"

#include "../components/id.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Pursue : public Pathfinding {
public:
    Pursue(const ID id, const JSON& data, Stage& stage);

    void enter(Stage& stage) override;
    void exit(Stage& stage) override;

    State* update(Stage& stage) override;

private:
    void setGoal(Stage& stage) override;
    bool in_seeing_range(Stage& stage);
};


#endif /* SRC_STATES_PURSUE_HPP_ */
