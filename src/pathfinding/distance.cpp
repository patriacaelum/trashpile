#include "distance.hpp"


/** The manhattan distance is the sum of the distances between two points in all
dimensions.

The manhattan distance between nodes `n1` and `n2` is defined as
`|x_n2 - x_n1| + |y_n2 - y_n1|`.

\param start    the start node.
\param goal     the goal node.

\returns        the manhattan distance between the start and goal nodes.
*/
float manhattan_distance(const GraphNode* start, const GraphNode* goal) {
    return abs(goal->x - start->x) + abs(goal->y - start->y);
}


/** The octinal distance is maximum distance between two points along a single
dimension.

The octinal distance is similar to the manhattan distance with the addition that
traversing along the diagonal is allowed. The octinal distance between nodes
`n1` and `n2` is defined as `max(|x_n2 - x_n1|, |y_n2 - y_n1|)`.

\param start    the start node.
\param goal     the goal node.

\returns        the octinal distance between the start and goal.
*/
float octinal_distance(const GraphNode* start, const GraphNode* goal) {
    return std::max(abs(goal->x - start->x), abs(goal->y - start->y));
}
