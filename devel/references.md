# General

- [Game Developers Conference](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ)
  are long talks by game developers about the games they've worked on. It's kind
  of like Ted Talks for video games.
- [Joseph Anderson](https://www.youtube.com/channel/UCyhnYIvIKK_--PiJXCMKxQQ)
  creates long format videos on video game critiques, focusing mostly gameplay
  and story.
- [Mark Brown](https://www.youtube.com/channel/UCqJ-Xo29CKyLTjn6z2XwYAw) has
  short ten minute videos on video game design.
- [Matthew Matosis](https://www.youtube.com/channel/UCb_sF2m3-2azOqeNEdMwQPw)
  creates in-depth reviews analysis on games.
- [snomaN Gaming](https://www.youtube.com/channel/UCmY2tPu6TZMqHHNPj2QPwUQ) has
  short 10 minute videos on games and their general design.
- [Turbo Button](https://www.youtube.com/channel/UCWPTiFpzm8559H-9Err59gw)
  creates short essays on specific mechanics in video games.

- [Tips](http://rampantgames.com/blog/?p=4028) on making interesting sidequests 
