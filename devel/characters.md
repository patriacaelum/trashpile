# Character Building

This is a list of questions you might ask yourself when creating a character.
Not all of the answers would show up explicitly in the character dialogue or
become a part of the story, but it is helpful for worldbuilding and informs
environmental storytelling. 

Try to answer the questions with as much detail as you can. The answers should read more like little stories or anecdotes because that's how we would tell
someone else about our own lives. Also, the questions are written with male
pronouns mostly for the fact that "he" is 30% shorter than "she."

- It's the first time you've laid eyes on him.
  - What is he wearing? Why does he wear those clothes? Does he wear something
    else at home?
  - What is his stature? Short or tall? How does this inform his worldview?
  - What is his posture? Is he slouching? Does he tend to lean on things?
  - How does he fidget? With his hands? His hair? His feet? Complete stillness?
- He starts to talk about himself.
  - Where does he live? How did he get there? Does he want to stay there?
  - What does he do for work? Does he like it? How does it pay?
  - How does he use his money? Does he have dependents?
  - What does he do in his spare time? What are his hobbies? Does he collect
    something? Any hobbies he's abandoned?
  - How are his relationships? To his parents? His grandparents? Extended
    family? Does he have children? Or want children? Friends? Neighbours? How
    does he maintain these relationships?
  - What are his hopes and dreams? Fears and tribulations? How does he imagine
    his future? With optimism or pessimism? How do others view his future? Is
    he in denial? Is he driven to prove something?
- He starts telling you his life story.
  - Where was he born? What was his childhood like? School? Work?
  - What are some of his fondest memories? His first love? Most memorable
    adventure?
  - What are some of his most traumatic memories? How do his current fears and
    anxieties reflect these? Is he even aware they still affect his life to
    this day?
  - What did he do in his early adulthood? Did he find someone and settle? Or
    never stopped looking? Or gave up completely? How did he find his current
    friends? Does he still see them often?
  - Did he have a midlife crisis? What's changed since he's worked for many
    years? How many places has he travelled? Has he ever been unfaithful? Has
    he grown bitter over the years? Or sweeter? What are some of the nagging
    issues or thoughts he simply refuses to acknowledge and address?
  - When did he retire? Why did he retire? How does he spend his time, now
    that he's not working? What is he most proud of? What does he deeply
    regret? Does he want to amend for his sins?
  - How does he imagine dying? Does he want to die? Is he lonely?

# Characters
Since we have not entirely figured out our game mechanics yet alone the story, 
I shall only make suggestions towards what we should consider when creating the
characters.

## Main Character
Like any good story, our main character should have some sort of character 
development. It should probably follow these acts:

* First act: Our character has a set of established relationships with other
  characters and their surrounding environment. However, this will soon be
  challenged by an external threat, introduced in a dramatic way. For instance
  in Harry Potter, this will be the moment when he found out he was a wizard and
  Voldemort was planning to destroy the wizarding world. In Mass Effect, it
  would be when they first discovered the existence of the Reapers. This first
  dramatic reveal often does not show the magnitude of the entire problem yet.
  Our hero, for whatever reason, is determined to solve this problem. However,
  at this point they will not have the mean to do so yet.

* Second act: Our character goes on adventures to better themselves, acquiring
  skills, friends, weapons, and finding means to solve the problem. They will
  discover the problem is actually much worse than what was originally thought.
  I believe this act is when our character becomes more self-aware, makes
  morally questionable choices, and is forced to change in order to solve the
  problem.

* Third act: This is the point where all the plots and subplots of the story
  resolve. Our character, now with new knowledge, experience, skills, and
  friends comes to resolve the final problem. At this point the character will
  be completely different from who they were at the beginning of the game. This
  I believe should be noticable should you play the game for a second time.

This character development acts should also be "the player development acts" as
they play the game. One of the things I enjoyed playing Dragon Age 3 was how at
the beginning of the game, I myself as a player did not know anything about the
world, just like the main character being thrown into the mess unwillingly. I
was making not fully well-informed decisions, asking my companions for their
opinions on everything, and dealing with undesired consequences. But as the game
progress, I learned more about the world and was much more confident on my
choices and actions. The game also reinforces this in term of dialogues. In the
beginning you have to be quite polite and hesistant. You ask someone nicely to
help you on something. Your companions can order you around. Towards the end, my
character was giving oders, making sarcastic comments, and taunting the enemies,
etc. It was quite noticable when I played the game the second time around and I
really enjoyed it.

### Kito

She has very low self-value. She was born to be a nobody, and she doesn't
exist. She has lived her life like she is a pest, like a mosquito. Her name
might be something soft and rolls off the tongue when her mother softly
whispers her name, but now, she calls herself Kito.

After the main story when she loses her memory and is successful in erasing
the traces of herself and her family, people who had a faint memory of her
parents have inventive stories about her life that are surprisingly accurate.
Gossip around her might sound like,

"Did you hear about the rumour that the one of the leaders of the patriot
revolution was married to a famous painter from the winterlands? For some
reason, a lot of people from the previous generation have a collective memory
of who they were. They remember seeing their faces in newspapers and remember
seeing the paintings in the national museum, but for some mysterious reason,
they disappeared from history. It's said that the patriot got hurt while
fighting in the revolution and retired to the countryside with his wife, and
they had two kids, but they both died to scarlet fever, which was going around
at the time."
"I think I heard of this too. My father said that they actually had a third
child, but was undocumented for some reason.  And he says that it's that third
child who went around erasing their existence from the world as some sort of
revenge for hurting their parents. But nobody knows if it was a boy or girl, or
if they were younger or older than the two kids we know about. My dad also
said that after both of their kids died, the wife left the patriot and he got
really sick and that he must've died from a broken heart. It was funny that he 
clutched his chest as he said that."
"Typical men. You kids' fathers don't know what them's talking about. He
didn't get sick after she left him. _He_ left _her_. _After_ he got sick. The 
reason? Everyone who knows anything knows that if your spouse has a chronic
illness, their debts are passed onto you. But _they_ found out early and used
their dead babies as the excuse for divorce. That way, he could drown in debt
and she could continue her life without being thrown in her debter's prison.
That's what I would've done, anyways. Only a woman would know. I even figured
out the part about the missing third kid. I bet she does exist. She's definitely
a girl, otherwise they wouldn't hide her, even if she were born out of
wedlock."
"What does that mean?"
"It means, kid, that was born when the two weren't married.  Everyone who
knows anything knows that if an unmarried woman has a child, she is thrown
out of her village and shunned from the church. Basically committing suicide.
Life is fright with danger when you are a woman. But the tricky part is
figuring out her age. She could've been born before they were married, or after
they divorced."

## What we should have in our game
Keeping in mind that our game would a short one, and there will not be that many
opportunities for complex character development, I suggest a few indicators that
would help player experience the growth of their character:


* Gain a reputation. If you repeatedly choose to rob people's home, you can hear
  people talking about a theft (that fits your character's appearances).
  Commonfolks might be more on guard when you are around. Criminals might
  approach you for side-quests.
* Change in physical appearance. Lose an arm during a battle. Gain a few scars.
* Change in dialogue. Subtle changes over time on how your character talks and
  other characters address them.
* Have the ability to make friends or enemies with certain NPCs or have the view
  of existing friends change as you play the game. However, I don't mean it in
  this way: if you kill someone all your friends will disapprove you for it and
  become less friendly (that would encourage/force the player to keep making
  nice decisions). It should be so that each action you do, some of your friends
  will like it and others will not. Say if you rob a family, someone will say
  that is immoral, others will think you're practical.

## Side Characters
- Spoiler Owl: an annoying owl like in Zelda that tells you spoilers of various media despite your best efforts to shoo him away.
