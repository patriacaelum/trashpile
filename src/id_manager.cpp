#include "id_manager.hpp"


/** Sets all the IDs ready for distribution.
*/
IDManager::IDManager() {
	reset();
}


/** Get an available ID.

\param name		the name of the entity.

\returns		the next available ID.
*/
ID IDManager::add(const std::string name) {
	if (_nids == _max) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"IDManager :: maximum number of IDs created"
		);

		throw std::overflow_error(
			"IDManager :: maximum number of IDs created"
		);
	}
	else if (_names_to_ids.find(name) != _names_to_ids.end()) {
		SDL_LogError(
			SDL_LOG_CATEGORY_APPLICATION,
			"IDManager :: name '%s' already exists",
			name.c_str()
		);

		throw std::runtime_error("IDManager :: name already exists");
	}

	ID id = _available.front();
	_available.pop();

	_ids.set(id);
	++_nids;

	_names_to_ids[name] = id;
	_ids_to_names[id] = name;

	return id;
}


/** Adds the ID back to the availability list.

When removing an ID, it is assumed that any components that relating to that
entity has been destroyed.

\param id		the ID to be added to the availability list.
*/
void IDManager::remove(const ID id) {
	if (_ids.test(id)) {
		std::string name = _ids_to_names[id];

		_ids_to_names.erase(id);
		_names_to_ids.erase(name);

		--_nids;
		_ids.reset(id);

		_available.push(id);
	}
}


/** Resets all the IDs.
*/
void IDManager::reset() {
	_nids = 0;

	_ids.reset();

	_available = std::queue<ID>();

	_names_to_ids.clear();
	_ids_to_names.clear();

	for (ID id = 0; id < _max; ++id) {
		_available.push(id);
	}
}


/** Determine if the ID is in use.

\param id		the ID in question.

\returns		`true` if the ID is in use, `false` otherwise.
*/
bool IDManager::test(const ID id) const {
	return _ids.test(id);
}


/** Finds the ID of the entity with the given name.

\param name		the name of the entity.

\returns		the ID of the entity.
*/
ID IDManager::id(const std::string name) const {
	return _names_to_ids.at(name);
}


/** Finds the name of the entity with the given ID.

\param id		the ID of the entity.

\returns		the name of the entity.
*/
std::string IDManager::name(const ID id) const {
	return _ids_to_names.at(id);
}
