/** input_manager.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `InputManager` takes player input and translates it into game inputs
that are processed by the game engine.

By default, the keyboard binds to standard controller buttons:

- Z -> A
- X -> B
- A -> X
- S -> Y
*/


#ifndef SRC_INPUT_MANAGER_HPP_
#define SRC_INPUT_MANAGER_HPP_


#include <iostream>
#include <SDL2/SDL.h>

#include "globals.hpp"


class InputManager {
public:
	EVENT getInput();
};


#endif /* SRC_INPUT_MANAGER_HPP_ */
