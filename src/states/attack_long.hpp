/** attack_long.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

Creates a longer version of the throwing animation that cannot be interrupted
by new inputs.
*/


#ifndef SRC_STATES_ATTACK_LONG_HPP_
#define SRC_STATES_ATTACK_LONG_HPP_


#include <string>

#include "state.hpp"

#include "../components/id.hpp"
#include "../json.hpp"
#include "../scene.hpp"
#include "../stage.hpp"


class AttackLong : public State {
public:
	AttackLong(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* update(Stage& stage) override;
};


#endif /* SRC_STATES_ATTACK_LONG_HPP_ */
