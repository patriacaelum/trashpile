#include "graph_node.hpp"


/** A graph node contains information about its location on a 2D graph.

\param x_               the horizontal location in the graph.
\param y_               the vertical location in the graph.
\param traversable_     set to `true` if the node can be traversed, `false`
                        otherwise.
*/
GraphNode::GraphNode(const int x_, const int y_, const bool traversable_) :
    x(x_),
    y(y_),
    traversable(traversable_),
    came_from(nullptr),
    g(std::numeric_limits<float>::infinity()),
    h(0)
{

}


/** The f score represents the current best guess as to how short a path from
the start node to the goal node can be if it goes through this node.

The f score is defined as `f = g + h`, where `g` is the cost of the cheapest
path from the start node to this node, and `h` is the estimate cost to reach the
goal node from this node.

\returns    the f score of this node.
*/
float GraphNode::f_score() const {
    return g + h;
}


/** Defines the less than operator.

This is for the priority queue where the highest priority element in the queue
is the node with the lowest f score. In other words, for nodes `n1` and `n2`,
`n1 < n2` if and only if `f(n1) > f(n2)`.

\param l    the left node.
\param r    the right node.

\returns    `true` if `n1 < n2` or `f(n1) > f(n2)`, `false` otherwise.
*/
bool NodeComparator::operator()(const GraphNode* l, const GraphNode* r) {
    return l->f_score() > r->f_score();
}
