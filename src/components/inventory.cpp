#include "inventory.hpp"


/** Adds an entity to this entity's inventory.

\param id       the id of the entity being added.
*/
void Inventory::add(const ID id) {
    emplace(id);
}


/** Removes an entity from this entity's inventory.

\param id       the id of the entity being removed.
*/
void Inventory::remove(const ID id) {
    erase(id);
}


/** Checks if the entity is in this entity's inventory.

\param id       the id of the entity that may or may not be in inventory.

\returns        `true` if the specified entity is in inventory, `false`
                otherwise.
*/
bool Inventory::contains(const ID id) const {
    if (find(id) != end()) {
        return true;
    }

    return false;
}
