/** id_manager.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `IDManager` keeps track of and distributes IDs for each entity. The maximum
number of IDs that can be distributed is defined in `globals.hpp`.
*/


#ifndef SRC_ID_MANAGER_HPP_
#define SRC_ID_MANAGER_HPP_


#include <bitset>
#include <stdexcept>
#include <string>
#include <queue>
#include <unordered_map>

#include <SDL2/SDL.h>

#include "components/id.hpp"
#include "globals.hpp"


class IDManager {
public:
	IDManager();

	ID add(const std::string name);
	void remove(const ID id);

	void reset();
	bool test(const ID id) const;

	ID id(const std::string name) const;
	std::string name(const ID id) const;

private:
	/// The current number of IDs distributed.
	int _nids = 0;

	/// The maximum number of IDs that can be distributed.
	const int _max = PROPERTIES::MAX_ENTITIES;

	/// Keeps track of which IDs have been distributed.
	std::bitset<PROPERTIES::MAX_ENTITIES> _ids;

	/// The IDs available to be distributed.
	std::queue<ID> _available;

	/// Maps names to IDs.
	std::unordered_map<std::string, ID> _names_to_ids;

	/// Maps IDs to names.
	std::unordered_map<ID, std::string> _ids_to_names;
};


#endif /* SRC_ID_MANAGER_HPP_ */
