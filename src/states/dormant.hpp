/** dormant_state.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The base state that immediately disconnects from all connection attempts.
*/


#ifndef SRC_STATES_DORMANT_HPP_
#define SRC_STATES_DORMANT_HPP_


#include <string>

#include "state.hpp"
#include "../components/id.hpp"
#include "../globals.hpp"
#include "../scene.hpp"
#include "../stage.hpp"


class Dormant: public State {
public:
	Dormant(const ID id, const JSON& data, Stage& stage);
};


#endif /* SRC_STATES_DORMANT_HPP_ */
