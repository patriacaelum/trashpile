/** grappling_hook_short.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The short animation for the grappling hook. This is associated with the
`ShortAttack` state.

The state process is

```
GrapplingHookShort -> LongGrapplingHook
```
*/


#ifndef SRC_STATES_GRAPPLING_HOOK_SHORT_HPP_
#define SRC_STATES_GRAPPLING_HOOK_SHORT_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class GrapplingHookShort : public State {
public:
	GrapplingHookShort(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;
	void exit(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_GRAPPLING_HOOK_SHORT_HPP_ */
