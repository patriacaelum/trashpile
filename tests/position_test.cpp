/** position_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `Position` component.
*/


#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/position.hpp"
#include "../src/globals.hpp"
#include "../src/json.hpp"


TEST_CASE("Position component tests", "[position][component]") {
	JSON data = R"(
		{
			"position": {
				"x": 0,
				"y": 1,
				"z": 2,
				"d": "north"
			},
			"bad_position": {
				"foo": "bar"
			}
		}
	)"_json;

	SECTION("Position component default constructor") {
		Position position = data.at("position").get<Position>();

		REQUIRE(position.x == 0);
		REQUIRE(position.y == 1);
		REQUIRE(position.z == 2);
		REQUIRE(position.d == DIRECTION::NORTH);
	}

	SECTION("Position component bad key") {
		REQUIRE_THROWS(data.at("bad_position").get<Position>());
	}
}
