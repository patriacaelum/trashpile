/** door_open.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The open state moves the connected entity to the exit.
*/


#ifndef SRC_STATES_DOOR_OPEN_HPP_
#define SRC_STATES_DOOR_OPEN_HPP_


#include <string>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../scene.hpp"


class DoorOpen: public State {
public:
	DoorOpen(const ID id, const JSON& data, Stage& stage);

	State* update(Stage& stage) override;

private:
	/// The location of where the connected entity is moved to
	const Position _exit;
};


#endif /* SRC_STATES_DOOR_OPEN_HPP_ */
