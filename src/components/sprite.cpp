#include "sprite.hpp"


/** Destroys the spritesheet.
*/
Sprite::~Sprite() {
	if (spritesheet != NULL) {
		SDL_DestroyTexture(spritesheet);

		spritesheet = NULL;

		SDL_LogDebug(
			SDL_LOG_CATEGORY_VIDEO,
			"Sprite :: successfully destroyed spritesheet '%s' for id=%d",
			filename.c_str(),
			id
		);
	}
}


/** Loads the spritesheet.

\param renderer		the renderer where the sprites will be shown.
*/
void Sprite::load(SDL_Renderer* renderer) {
	SDL_Surface* surface = IMG_Load(filename.c_str());

	if (surface == NULL) {
		SDL_LogError(
			SDL_LOG_CATEGORY_VIDEO,
			"Sprite :: failed to load spritesheet '%s' as SDL_Surface for id=%d: %s",
			filename.c_str(),
			id,
			IMG_GetError()
		);

		throw std::runtime_error(
			"Sprite :: failed to load spritesheet as SDL_Surface"
		);
	}

	spritesheet = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);

	if (spritesheet == NULL) {
		SDL_LogError(
			SDL_LOG_CATEGORY_VIDEO,
			"Sprite :: failed to load spritesheet '%s' as SDL_Texture for id=%d: %s",
			filename.c_str(),
			id,
			IMG_GetError()
		);

		throw std::runtime_error(
			"Sprite :: failed to load spritesheet as SDL_Texture"
		);
	}

	SDL_LogDebug(
		SDL_LOG_CATEGORY_VIDEO,
		"Sprite :: successfully loaded spritesheet '%s' for id=%d",
		filename.c_str(),
		id
	);
}
