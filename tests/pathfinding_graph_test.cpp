/** pathfinding_graph_test.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

Tests creating a graph and the pathfinding algorithm.
*/


#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/pathfinding/distance.hpp"
#include "../src/pathfinding/graph.hpp"
#include "../src/pathfinding/graph_node.hpp"


TEST_CASE("Pathfinding graph and A* algorithm", "[pathfinding][graph]") {
    const int w = 10;
    const int h = 10;

    Graph graph = Graph(w, h);

    const int x_start = 1;
    const int y_start = 2;

    const int x_goal = 5;
    const int y_goal = 7;

    GraphNode start = graph.at(x_start, y_start);
    GraphNode goal = graph.at(x_goal, y_goal);

    const int distance = manhattan_distance(&start, &goal);

    const SDL_Point start_size = {2, 2};

    SECTION("Pathfinding graph constructor") {
        REQUIRE(graph.path_found());
        REQUIRE(graph.path().size() == 0);
    }

    SECTION("Pathfinding left and upward neighbours blocked") {
        graph.at(0, 3).traversable = false;
        graph.at(2, 1).traversable = false;

        std::vector<GraphNode*> neighbours = graph.neighbours(&start, start_size.x, start_size.y);

        REQUIRE(neighbours.size() == 2);

        // Right nearest neighbour
        REQUIRE(neighbours[0]->x == 2);
        REQUIRE(neighbours[0]->y == 2);

        // Downward nearest neighbour
        REQUIRE(neighbours[1]->x == 1);
        REQUIRE(neighbours[1]->y == 3);
    }

    SECTION("Pathfinding right and downward neighbours blocked") {
        graph.at(3, 3).traversable = false;
        graph.at(2, 4).traversable = false;

        std::vector<GraphNode*> neighbours = graph.neighbours(&start, start_size.x, start_size.y);

        REQUIRE(neighbours.size() == 2);

        // Left nearest neighbour
        REQUIRE(neighbours[0]->x == 0);
        REQUIRE(neighbours[0]->y == 2);

        // Upward nearest neighbour
        REQUIRE(neighbours[1]->x == 1);
        REQUIRE(neighbours[1]->y == 1);
    }

    SECTION("Pathfinding graph resize") {
        const int new_w = 20;
        const int new_h = 20;

        const int x = 13;
        const int y = 17;

        graph.resize(new_w, new_h);

        REQUIRE(graph.path_found());
        REQUIRE(graph.path().size() == 0);
    }

    SECTION("Pathfinding simple path") {
        graph.find_path(start, goal, manhattan_distance);

        REQUIRE(graph.path_found());
        REQUIRE(graph.path().size() == distance + 1);
        REQUIRE(graph.path()[0].x == x_start);
        REQUIRE(graph.path()[0].y == y_start);
        REQUIRE(graph.path()[distance].x == x_goal);
        REQUIRE(graph.path()[distance].y == y_goal);
    }

    /* Visual representation of graph.

     |0123456789
    ------------
    0|          
    1|          
    2| S XXXXX  
    3|   X      
    4|   X      
    5|   X      
    6|   X      
    7|   X G    
    8|          
    9|          

    S is the start node, G is the goal node, and X are intraversable nodes.
    */
    SECTION("Pathfinding with obstructions") {
        graph.at(3, 2).traversable = false;
        graph.at(4, 2).traversable = false;
        graph.at(5, 2).traversable = false;
        graph.at(6, 2).traversable = false;
        graph.at(7, 2).traversable = false;
        graph.at(3, 3).traversable = false;
        graph.at(3, 4).traversable = false;
        graph.at(3, 5).traversable = false;
        graph.at(3, 6).traversable = false;
        graph.at(3, 7).traversable = false;

        graph.find_path(start, goal, manhattan_distance);
        const int path_size = graph.path().size();

        REQUIRE(graph.path_found());
        REQUIRE(path_size > 0);
        REQUIRE(graph.path()[0].x == x_start);
        REQUIRE(graph.path()[0].y == y_start);
        REQUIRE(graph.path()[path_size - 1].x == x_goal);
        REQUIRE(graph.path()[path_size - 1].y == y_goal);
    }

    /* Visual representation of graph.

     |0123456789
    ------------
    0|          
    1|          
    2| SSXXXXX  
    3| SSX      
    4|   X      
    5|   X      
    6|   X      
    7|   X G    
    8|          
    9|          

    S are the start nodes, G is the goal node, and X are intraversable nodes.
    */
    SECTION("Pathfinding with obstructions and larger size") {
        graph.at(3, 2).traversable = false;
        graph.at(4, 2).traversable = false;
        graph.at(5, 2).traversable = false;
        graph.at(6, 2).traversable = false;
        graph.at(7, 2).traversable = false;
        graph.at(3, 2).traversable = false;
        graph.at(3, 3).traversable = false;
        graph.at(3, 4).traversable = false;
        graph.at(3, 5).traversable = false;
        graph.at(3, 6).traversable = false;
        graph.at(3, 7).traversable = false;

        graph.find_path(start, goal, manhattan_distance, start_size);
        const int path_size = graph.path().size() - 1;

        REQUIRE(graph.path_found());
        REQUIRE(path_size > 0);
        REQUIRE(graph.path()[0].x == x_start);
        REQUIRE(graph.path()[0].y == y_start);
        REQUIRE(abs(graph.path()[path_size].x - x_goal) < start_size.x);
        REQUIRE(abs(graph.path()[path_size].y - y_goal) < start_size.y);
    }

    /* Visual representation of graph.

     |0123456789
    ------------
    0|   X      
    1|   X      
    2| S X      
    3|   X      
    4|   X      
    5|   X      
    6|   X      
    7|   X G    
    8|   X      
    9|   X      

    S is the start node, G is the goal node, and X are intraversable nodes.
    */
    SECTION("Pathfinding no possible path") {
        graph.at(3, 0).traversable = false;
        graph.at(3, 1).traversable = false;
        graph.at(3, 2).traversable = false;
        graph.at(3, 3).traversable = false;
        graph.at(3, 4).traversable = false;
        graph.at(3, 5).traversable = false;
        graph.at(3, 6).traversable = false;
        graph.at(3, 7).traversable = false;
        graph.at(3, 8).traversable = false;
        graph.at(3, 9).traversable = false;

        graph.find_path(start, goal, manhattan_distance);

        REQUIRE(graph.path_found());
        REQUIRE(graph.path().size() == 0);
    }

    /* Visual representation of graph.

     |0123456789
    ------------
    0|          
    1|   X      
    2| SSXXXXX  
    3| SSX      
    4|   X      
    5|   X      
    6|   X      
    7|   X G    
    8|   X      
    9|          

    S are the start nodes, G is the goal node, and X are intraversable nodes.
    */
    SECTION("Pathfinding not possible path for larger entity") {
        graph.at(3, 1).traversable = false;
        graph.at(3, 2).traversable = false;
        graph.at(4, 2).traversable = false;
        graph.at(5, 2).traversable = false;
        graph.at(6, 2).traversable = false;
        graph.at(7, 2).traversable = false;
        graph.at(3, 2).traversable = false;
        graph.at(3, 3).traversable = false;
        graph.at(3, 4).traversable = false;
        graph.at(3, 5).traversable = false;
        graph.at(3, 6).traversable = false;
        graph.at(3, 7).traversable = false;
        graph.at(3, 8).traversable = false;

        graph.find_path(start, goal, manhattan_distance, start_size);

        REQUIRE(graph.path_found());
        REQUIRE(graph.path().size() == 0);
    }
}
