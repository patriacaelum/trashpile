#include "logger.hpp"


std::map <int, std::string> CATEGORIES = {
    {SDL_LOG_CATEGORY_APPLICATION, "APPLICATION"},
    {SDL_LOG_CATEGORY_ERROR, "ERROR"},
    {SDL_LOG_CATEGORY_ASSERT, "ASSERT"},
    {SDL_LOG_CATEGORY_SYSTEM, "SYSTEM"},
    {SDL_LOG_CATEGORY_AUDIO, "AUDIO"},
    {SDL_LOG_CATEGORY_VIDEO, "VIDEO"},
    {SDL_LOG_CATEGORY_RENDER, "RENDER"},
    {SDL_LOG_CATEGORY_INPUT, "INPUT"},
    {SDL_LOG_CATEGORY_TEST, "TEST"}
};

std::map <int, std::string> PRIORITIES = {
    {SDL_LOG_PRIORITY_VERBOSE, "VERBOSE"},
    {SDL_LOG_PRIORITY_DEBUG, "DEBUG"},
    {SDL_LOG_PRIORITY_INFO, "INFO"},
    {SDL_LOG_PRIORITY_WARN, "WARN"},
    {SDL_LOG_PRIORITY_ERROR, "ERROR"},
    {SDL_LOG_PRIORITY_CRITICAL, "CRITICAL"}
};


/** A custom logging function.

A custom logging function that prints the message on the terminal and
writes to a daily log file.

\param userdata   a parameter passed through `SDL2`.
\param category   category of the log message.
\param priority   priority of the log message.
\param message    the message being output.
*/
void Logger(
        void* userdata,
        int category,
        SDL_LogPriority priority,
        const char *message
    ) {

	// Create formatted log message
	std::string log = " -- " + CATEGORIES[category] + " " + PRIORITIES[priority]
	    + " -- " + message;

	// Get current time
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	auto now = std::put_time(&tm, "%H:%M:%S");

	// Print log message to terminal
	std::clog << now << log << std::endl;

	// Append log message to today's log file
	std::string filename = __DATE__;
	std::ofstream logfile;

	logfile.open("../../logs/" + filename + ".txt", std::ios::app);
	logfile << now << log;
	logfile.close();
}
