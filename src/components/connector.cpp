#include "connector.hpp"


/** Determines if this entity has an established connection.

\returns    `true` if there is a connection, otherwise `false`.
*/
bool Connector::isConnected() const {
    return _connection != -1;
}


/** Returns the ID of the entity connected to this one.

If there is no current connection, an ID value of `-1` is returned.

\returns    the ID of the connected entity, `-1` otherwise.
*/
ID Connector::connection() const {
    return _connection;
}


/** Establishes a connection between two entities.

This method fails if this entity is already connected to another entity, but
does not check if the other entity is connected.

\param connection       the ID of the other entity.
*/
void Connector::connect(const ID& connection) {
    if (_connection != -1) {
        SDL_LogError(
            SDL_LOG_CATEGORY_APPLICATION,
            "Connector :: failed to connect id=%d with id=%d: id=%d is already connected with id=%d",
            id,
            connection,
            id,
            _connection
        );

        throw std::logic_error(
            "Connector :: failed to connect: entity already has connection"
        );
    }

    // Establish connection
    _connection = connection;

    SDL_LogDebug(
        SDL_LOG_CATEGORY_APPLICATION,
        "Connector :: successfully connected id %d with id=%d",
        id,
        connection
    );
}


/** Disconnect this entity.

This method does not disconnect the other entity.
*/
void Connector::disconnect() {
    ID connection = _connection;
    _connection = -1;

    SDL_LogDebug(
        SDL_LOG_CATEGORY_APPLICATION,
        "Connector :: successfully disconnected id=%d from id=%d",
        id,
        connection
    );
}
