/** position.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Position` denotes the location of the `Entity` in the `Scene`.
*/


#ifndef SRC_COMPONENTS_POSITION_HPP_
#define SRC_COMPONENTS_POSITION_HPP_


#include "id.hpp"
#include "../globals.hpp"


class Position {
public:
	/// ID of the associated entity
	ID id = -1;

	/// The direction the entity is facing.
	DIRECTION d = DIRECTION::SOUTH;

	/// The number of pixels from the left of the window.
	int x = 0;

	/// The number of pixels from the top of the window.
	int y = 0;

	/// The number of layers from the bottom of the `Stage`.
	int z = 0;
};


#endif /* SRC_COMPONENTS_POSITION_HPP_ */
