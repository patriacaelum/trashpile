/** noisemaker_on.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `NoisemakerOn` state is the active state for the noisemaker.
*/


#ifndef SRC_STATES_NOISEMAKER_ON_HPP_
#define SRC_STATES_NOISEMAKER_ON_HPP_


#include <string>

#include "state.hpp"

#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class NoisemakerOn : public State {
public:
    NoisemakerOn(const ID id, const JSON& data, Stage& stage);

    void enter(Stage& stage) override;

    State* update(Stage& stage) override;

private:
    int _n_repetitions;
    const int _max_repetitions;
};


#endif /* SRC_STATES_NOISEMAKER_ON_HPP_ */
