# World Building

### Questions
- If the citizens have a special ability, or lack of it, how do they design
  their living space to best benefit them?
- How do they use their natural resources? How does that affect their way
  of life?
- If electricity exists, how is power generated?
- How do they get rid of dead bodies? How do they honour and remember the
  dead?
- How do the citizens get from one part of the city to the other? How does
  it differ between long and short distances? Young and old? Rich and poor?
- How do the citizens incorporate green space? Water and agriculture? 
  Livestock?
- How do the citizens deal with the terrain and weather? Rain and snow?
  Floods and droughts?
