/** top_down_automata.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Connector` controls which entities are interacting with other entities.

*/


#ifndef SRC_COMPONENTS_CONNECTOR_HPP_
#define SRC_COMPONENTS_CONNECTOR_HPP_

#include <stdexcept>
#include <SDL2/SDL.h>

#include "id.hpp"


class Connector {
public:
    bool isConnected() const;
    ID connection() const;

    void connect(const ID& connection);
    void disconnect();

    ID id = -1;

private:
    ID _connection = -1;
};


#endif /* SRC_COMPONENTS_CONNECTOR_HPP_ */
