/** scene.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Scene` contains the data for all the entities in the current scene. The
`Scene` consists of multiple instances of the `ComponentManager` each storing
a specific type of component for all the entities. In this way, an entity is
not a single object, but a construction connected by an ID.

Throughout the code, the abbreviations used are:

  - `edata` meaning entity data.
  - `cdata` meaning component data.

*/


#ifndef SRC_SCENE_HPP_
#define SRC_SCENE_HPP_


#include <unordered_map>
#include <queue>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

#include <SDL2/SDL.h>

#include "components/connector.hpp"
#include "components/dialogue.hpp"
#include "components/hitbox.hpp"
#include "components/id.hpp"
#include "components/inventory.hpp"
#include "components/knowledge.hpp"
#include "components/path.hpp"
#include "components/position.hpp"
#include "components/property.hpp"
#include "components/sprite.hpp"
#include "components/pushdown_automaton.hpp"
#include "components/velocity.hpp"
#include "component_manager.hpp"
#include "id_manager.hpp"
#include "globals.hpp"
#include "json.hpp"


class Stage;


class Scene {
public:
	Scene();
	~Scene();

	std::vector<ID> ids() const;
	std::string name(const ID id) const;

	void load(const std::string filename, SDL_Renderer* renderer, Stage& stage);
	void unload();

	Connector& connector(const ID& id);	
	Dialogue& dialogue(const ID& id);
	Hitbox& hitbox(const ID& id);
	Inventory& inventory(const ID& id);
	Knowledge& knowledge(const ID& id);
	Path& path(const ID& id);
	Position& position(const ID& id);
	Sprite& portrait(const ID& id);
	SDL_Rect& portraitPosition(const ID& id);
	Property& property(const ID& id);
	Sprite& sprite(const ID& id);
	Velocity& velocity(const ID& id);

	PushdownAutomaton& pda(const ID& id);

	ID subentity(const ID& id);
	ID superentity(const ID& id);

	SDL_Rect mapSize();

private:
	ID loadEntity(const auto& data, SDL_Renderer* renderer, Stage& stage);

	// Required components
	void loadConnector(const ID id, const auto& data);
	void loadHitbox(const ID id, const auto& data);
	void loadPosition(const ID id, const auto& data);
	void loadProperty(const ID id, const auto& data);
	void loadVelocity(const ID id, const auto& data);

	// Sprite-related components
	void loadPortrait(const ID id, const auto& data, SDL_Renderer* renderer);
	void loadPortraitPosition(const ID id, const auto& data);
	void loadSprite(const ID id, const auto& data, SDL_Renderer* renderer);

	// State-related components	
	void loadPDA(const ID id, const auto& data, Stage& stage);

	// Components with special loading conditions
	void loadDialogue(const ID id, const auto& data, SDL_Renderer* renderer);
	void loadInventory(const ID id, const auto& data);
	void loadKnowledge(const ID id, const auto& data);

	// Other helper functions
	void offsetPositions();

	IDManager _idman;

	ComponentManager<Connector> _conman;
	ComponentManager<Dialogue> _dialman;
	ComponentManager<Hitbox> _hitman;
	ComponentManager<Inventory> _inventman;
	ComponentManager<Knowledge> _knowman;	
	ComponentManager<Path> _pathman;
	ComponentManager<Position> _posman;
	ComponentManager<Sprite> _portman;
	ComponentManager<SDL_Rect> _portposman;
	ComponentManager<Property> _propman;
	ComponentManager<Sprite> _spriteman;
	ComponentManager<Velocity> _velman;

	ComponentManager<ID> _subman;
	ComponentManager<ID> _superman;
	ComponentManager<PushdownAutomaton> _pdaman;

	SDL_Rect _map_size;

	/// The size buffer when calculating the map size, added to each side of
	/// the map.
	const int SIZE_BUFFER = 500;
};


#endif /* SRC_SCENE_HPP_ */
