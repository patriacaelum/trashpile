#include "./dormant.hpp"


Dormant::Dormant(const ID id, const JSON& data, Stage& stage)
:	State("dormant", true, {}, id, data)
{}
