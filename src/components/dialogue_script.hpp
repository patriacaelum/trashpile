/** dialogue_script.cpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

A collection of dialogue nodes that make up a conversation.

Each script is expected to be in the format

```
{
    "first_node": int,
    "nodes": [
        {DialogueNode},
        {DialogueNode},
        ...
    ]
}
```
*/


#ifndef SRC_COMPONENTS_DIALOGUE_SCRIPT_HPP_
#define SRC_COMPONENTS_DIALOGUE_SCRIPT_HPP_


#include <algorithm>
#include <string>
#include <unordered_map>

#include "dialogue_node.hpp"


class DialogueScript {
public:
    std::string nextLine(std::string choice="");
    std::string currentLine();

    void reset();

    std::unordered_map<ID, DialogueNode> nodes;

    ID first_node_id;
    ID current_node_id;
    ID last_node_id;

private:
    void nextNode(std::string choice="");
    void setValue(std::string key, std::string value);
};


#endif /* SRC_COMPONENTS_DIALOGUE_SCRIPT_HPP_ */
