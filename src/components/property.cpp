#include "property.hpp"


int Property::at(const std::string key) {
    try {
        return std::unordered_map<std::string, int>::at(key);
    }
    catch (std::out_of_range& error) {
        return -1;
    }
}
