/** graph_node.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

A graph node represents a square on a 2D grid.
*/


#ifndef SRC_PATHFINDING_GRAPH_NODE_HPP_
#define SRC_PATHFINDING_GRAPH_NODE_HPP_


#include <limits>


class GraphNode {
public:
    GraphNode(
        const int x_ = 0,
        const int y_ = 0,
        const bool traversable_ = true
    );

    float f_score() const;

    /// Horizontal location in the graph.
    int x;
    /// Vertical location in the graph.
    int y;
    /// Set to `true` if the node can be traversed, `false` otherwise.
    bool traversable;

    /// The node immediately preceding it on the currently known cheapest path.
    GraphNode* came_from;

    /// The g score is the cost of the cheapest path from the start node to this
    /// node.
    float g;
    /// The h score is the estimate cost to reach the goal node from this node.
    float h;
};


struct NodeComparator {
    bool operator()(const GraphNode* l, const GraphNode* r);
};


#endif /* SRC_PATHFINDING_GRAPH_NODE_HPP_ */
