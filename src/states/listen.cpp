#include "listen.hpp"


Listen::Listen(const ID id, const JSON& data, Stage& stage)
:	State("listen", false, {}, id, data)
{
	// The STATES::SPEAK state is a dependency of the STATES::LISTEN state
	// We omit it here to avoid a circular dependency
}


/** Sets the entity `Velocity` to zero.

\param stage	the currently loaded stage.
*/
void Listen::enter(Stage& stage) {
	State::enter(stage);

	Velocity& velocity = stage.scene.velocity(id);

	velocity.dx = 0;
	velocity.dy = 0;
}


/** Notifies the state of an event.

The following states are returned:

- a `Speak` state given a `SPEAK` event.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a `State` pointer.
*/
State* Listen::notify(const EVENT event, const ID sender, Stage& stage) {
	const Connector& connector = stage.scene.connector(id);

	switch (event) {
	case EVENT::DISCONNECT:
		if (connector.isConnected() && sender == connector.connection()) {
			_finished = true;

			return nullptr;
		}

		break;

	case EVENT::SPEAK:
		if (connector.isConnected() && sender == connector.connection()) {
			_finished = true;

			return stage.scene.pda(id).at(STATES::SPEAK);
		}

		break;
	}

	return update(stage);
}
