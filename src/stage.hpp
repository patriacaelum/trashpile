/** stage.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Stage` is where all the internal parts of the engine reside. The `Scene`
holds all the entity information. The `Camera` keeps track of where the entities
are in on the screen. The layers keeps track of the render order of the
entities. The stage also manages the connections between entities.
*/


#ifndef SRC_STAGE_HPP_
#define SRC_STAGE_HPP_


#include <algorithm>
#include <bitset>
#include <queue>
#include <set>
#include <utility>

#include "camera.hpp"
#include "components/id.hpp"
#include "postal_service/mailbox.hpp"
#include "globals.hpp"
#include "menu.hpp"
#include "scene.hpp"
#include "visual_novel.hpp"


class Stage {
public:
	Stage();

	void update();

	void load(const std::string filename, SDL_Renderer* renderer);

	void hide(const ID& id);
	void show(const ID& id);

	ID findCollision(const ID& lid, const int dist = 0);
	bool isCollision(const ID& lid, const ID& rid, const int dist = 0);
	bool isOverlap(const ID& lid, const ID& rid);

	void connect(const ID& sender, const ID& receiver);
	void disconnect(const ID& sender, const ID& receiver);

	ID player() const;

	/// Receives and distributes events.
	Mailbox mailbox;

	/// Keeps track of where the entities are on the screen.
	Camera camera;

	/// Holds all entity information.
	Scene scene;

	/// Handles the menu context
	Menu menu;

	/// Handles the dialogue system and portraits.
	VisualNovel visual_novel;

	/// Determines the render order and which entities interact with another.
	std::vector<std::vector<ID>> layers;

private:
	void moveLayer(const ID id, const int from_layer, const int to_layer);
	void sortLayers();

	void updateCamera();
	void updateInteractions();
	void updateStates();

	bool compare(const ID& lhs, const ID& rhs);

	/// The ID of the player entity.
	ID _player;

	/// Keeps track of which layers require sorting.
	std::bitset<5> _sort_layer;
};


#endif /* SRC_STAGE_HPP_ */
