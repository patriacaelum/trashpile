/** engine.hpp

\verbatim
       ||          __  __  ___  ____   ___  ____  _____ ____
       --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
  -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
  -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
       --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
       ||
\endverbatim

The `Engine` processes player input and computes all the game logic.
*/


#ifndef SRC_ENGINE_HPP_
#define SRC_ENGINE_HPP_


#include <string>
#include <SDL2/SDL.h>

#include "input_manager.hpp"
#include "renderer.hpp"
#include "stage.hpp"


class Engine {
public:
	Engine();

	bool run();

private:
	void load(const std::string filename);

	int getRefreshRate();

	/// Collects inputs from the player.
	InputManager _inputman;

	/// Renders the `Scene` entities to the screen.
	Renderer _renderer;

	/// The `Stage` tracks all the systems for the entities in the `Scene`.
	Stage _stage;

	/// Determines if the game is running on a high refresh monitor.
	bool _high_refresh;

	/// Determines if we can skip the next frame.
	bool _skip;
};


#endif /* SRC_ENGINE_HPP_ */
