/** sprite.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Sprite` component contains a pointer to an `SDL_Texture` for the
spritesheet and an `SDL_Rect` which defines the section of the spritesheet to be
rendered.
*/


#ifndef SRC_COMPONENTS_SPRITE_HPP_
#define SRC_COMPONENTS_SPRITE_HPP_


#include <stdexcept>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "id.hpp"


class Sprite {
public:
	~Sprite();

	void load(SDL_Renderer* renderer);

	/// ID of the associated entity
	ID id = -1;

	/// The file of the spritesheet.
	std::string filename;

	/// The atlas of sprites.
	SDL_Texture* spritesheet;

	/// The rectangle defining the section of the spritesheet to be rendered.
	SDL_Rect sprite;
};


#endif /* SRC_COMPONENTS_SPRITE_HPP_ */
