/** knowledge.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Knowledge` component stores an entity's knowledge of other entities. An
entity's knowledge level may be one of the following:

  - Full
  - Partial
  - Empty

When loading from JSON, the knowledge component should have the format

```
{
    "knowledge": {
        "id_0": "FULL",
        "id_1": "PARTIAL",
        "id_2": "EMPTY"
    }
}
```
*/


#ifndef SRC_COMPONENTS_KNOWLEDGE_HPP_
#define SRC_COMPONENTS_KNOWLEDGE_HPP_


#include <stdexcept>
#include <unordered_map>

#include "id.hpp"
#include "../globals.hpp"


class Knowledge {
public:
    KNOWLEDGE level(const ID& id) const;
    void set(const ID id, KNOWLEDGE level);

    /// ID of the associated entity
    ID id = -1;

    /// Maps an entity ID to a knowledge level
    std::unordered_map<ID, KNOWLEDGE> levels;
};


#endif /* SRC_COMPONENTS_KNOWLEDGE_HPP_ */
