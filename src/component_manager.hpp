/** component_manager.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `ComponentManager` keeps track of and allocates memory for all components of
a single type.

Each instance of the `ComponentManager` holds a single type of component for all
the entities in the `Scene`. The components are held in a static-sized array
defined in `globals.hpp`. This is so the components are stored in stack rather
than the heap for faster retrieval.
*/


#ifndef SRC_COMPONENT_MANAGER_HPP_
#define SRC_COMPONENT_MANAGER_HPP_


#include <algorithm>
#include <array>
#include <exception>
#include <map>

#include <SDL2/SDL.h>

#include "components/id.hpp"
#include "globals.hpp"


template<class T>
class ComponentManager {
public:
	/** Adds a component mapped by its ID.

	\param ID			The ID of the entity.
	\param component	The component to be stored.
	*/
	void add(const ID id, const T component) {
		if (_ncomponents == _max) {
			SDL_LogError(
				SDL_LOG_CATEGORY_APPLICATION,
				"ComponentManager :: maximum number of components created"
			);

			throw std::overflow_error(
				"ComponentManager :: maximum number of components created"
			);
		}

		_components[_ncomponents] = component;
		_ids[_ncomponents] = id;
		_indices[id] = _ncomponents;

		++_ncomponents;
	}

	/** Removes a component.

	\param ID			The ID of the entity.
	\param component	The component to be removed.
	*/
	void remove(const ID id) {
		int index = _indices[id];
		--_ncomponents;

		// Move the last component in the array into the position of the one
		// being removed
		_components[index] = _components[_ncomponents];
		_ids[index] = _ids[_ncomponents];
		_indices[_ids[_ncomponents]] = index;

		// Erase the references to the old location
		_components.erase(_ncomponents);
		_ids.erase(_ncomponents);
		_indices.erase(id);
	}

	/** Retrieves a reference to the component.

	\param ID		The ID of the entity.
	*/
	T& get(const ID id) {
		return _components.at(_indices.at(id));
	}

	/** Retrieves the ID of the entity with a matching component.

	Note that this function currently only returns the first ID that has a
	matching component. This means that if there are two entities with exactly
	the same components, only one is returned.

	\param component	The component of the entity.
	*/
	ID id(const T& component) {
		T* iter = std::find(_components.begin(), _components.end(), component);
		int index = iter - _components.begin();

		return _ids.at(index);
	}

	/** Clears the contents of the container.

	Note that the containers are cleared by the objects themselves are not
	manually deleted.
	 */
	void clear() {
		_ncomponents = 0;
		_components = std::array<T, PROPERTIES::MAX_ENTITIES>();
		_ids.clear();
		_indices.clear();
	}

private:
	/// The number of components currently stored.
	int _ncomponents = 0;

	/// The maximum number of components that can be stored.
	const int _max = PROPERTIES::MAX_ENTITIES;

	/// Where the components are stored.
	std::array<T, PROPERTIES::MAX_ENTITIES> _components;

	/// Maps the index in the array where the component is stored to the entity
	/// ID of the component.
	std::map<int, ID> _ids;

	/// Maps the entity ID of the component to the index of the array where it
	/// was stored.
	std::map<ID, int> _indices;
};


#endif /* SRC_COMPONENT_MANAGER_HPP_ */
