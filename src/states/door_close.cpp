#include "door_close.hpp"


DoorClose::DoorClose(const ID id, const JSON& data, Stage& stage)
:	State("door_close", true, {STATES::DOOR_OPEN}, id, data)
{}


/** Notifies the state of an event.

The following events can be posted:

- A `ROOM_CHANGE` event is posted given a `CONNECT` event.

The following states can be returned:

- an `Open` state is returned given a `CONNECT` event.

\param event
	the event trigger.
\param id
	the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a `State` pointer.
*/
State* DoorClose::notify(const EVENT event, const ID sender, Stage& stage) {
	switch (event) {
	case EVENT::CONNECT:
		const Connector& connector = stage.scene.connector(id);

		if (connector.isConnected() && sender == connector.connection()) {
			return stage.scene.pda(id)[STATES::DOOR_OPEN];
		}

		break;
	}

	return update(stage);
}
