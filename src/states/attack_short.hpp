/** attack_short.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

Creates a short throwing animation. The animation cannot be interrupted by an
input.

The state process is

```
AttackShort -> LongAttack
```
*/


#ifndef SRC_STATES_attack_short_HPP_
#define SRC_STATES_attack_short_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class AttackShort : public State {
public:
	AttackShort(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
	State* update(Stage& stage) override;

private:
     /// Stores if the B button is still being pressed.
	bool _b_button_pressed;
};


#endif /* SRC_STATES_attack_short_HPP_ */
