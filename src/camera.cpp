#include "camera.hpp"


/** Sets values for a generic SDL_Rect.
*/
Camera::Camera(): frame({0, 0, 0, 0}) {
}


/** Sets values for position and size of the `Camera`.

\param frame	the position and size of the camera in the scene.
*/
Camera::Camera(const SDL_Rect frame): frame(frame) {
	SDL_LogInfo(
		SDL_LOG_CATEGORY_APPLICATION,
		"Camera :: camera successfully initialized"
	);
}


/** Updates the position of the `Camera`.

Currently, the camera simply centers on the player.

\param spritebox	the spritebox of the player.
*/
void Camera::update(const SDL_Rect& sprite) {
	frame.x = sprite.x + (sprite.w - frame.w) / 2;
	frame.y = sprite.y + (sprite.h - frame.h) / 2;
}


/** Sets the size of the camera to the size of the renderer.

\param renderer		a pointer to the current renderer.
*/
void Camera::setRenderer(SDL_Renderer* renderer) {
	int width = 0;
	int height = 0;

	SDL_GetRendererOutputSize(renderer, &width, &height);

	frame.w = width;
	frame.h = height;
}
