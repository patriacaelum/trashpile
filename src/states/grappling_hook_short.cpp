#include "grappling_hook_short.hpp"


/** Creates the state for the grappling hook corresponding with the short attack
state.
 */
GrapplingHookShort::GrapplingHookShort(
	const ID id,
	const JSON& data,
	Stage& stage
) :	State(
		"grappling_hook_short",
		false,
		{STATES::GRAPPLING_HOOK_LONG},
		id,
		data
	)
{}


/** Enters the state.

\param stage
	the currently loaded stage.
*/
void GrapplingHookShort::enter(Stage& stage) {
	State::enter(stage);

	stage.scene.position(id) = stage.scene.position(stage.scene.superentity(id));
	stage.scene.sprite(id).sprite = {0, _origin, 48, 432};
}


/** Exits the state.

\param stage
	the currently loaded stage.
*/
void GrapplingHookShort::exit(Stage& stage) {
	const Connector& connector = stage.scene.connector(id);

	if (connector.isConnected()) {
		stage.disconnect(id, connector.connection());
	}

	State::exit(stage);
}


/** Updates the state.

Given an `INITIATE` input, a `LongGrapplingHook` state is returned if the input
is held until the second frame. Given a `TERMINATE` input, the state is
finished.

The short attack animation is 15 sprites * 5 frames / sprite = 75 frames. The
first 15 frames are the wind up, the next 30 are the extension, and the last 30
are the retraction.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.

returns
	a state pointer.
*/
State* GrapplingHookShort::notify(const EVENT event, const ID sender, Stage& stage) {
	const ID superentity = stage.scene.superentity(id);

	switch(event) {
	case EVENT::INITIATE:
		if (superentity != -1 && sender == superentity) {
			_finished = true;

			return stage.scene.pda(id)[STATES::GRAPPLING_HOOK_LONG];
		}

		break;

	case EVENT::TERMINATE:
		if (superentity != -1 && sender == superentity) {
			_finished = true;

			return nullptr;
		}

		break;
	}

	updateSprite(stage);

	return nullptr;
}
