/** inventory.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

Stores an entity's inventory.

*/


#ifndef SRC_COMPONENTS_INVENTORY_HPP_
#define SRC_COMPONENTS_INVENTORY_HPP_


#include <unordered_set>

#include "../globals.hpp"
#include "id.hpp"


class Inventory : public std::unordered_set<ID> {
public:
    void add(const ID id);
    void remove(const ID id);

    bool contains(const ID id) const;

    /// ID of the associated entity.
    ID id = -1;
};


#endif /* SRC_COMPONENTS_INVENTORY_HPP_ */
