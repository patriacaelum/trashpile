/** SDL_extras.cpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

These are additional functions related to the primitives from the SDL library.
*/


#ifndef SRC_SDL_EXTRAS_HPP_
#define SRC_SDL_EXTRAS_HPP_


#include <tuple>

#include <SDL2/SDL.h>


// Operator overloads for SDL primitives.
bool operator==(const SDL_Point& lhs, const SDL_Point& rhs);
bool operator==(const SDL_Rect& lhs, const SDL_Rect& rhs);


#endif /* SRC_SDL_EXTRAS_HPP_ */
