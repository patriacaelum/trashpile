/** dialogue_node_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for the `DialogueNode` in the vngine.
*/


#include <string>
#include <vector>

#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/dialogue_node.hpp"
#include "../src/json.hpp"


TEST_CASE("Dialogue node tests", "[components][dialogue_node]") {
	JSON data = R"(
		{
            "nodes": [
                {
                    "id": 0,
                    "type": "line",
                    "speaker": "Me",
                    "text": "Hello World!",
                    "next_id": [1]
                },
                {
                    "id": 1,
                    "type": "choices",
                    "choices": [
                        "Right choice.",
                        "Wrong choice."
                    ],
                    "next_id": [2, 3]
                },
                {
                    "id": 2,
                    "type": "setter",
                    "key": "player.mood",
                    "value": "calm",
                    "next_id": [3]
                },
                {
                    "id": 3,
                    "type": "line",
                    "speaker": "You",
                    "text": "So long. Farewell.",
                    "next_id": []
                }
            ]
		}
	)"_json;

	SECTION("Dialogue node line type") {
		DialogueNode line_node = data.at("nodes")[0].get<DialogueNode>();

        REQUIRE(line_node.id == 0);
        REQUIRE(line_node.next_id == std::vector<ID>{1});
        REQUIRE(line_node.node_type == "line");

        REQUIRE(line_node.speaker == "Me");
        REQUIRE(line_node.text == "Hello World!");

        REQUIRE(line_node.choices.empty());

        REQUIRE(line_node.key == "");
        REQUIRE(line_node.value == "");
	}

    SECTION("Dialogue node choices type") {
		DialogueNode choices_node = data.at("nodes")[1].get<DialogueNode>();

        REQUIRE(choices_node.id == 1);
        REQUIRE(choices_node.next_id == std::vector<ID>{2, 3});
        REQUIRE(choices_node.node_type == "choices");

        REQUIRE(choices_node.speaker == "");
        REQUIRE(choices_node.text == "");

        std::vector<std::string> choices = {
            "Right choice.",
            "Wrong choice."
        };
        REQUIRE(choices_node.choices == choices);

        REQUIRE(choices_node.key == "");
        REQUIRE(choices_node.value == "");
    }

    SECTION("Dialogue node setter type") {
		DialogueNode setter_node = data.at("nodes")[2].get<DialogueNode>();

        REQUIRE(setter_node.id == 2);
        REQUIRE(setter_node.next_id == std::vector<ID>{3});
        REQUIRE(setter_node.node_type == "setter");

        REQUIRE(setter_node.speaker == "");
        REQUIRE(setter_node.text == "");

        REQUIRE(setter_node.choices.empty());

        REQUIRE(setter_node.key == "player.mood");
        REQUIRE(setter_node.value == "calm");
    }
}
