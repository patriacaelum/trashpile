/** noisemaker_off.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `NoisemakerOff` state is similar to the `Dormant` state and is the base
state for the noisemaker.

The state process is:

```
NoisemakerOff <-> NoisemakerOn
```
*/


#ifndef SRC_STATES_NOISEMAKER_OFF_HPP_
#define SRC_STATES_NOISEMAKER_OFF_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class NoisemakerOff : public State {
public:
     NoisemakerOff(const ID id, const JSON& data, Stage& stage);

     State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_NOISEMAKER_OFF_HPP_ */
