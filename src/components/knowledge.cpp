#include "knowledge.hpp"


/** Returns this entity's knowledge level of the given entity. `EMPTY` by
default.

\param id       an entity ID.

returns         this entity's knowledge level of the given entity. If there is
                no knowledge of this entity, then the knowledge level is set to
                `EMPTY` by default.
*/
KNOWLEDGE Knowledge::level(const ID& id) const {
    try {
        return levels.at(id);
    }
    catch (std::out_of_range& error) {
        return KNOWLEDGE::EMPTY;
    }
}


/** Sets this entity's knowledge level of the given entity.

\param id       an entity ID.
\param level    this entity's knowledge level of the given entity.
*/
void Knowledge::set(const ID id, KNOWLEDGE level) {
    levels[id] = level;
}
