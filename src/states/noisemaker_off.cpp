#include "noisemaker_off.hpp"


NoisemakerOff::NoisemakerOff(const ID id, const JSON& data, Stage& stage)
:   State("noisemaker_off", true, {STATES::NOISEMAKER_ON}, id, data)
{}


State* NoisemakerOff::notify(const EVENT event, const ID sender, Stage& stage) {
    switch (event) {
    case EVENT::INITIATE:
        return stage.scene.pda(id).at(STATES::NOISEMAKER_ON);

        break;
    }

    return nullptr;
}
