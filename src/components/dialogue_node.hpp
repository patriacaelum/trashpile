/** dialogue_node.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The nodes that are used to interpret different possible actions from the visual
novel.

Each node is expected to be in the format

```
{
	"id": int,
	"next_id": [
		int,
		int,
		...
	],
	"type": "str",
	"speaker": "str",
	"text": "str",
	"choices": {
		"str": "str",
		"str": "str",
		...
	},
	"key": "str",
	"value": "str"
}
```

The keys "id", "next_id", and "type" are required for all nodes. The other keys
are dependent on the "type".

- For `"type": "line"`, the keys "speaker" and "text" are required.
- For `"type": "choices"`, the key "choices" is required.
- For `"type": "setter"`, the keys "key" and "value" are required.
*/


#ifndef SRC_COMPONENTS_DIALOGUE_NODE_HPP_
#define SRC_COMPONENTS_DIALOGUE_NODE_HPP_


#include <string>
#include <vector>

#include "id.hpp"


class DialogueNode {
public:
	ID id;
	std::vector<ID> next_id;
	std::string node_type = "";

	// Line node
	std::string speaker = "";
	std::string text = "";

	// Choices node
	std::vector<std::string> choices;

	// Setter node
	std::string key = "";
	std::string value = "";
};


#endif /* SRC_COMPONENTS_DIALOGUE_NODE_HPP_ */
