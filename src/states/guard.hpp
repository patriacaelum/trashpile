/** guard.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Guard` state is the default state of the antagonistic entities toward the
player.

  - They have a minimum hearing range where, if a noise event happened, they
    start moving toward the source of the sound.

The state transitions are:

```
Guard <-> Goto
```
*/


#ifndef SRC_STATES_GUARD_HPP_
#define SRC_STATES_GUARD_HPP_


#include <vector>

#include "state.hpp"

#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Guard : public State {
public:
    Guard(const ID id, const JSON& data, Stage& stage);

    State* notify(const EVENT event, const ID sender, Stage& stage) override;

private:
    bool in_hearing_range(const ID sender, Stage& stage);
};


#endif /* SRC_STATES_GUARD_HPP_ */
