#include "speak.hpp"


Speak::Speak(const ID id, const JSON& data, Stage& stage)
:	State("speak", false, {STATES::LISTEN}, id, data)
{}


/** Enters the state.

The velocity of the entity is set to zero.

\param stage
	the currently loaded stage.
*/
void Speak::enter(Stage& stage) {
	State::enter(stage);

	Velocity& velocity = stage.scene.velocity(id);

	velocity.dx = 0;
	velocity.dy = 0;
}


/** Updates the state.

Given an `INITIATE` input, a `Listen` state is returned until the `Dialogue`
component runs out of text. Given a `DISCONNECT` input, the state is finished.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a state pointer.
*/
State* Speak::notify(const EVENT event, const ID sender, Stage& stage) {
	const Connector& connector = stage.scene.connector(id);

	switch (event) {
	case EVENT::DISCONNECT:
		if (connector.isConnected()) {
			stage.scene.pda(connector.connection()).notify(event, id, stage);
			stage.disconnect(id, sender);

			_finished = true;

			return nullptr;
		}

		break;

	case EVENT::LISTEN:
		if (connector.isConnected() && sender == connector.connection()) {
			_finished = true;

			return stage.scene.pda(id).at(STATES::LISTEN);
		}

		break;
	}

	return update(stage);
}
