#include "input_manager.hpp"


/** Takes player input and translates it into game inputs.

The translation scheme is defined in `globals.hpp`. The events are polled, and
the input is only changed if another key has been pressed or released.

\returns	an integer corresponding with the game input, defined in
			`globals.hpp`.
*/
EVENT InputManager::getInput() {
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			SDL_LogWarn(
				SDL_LOG_CATEGORY_APPLICATION,
				"InputManager :: quit input detected"
			);

			return EVENT::QUIT;

		case SDL_KEYDOWN:
			// Ignore repeated inputs from holding down
			if (event.key.repeat != 0) {
				break;
			}

			switch (event.key.keysym.sym) {
			case SDLK_z:
				return EVENT::PRESS_A_BUTTON;

			case SDLK_x:
				return EVENT::PRESS_B_BUTTON;

			case SDLK_a:
				return EVENT::PRESS_X_BUTTON;

			case SDLK_s:
				return EVENT::PRESS_Y_BUTTON;

			case SDLK_q:
				return EVENT::PRESS_LEFT_TRIGGER;

			case SDLK_w:
				return EVENT::PRESS_RIGHT_TRIGGER;

			case SDLK_DOWN:
				return EVENT::PRESS_DOWN_BUTTON;

			case SDLK_LEFT:
				return EVENT::PRESS_LEFT_BUTTON;

			case SDLK_RIGHT:
				return EVENT::PRESS_RIGHT_BUTTON;
				
			case SDLK_UP:
				return EVENT::PRESS_UP_BUTTON;

			case SDLK_ESCAPE:
				return EVENT::PRESS_START_BUTTON;

			case SDLK_m:
				return EVENT::PRESS_SELECT_BUTTON;
			}
		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
			case SDLK_z:
				return EVENT::RELEASE_A_BUTTON;

			case SDLK_x:
				return EVENT::RELEASE_B_BUTTON;

			case SDLK_a:
				return EVENT::RELEASE_X_BUTTON;

			case SDLK_s:
				return EVENT::RELEASE_Y_BUTTON;

			case SDLK_q:
				return EVENT::RELEASE_LEFT_TRIGGER;

			case SDLK_w:
				return EVENT::RELEASE_RIGHT_TRIGGER;

			case SDLK_DOWN:
				return EVENT::RELEASE_DOWN_BUTTON;

			case SDLK_LEFT:
				return EVENT::RELEASE_LEFT_BUTTON;

			case SDLK_RIGHT:
				return EVENT::RELEASE_RIGHT_BUTTON;

			case SDLK_UP:
				return EVENT::RELEASE_UP_BUTTON;
			
			case SDLK_ESCAPE:
				return EVENT::RELEASE_START_BUTTON;
			}
		default:
			return EVENT::INACTION;
		}
	}
}
