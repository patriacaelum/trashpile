#include "dialogue.hpp"


DialogueScript& Dialogue::operator[](const ID id) {
	return scripts[id];
}


DialogueScript& Dialogue::at(const ID id) {
	return scripts.at(id);
}
