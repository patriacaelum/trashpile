/** sprite_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `Sprite` component.
*/


#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/components/sprite.hpp"
#include "../src/json.hpp"


TEST_CASE("Sprite component tests", "[sprite][component]") {
	JSON data = R"(
		{
			"sprite": {
				"filename": "../../img/test.png",
				"sprite": {
					"x": 0,
					"y": 1,
					"w": 2,
					"h": 3
				}
			},
			"bad_sprite": {
				"foo": "bar"
			}
		}
	)"_json;

	SECTION("Sprite component default constructor") {
		Sprite sprite = data.at("sprite").get<Sprite>();

		REQUIRE(sprite.filename == "../../img/test.png");
		REQUIRE(sprite.sprite.x == 0);
		REQUIRE(sprite.sprite.y == 1);
		REQUIRE(sprite.sprite.w == 2);
		REQUIRE(sprite.sprite.h == 3);
	}

	SECTION("Sprite component bad key") {
		REQUIRE_THROWS(data.at("bad_sprite").get<Sprite>());
	}
}
