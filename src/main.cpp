/** main.cpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The main game loop that executes all of the code.
*/


#define SDL_MAIN_HANDLED


#include <SDL2/SDL.h>

#include "init.hpp"
#include "engine.hpp"


/** The `main` game loop.

This function first calls `initAll` to initialize `SDL2` and the
logger.

The main game loop consists of:
  - Processing player input from the `InputManager`.
  - The input is then given to the `Updater` where the game logic is computed.
  - The `Window` is then rendered by the `Renderer` (this is performed in the
	`Updater`).

When the `main` loop is exited, the `Updater` is destroyed and `SDL2`
closes.

\param argc		the number of command line arguments given.
\param argv		a vector of the command line arguments given.
\returns		`0` if the application initialized and quit without errors, `1`
				otherwise.
*/
int main(int argc, char* argv[]) {
	SDL_SetMainReady();
	if (!initAll()) {
		return 1;
	}

	// Main game loop
	{
		Engine engine = Engine();
		bool running = true;
		while (running) {
			running = engine.run();
		}
	}

	quitAll();

	return 0;
}
