#include "room.hpp"


Room::Room(const ID id, const JSON& data, Stage& stage)
:   State("room", true, {}, id, data)
{
    stage.mailbox.subscribe(&stage.scene.pda(id), EVENT::ROOM_CHANGE);
}


/** Notifies the state of an event.

The room changes its mode given a `ROOM_CHANGE` event.

\param event
    the event trigger.
\param id
    the entity that posted the event.
\param stage
    the currently loaded stage.

\returns
    a `State` pointer.
*/
State* Room::notify(const EVENT event, const ID sender, Stage& stage) {
    const ID player_id = stage.player();

    if (event == EVENT::ROOM_CHANGE && sender == player_id) {
        SDL_Rect& sprite = stage.scene.sprite(id).sprite;
        Knowledge& knowledge = stage.scene.knowledge(player_id);


        if (stage.isOverlap(id, player_id)) {
            // When player is in room, fully light room
            knowledge.set(id, KNOWLEDGE::FULL);

            _origin = 0;
            _hframes = 0;
        }
        else {
            // When player is not in room, room mode is based on knowledge
            // level
            switch (knowledge.level(id)) {
            case KNOWLEDGE::FULL:
                // When player has left room, knowledge becomes partial
                knowledge.set(id, KNOWLEDGE::PARTIAL);

                _origin = sprite.w;
                _hframes = 0;

                break;

            case KNOWLEDGE::PARTIAL:
                // Partially lit room
                _origin = sprite.w;
                _hframes = 0;

                break;

            default:
                // Darkened room
                _origin = 2 * sprite.w;
                _hframes = 0;

                break;
            }
        }
    }

    return nullptr;
}
