/** roll.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

*/


#ifndef SRC_STATES_ROLL_HPP_
#define SRC_STATES_ROLL_HPP_


#include <algorithm>
#include <array>
#include <cmath>

#include "state.hpp"

#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../components/velocity.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Roll : public State {
public:
    Roll(const ID id, const JSON& data, Stage& stage);

    void enter(Stage& stage) override;

    State* notify(const EVENT event, const ID sender, Stage& stage) override;
    State* update(Stage& stage) override;

private:
    int _diff_index = 0;
    std::array<int, 3> _speed_diff = {6, -4, -2};
};


#endif /* SRC_STATES_ROLL_HPP_ */
