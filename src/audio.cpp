#include "./audio.hpp"


/** Initializes the audio API.
*/
Audio::Audio () {
	int mixer = Mix_OpenAudio(
		MIX_DEFAULT_FREQUENCY,    // Output sampling frequency
		MIX_DEFAULT_FORMAT,       // Output sample format
		2,                        // Output channels (2 for stereo)
		4096                      // Bytes per output sample
	);

	if (mixer == -1) {
		SDL_LogError (
			SDL_LOG_CATEGORY_AUDIO,
			"Audio :: SDL mixer API failed initialization: %s",
			Mix_GetError ()
		);
	}

	this->totalBGM = 0;
	this->totalSFX = 0;
}


/** Plays and loops a BGM track.

A new channel is allocated if all channels are currently being used.

\param track    path to the audio file.
\param vol      volume of the channel, ranges from `0`-`100`, with `100`
                as maximum volume.
\returns        channel number the track is being played on.
*/
int Audio::playLoop (const char* track, int vol) {
	if (Mix_GroupAvailable (BGMtag) == -1) {
		this->totalBGM += 1;
		Mix_AllocateChannels (totalBGM + totalSFX);
		Mix_GroupChannel ((totalBGM + totalSFX) -1, BGMtag);
		SDL_LogInfo (
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: channel allocated as bgm for '%s'",
			track
		);
	}

	Mix_Chunk *sample;
	sample = Mix_LoadWAV(track);

	if (sample == NULL){
		SDL_LogWarn (
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: error loading chunk: %s",
			Mix_GetError ()
		);

		return -1;
	} else {
		int freeChan = Mix_GroupAvailable (BGMtag);
		int test = Mix_PlayChannelTimed (freeChan, sample, -1, -1);
		Mix_Volume (test, (int) MIX_MAX_VOLUME * ((float) vol / 100.));

		return test;
	}
}


/** Plays an SFX track once through.

A new channel is allocated if there are none available. The SFX track
is only played once through and is not looped.

\param track    path to the audio file.
\param vol      volume of the channel, ranges from `0`-`100`, with `100`
                as maximum volume.
\returns        channel number the track is being played on.
 */
int Audio::playSFX (const char * track, int vol) {
	if (Mix_GroupAvailable (SFXtag) == -1) {
		this->totalSFX += 1;
		Mix_AllocateChannels (totalSFX + totalBGM);
		Mix_GroupChannel ((totalSFX + totalBGM) - 1, SFXtag);
		SDL_LogInfo (
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: channel allocated as SFX for '%s'",
			track
		);
	}

	Mix_Chunk *sample;
	sample = Mix_LoadWAV (track);


	if (sample == NULL) {
		SDL_LogWarn (
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: error loading chunk: %s",
			Mix_GetError ()
		);

		return -1;
	} else {
		int freeChan = Mix_GroupAvailable (SFXtag);
		int test = Mix_PlayChannelTimed (freeChan, sample, 0, -1);
		Mix_Volume (test, (int) MIX_MAX_VOLUME * ((float) vol / 100.));

		return test;
	}
}


/** Total number of channels allocated for SFX.
*/
int Audio::numSFX () {
	return totalSFX;
}


/** Total number of channels allocated for BGM.
*/
int Audio::numBGM () {
	return totalBGM;
}


/** Checks if the specified channel is currently being used.

\param chan    the channel number.
\returns       `true` if the channel is currently being used, `false`
               otherwise.
*/
bool Audio::checkChannel (int chan) {
	int test = Mix_Playing (chan);

	if (test == 0) {
		SDL_LogVerbose(
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: channel '%d' is not playing",
			chan
		);

		return false;
	} else {
		SDL_LogVerbose(
			SDL_LOG_CATEGORY_AUDIO,
			"audio :: channel '%d' is playing",
			chan
		);

		return true;
	}
}


/** Adjusts the volume of the specified channel.

\param chan    the channel number.
\param vol     the desired volume of the channel. The value is in the
               range `0`-`100`, where `100` is the maximum volume.
*/
void Audio::changeVol (int chan, int vol) {
	Mix_Volume (chan, MIX_MAX_VOLUME * ((float) vol / 100.));
	SDL_LogInfo(
		SDL_LOG_CATEGORY_AUDIO,
		"audio :: changed volume of channel %d to %d percent",
		chan,
		vol
	);
}


/** Replaces a BGM track with another to loop.

\param chan     channel number of the track to be replaced.
\param track    path to the replacement audio file.
*/
void Audio::replace (int chan, const char* track) {
	Mix_Chunk* sample;
	sample = Mix_LoadWAV(track);
	Mix_PlayChannel(chan, sample, -1);
}


/** Stops all BGM channels.
*/
void Audio::stopBGM(){
	Mix_HaltGroup(BGMtag);
}


/** Stops all SFX channels.
*/
void Audio::stopSFX(){
	Mix_HaltGroup(SFXtag);
}


/** Closes a channel or deallocates all channels and closes audio API.

\param chan    the channel number to be halted. All channels are halted
               and deallocated if `-1` is specified. This will also
               close the audio API.
*/
void Audio::close (int chan) {
	if (chan == -1) {
		Mix_HaltGroup (BGMtag);
		Mix_HaltGroup (SFXtag);
		Mix_AllocateChannels (0);
		Mix_CloseAudio ();
	} else if (chan >= 0) {
		Mix_HaltChannel (chan);
	}
}

