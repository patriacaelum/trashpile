#include "attack_short.hpp"


AttackShort::AttackShort(const ID id, const JSON& data, Stage& stage)
:	State("attack_short", false, {STATES::ATTACK_LONG}, id, data), 
	_b_button_pressed(false) 
{}


/** Enters the short attack state.

The entity velocity is set to zero. If a subentity exists, then its position is
updated to the superentity.

\param stage
	the currently loaded stage.
*/
void AttackShort::enter(Stage& stage) {
	_b_button_pressed = true;

	Velocity& velocity = stage.scene.velocity(id);

	velocity.dx = 0;
	velocity.dy = 0;

	State::enter(stage);

	const ID subid = stage.scene.subentity(id);

	if (subid != -1) {
		stage.scene.position(subid) = stage.scene.position(id);
		stage.scene.pda(subid).notify(EVENT::INITIATE, id, stage);
	}
}


/** Notifies the state of an event.

The state tracks if the B button is pressed.

\param event
	the event trigger.
\param sender
	the id of the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a null pointer.
*/
State* AttackShort::notify(const EVENT event, const ID sender, Stage& stage) {
	switch (event) {
	case EVENT::RELEASE_B_BUTTON:
		_b_button_pressed = false;

		break;
	}

	return nullptr;
}


/** Updates the state. The animation is interrupted upon collision with another
entity.

The following states can be returned:

- a `LongAttack` state given the B button is still pressed during the last frame
  of the first sprite.

\param stage
	the currently loaded stage.

\returns
	a `State` pointer.
*/
State* AttackShort::update(Stage& stage) {
	int mass = stage.scene.property(id)["mass"];
	Position& position = stage.scene.position(id);
	Position position_old = position;
	SDL_Rect& sprite = stage.scene.sprite(id).sprite;

	const ID subid = stage.scene.subentity(id);
	const Hitbox& subhitbox = stage.scene.hitbox(subid);
	Position& subposition = stage.scene.position(subid);
	SDL_Rect& subsprite = stage.scene.sprite(subid).sprite;

	// State change to long attack if action button is held for longer than one
	// frame
	if (_b_button_pressed && sprite.x == 0 && _hframes == 1) {
		_finished = true;

		return stage.scene.pda(id)[STATES::ATTACK_LONG];
	}

	ID connection = stage.scene.connector(subid).connection();

	// Establish connection with colliding entity
	if (!stage.scene.connector(subid).isConnected()) {
		connection = stage.findCollision(subid);

		if (connection != -1) {
			stage.connect(subid, connection);
		}
	}

	// Resolve collisions with the subentity
	if (stage.scene.connector(subid).isConnected()) {
		const int subhframes = stage.scene.pda(subid).currentState()->hframes();

		// If the entity is in its extending phase, then change to its
		// symmetrical retracting phase
		if (sprite.x <= 2 * sprite.w) {
			_hframes = (((subsprite.x / sprite.w) % 3) * 5) + subhframes;

			sprite.x = (5 * sprite.w) - sprite.x;
			subsprite.x = (17 * subsprite.w) - subsprite.x;
		}

		const int conmass = stage.scene.property(connection)["mass"];
		Position& conposition = stage.scene.position(connection);
		Position conposition_old = conposition;
		const SDL_Rect consprite = stage.scene.sprite(connection).sprite;

		// Moving the entity is only necessary when the animation changes and
		// is not the last frame, which leads to overlapping with the
		// superentity
		if (subhframes <= 1 && subsprite.x < 14 * subsprite.w) {
			// Find change in distance
			SDL_Rect next_sprite = subsprite;
			next_sprite.x += next_sprite.w;
			int distance = subhitbox.highpoint(subsprite) - subhitbox.highpoint(next_sprite);
			// Move the lighter entity toward the heavier one
			if (2 * mass <= conmass) {
				// Move the subentity toward the connection
				switch (position.d) {
					case DIRECTION::SOUTH:
						position.y += distance;
						subposition.y += distance;
						break;
					case DIRECTION::WEST:
						position.x -= distance;
						subposition.x -= distance;
						break;
					case DIRECTION::EAST:
						position.x += distance;
						subposition.x += distance;
						break;
					case DIRECTION::NORTH:
						position.y -= distance;
						subposition.y -= distance;
						break;
				}

				if (stage.isCollision(id, connection)) {
					position = position_old;
				}
			}
			else if (mass >= 2 * conmass) {
				// Move the connection toward the subentity
				switch (position.d) {
					case DIRECTION::SOUTH:
						conposition.y -= distance;
						break;
					case DIRECTION::WEST:
						conposition.x += distance;
						break;
					case DIRECTION::EAST:
						conposition.x -= distance;
						break;
					case DIRECTION::NORTH:
						conposition.y += distance;
						break;
				}

				if (stage.isCollision(id, connection)) {
					conposition = conposition_old;
				}
			}
			else {
				// Similar masses don't move either
				notifySubentity(EVENT::TERMINATE, id, stage);

				_finished = true;

				return nullptr;
			}
		}
	}

	updateSprite(stage);
	updateSubentity(stage);

	return nullptr;
}

