/** subscription.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Subscription` is the base observer class that subscribes to the `Mailbox`
listens for events. When the subscription is notified of an event, it will
trigger some action.
*/


#ifndef SRC_POSTAL_SERVICE_SUBSCRIPTION_HPP_
#define SRC_POSTAL_SERVICE_SUBSCRIPTION_HPP_


#include "../components/id.hpp"
#include "../globals.hpp"


class Stage;


class Subscription {
public:
    virtual void notify(const EVENT event, const ID sender, Stage& stage) = 0;
};


#endif /* SRC_POSTAL_SERVICE_SUBSCRIPTION_HPP_ */
