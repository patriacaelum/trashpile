last modified: 2017-11-12

# What type of game?

- 2D top-down 
- Possibly: combat (maybe), depends on what story we want to come up with 


# What idea should the whole game revolve around

+ Human nature: morality code 
+ Dystopian game: be watched by the government overtime 
+ Breaking world dystopian: self-opression, drug that makes you feel happy and
  never wants to rebel 
+ Go longer without the drug: people become monster 


# Examples of core concept

+ Mario: jumping
  + To hit blocks and get powerups
  + To defeat enemies
  + To reach top of the flagpole
+ Zelda: exploring
  + Go to any dungeon we want
  + Items from one dungeon may be needed to unlock all secrets
  + Secrets in main world not given on map, just hinted at
+ Dark souls: feeling of relief/accomplishment
  + Enemies are placed strategically, encouraging awareness
  + Enemy attacks are always choreographed so players can learn their attacks
  + Dungeons are designed so shortcuts are interconnected, relief for safety
+ Mario Odyssey: throwing a hat/boomerang
  + To catch different enemies/objects
  + To interact with the environment, like coin blocks, grass, etc.
  + To open doors and the spaceship


# Ideas for our game

+ Feeling of urgency
  + Urgency to get to the next check point to get drug
  + Feeling that you are in danger and need to move quickly
+ Using the environment to change yourself
  + Character that steals power of enemies to gain new abilities
+ Morality
  + Whether you want to kill the enemy or not
+ Change in environment
  + See how your actions affect the environment
  + What you do should have consequences
    + Which actions you take
    + Who you support
    + Who you talk to
  + Focus on how you shape the world
  + Focus on the things you do, not the things you say
  + Open world
+ Stealth 


# What mechanics to make the game fun?

- Person fear other characters 
- Restrictive weapon 
- Push things
- How you steal and the consquence of the method of stealing 
- If you decide to steal, it will have a consequence 

