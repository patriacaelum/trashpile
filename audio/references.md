# General

- [8-bit Music Theory](https://www.youtube.com/channel/UCeZLO2VgbZHeDcongKzzfOw)
  creates more detailed theoretical analyses of video game music and how it's
  used.
- [Holistic Songwriting](https://www.youtube.com/channel/UCLtD67ljlaeXQMV4sb-YzNA)
  has a more theoretical approach of looking at popular music and their
  individual stylistic aspects.
- [The Musical Narrative](http://jasonyu.me) is a collection of essays on the
  the use of musical themes and development, and how they aid in the narrative
  of telling stories in video game music.
- [Polyphonic](https://www.youtube.com/channel/UCXkNod_JcH7PleOjwK_8rYQ) creates
  video essays on popular musicians.
- [Free to use sounds](https://freetousesounds.com/) has many soundscapes and
  sounds to sample.

  
# Ideas

## Input based tempo

For some dramatic sections, the music could be affected by the rapidity of input
from the player rather than of circumstance, such as

- Syncing the rhythm of the player to the timing of certain inputs
- Silence or ambience when the player is still and waiting for safety
- Tempo is based on quick movement or actions
- Dynamics change as the player nears visual climax

## Japanese Jazz

- Minor melodies with major harmonies
- Emphasis on upward leaps on weak beats
