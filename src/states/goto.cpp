#include "goto.hpp"


Goto::Goto(const ID id, const JSON& data, Stage& stage)
:   Pathfinding("goto", false, {STATES::PURSUE}, id, data)
{
}


/** Assigns the position of the connection as the target.

The entity should have a path defined for the entity to go to, otherwise, the
state immediately exits.

\param stage
    the currently loaded stage.
*/
void Goto::enter(Stage& stage) {
    Pathfinding::enter(stage);

    if (stage.scene.path(id).empty()) {
        _finished = true;
    }
}


/** The entity will follow the path until the target location is complete.

When the target location has been reached, the pursue state is returned and
will pursue the player character if they are in range.

\param stage
    the currently loaded stage.
*/
State* Goto::update(Stage& stage) {
    followPath(stage);

    if (_finished) {
        return stage.scene.pda(id)[STATES::PURSUE];
    }

    return Pathfinding::update(stage);
}
