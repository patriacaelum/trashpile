/** mailbox.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Mailbox` receives events that are posted from multiple entities and
distributes them to the entities that subscribed.

Posted events should be events where the poster does not know the ID of the
receiver. If the ID of the receiver is known, it is better to simply notify
that entity directly.  Otherwise, the events are queued up and distributed all
at once.

Subscriptions can be ID agnostic or ID specific. When subscribing to an event,
the entity can specify only subscribing to an event posted by a specific entity
by specifying the ID or subscribe to an event posted by any entity.
*/


#ifndef SRC_POSTAL_SERVICE_MAILBOX_HPP_
#define SRC_POSTAL_SERVICE_MAILBOX_HPP_


#include <algorithm>
#include <array>
#include <iterator>
#include <vector>

#include "subscription.hpp"
#include "../components/id.hpp"
#include "../globals.hpp"


class Stage;


class Mailbox {
public:
    Mailbox();

    void post(const EVENT event, const ID id = -1);
    void distribute(Stage& stage, const Subscription* subscription = nullptr);

    void subscribe(
        Subscription* subscription, 
        const EVENT event, 
        const ID id = -1
    );
    void unsubscribe(
        const Subscription* subscription, 
        const EVENT event, 
        const ID id = -1
    );

    void reset();

private:
    void distributeFor(
        const EVENT event, 
        const ID list_id, 
        const ID poster_id,
        const Subscription* special_sub,
        Stage& stage
    );
    int wrapIncrement(const int x);

    int _head;
    int _tail;
    std::array<EVENT, PROPERTIES::MAX_ENTITIES> _event_queue;
    std::array<ID, PROPERTIES::MAX_ENTITIES> _id_queue;
    std::unordered_map<EVENT, std::unordered_map<ID, std::vector<Subscription*>>> _subscriptions;
};


#endif /* SRC_POSTAL_SERVICE_MAILBOX_HPP_ */
