/** grappling_hook_inactive.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The idle state of the grappling hook when it is not in use.

The state process is

```
GrapplingHookInactive -> GrapplingHookShort -> GrapplingHookLong
```
*/


#ifndef SRC_STATES_GRAPPLING_HOOK_INACTIVE_HPP_
#define SRC_STATES_GRAPPLING_HOOK_INACTIVE_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../scene.hpp"


class GrapplingHookInactive : public State {
public:
	GrapplingHookInactive(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_GRAPPLING_HOOK_INACTIVE_HPP_ */
