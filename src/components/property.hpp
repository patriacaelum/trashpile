/** property.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The property component stores all properties of an entity that don't require
an entire class to themselves.

The property component should follow the format:

```
{
     "property": {
          "string": int,
          ...
     }
}
```

At the moment, the following properties are used:

  - "mass": the mass of the object (in kg).
  - "pickup": any value other than -1 indicates the item can be picked up.
  - "equip": any value other than -1 indicates the item can be equipped.

*/


#ifndef SRC_COMPONENTS_PROPERTY_HPP_
#define SRC_COMPONENTS_PROPERTY_HPP_


#include <stdexcept>
#include <string>
#include <unordered_map>

#include "id.hpp"


class Property : public std::unordered_map<std::string, int> {
public:
     int at(const std::string key);

     ID id;
};


#endif /* SRC_COMPONENTS_PROPERTY_HPP_ */
