/** pathfinding.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Pathfinding` state is a base state that provides the interface for any
state that requires pathfinding.

Any states that require pathfinding should inherit from this class.
*/


#ifndef SRC_STATES_PATHFINDING_HPP_
#define SRC_STATES_PATHFINDING_HPP_


#include <functional>
#include <vector>

#ifdef __GNUC__
#include <thread>
#endif

#ifdef __MINGW32__
#include <mingw.thread.h>
#endif

#include <SDL2/SDL.h>

#include "state.hpp"

#include "../components/id.hpp"
#include "../components/path.hpp"
#include "../components/position.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../pathfinding/distance.hpp"
#include "../pathfinding/graph.hpp"
#include "../pathfinding/graph_node.hpp"
#include "../stage.hpp"


class Pathfinding : public State {
public:
    Pathfinding(
        const std::string name,
        const bool is_initial,
        const std::vector<STATES> dependent_states,
        const ID id,
        const JSON& data
    );

protected:
    void startPathfinding(Stage& stage);
    void setPath(Stage& stage);
    void followPath(Stage& stage);

    virtual void setStart(Stage& stage);
    virtual void setGoal(Stage& stage);
    void setGraph(Stage& stage);
    void setNodes();
    void findPath();

    /// The starting location for the pathfinding process. This should be in the
    /// scaled coordinates.
    SDL_Rect _start;
    /// The target location for the pathfinding process. This should be in the
    /// scaled coordinates.
    SDL_Rect _goal;

    /// The thread that runs the pathfinding process.
    std::thread _thread;

    /// The graph representing the scene at the time the thread is running.
    Graph _graph;
    /// The multiplicative scale for the graph.
    const int _scale = 10;

    /// The starting location of the entity in graph coordinates.
    GraphNode _start_node;
    /// The target location of the entity in graph coordinates.
    GraphNode _goal_node;
};

#endif /* SRC_STATES_PATHFINDING_HPP_ */
