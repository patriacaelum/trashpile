#include "walk.hpp"


Walk::Walk(const ID id, const JSON& data, Stage& stage)
:	State("walk", false, {STATES::ROLL}, id, data)
{}


/** Enters the state and sets the velocity.

\param stage
	the currently loaded stage.
*/
void Walk::enter(Stage& stage) {
	State::enter(stage);

	Velocity& velocity = stage.scene.velocity(id);

	switch (stage.scene.position(id).d) {
	case DIRECTION::SOUTH:
		velocity.dy = _speed;

		break;

	case DIRECTION::WEST:
		velocity.dx = -_speed;

		break;

	case DIRECTION::EAST:
		velocity.dx = _speed;

		break;

	case DIRECTION::NORTH:
		velocity.dy = -_speed;

		break;
	}
}


/** Notifies the state of an event.

\param event
	the event trigger.
\param sender
	the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a state pointer.
*/
State* Walk::notify(const EVENT event, const ID sender, Stage& stage) {
	SDL_Rect& sprite = stage.scene.sprite(id).sprite;
	Position& position = stage.scene.position(id);
	Velocity& velocity = stage.scene.velocity(id);

	switch (event) {
	case EVENT::PRESS_B_BUTTON:
		return stage.scene.pda(id)[STATES::ROLL];

	case EVENT::PRESS_DOWN_BUTTON:
		sprite.x = 0;
		sprite.y = _origin + 0 * sprite.h;
		position.d = DIRECTION::SOUTH;
		velocity.dy = 2;

		break;

	case EVENT::PRESS_LEFT_BUTTON:
		sprite.x = 0;
		sprite.y = _origin + 1 * sprite.h;
		position.d = DIRECTION::WEST;
		velocity.dx = -2;

		break;

	case EVENT::PRESS_RIGHT_BUTTON:
		sprite.x = 0;
		sprite.y = _origin + 2 * sprite.h;
		position.d = DIRECTION::EAST;
		velocity.dx = 2;

		break;

	case EVENT::PRESS_UP_BUTTON:
		sprite.x = 0;
		sprite.y = _origin + 3 * sprite.h;
		position.d = DIRECTION::NORTH;
		velocity.dy = -2;

		break;

	case EVENT::PRESS_RIGHT_TRIGGER:
		velocity.dx *= _speed;
		velocity.dy *= _speed;

		break;

	case EVENT::RELEASE_DOWN_BUTTON:
		velocity.dy = 0;
		position.d = findDirection(velocity, position.d);

		break;

	case EVENT::RELEASE_LEFT_BUTTON:
		velocity.dx = 0;
		position.d = findDirection(velocity, position.d);

		break;

	case EVENT::RELEASE_RIGHT_BUTTON:
		velocity.dx = 0;
		position.d = findDirection(velocity, position.d);

		break;

	case EVENT::RELEASE_UP_BUTTON:
		velocity.dy = 0;
		position.d = findDirection(velocity, position.d);

		break;

	case EVENT::RELEASE_RIGHT_TRIGGER:
		velocity.dx /= _speed;
		velocity.dy /= _speed;

		break;

	case EVENT::TERMINATE:
		_finished = true;

		break;
	}

	if (velocity.dx == 0 && velocity.dy == 0) {
		_finished = true;

		return nullptr;
	}

	return update(stage);
}


/** Updates the state.

\param input
	a global input value.

\returns
	a state pointer.
 */
State* Walk::update(Stage& stage) {
	SDL_Rect& sprite = stage.scene.sprite(id).sprite;
	Position& position = stage.scene.position(id);
	Velocity& velocity = stage.scene.velocity(id);

	position.x += velocity.dx;
	position.y += velocity.dy;

	// Resolve collision by moving backward
	if (stage.findCollision(id) != -1) {
		position.x -= velocity.dx;
		position.y -= velocity.dy;
	}

	return State::update(stage);
}


DIRECTION Walk::findDirection(
	const Velocity& velocity, 
	const DIRECTION direction
) {
	if (velocity.dy > 0) {
		return DIRECTION::SOUTH;
	}
	else if (velocity.dx < 0) {
		return DIRECTION::WEST;
	}
	else if (velocity.dx > 0) {
		return DIRECTION::EAST;
	}
	else if (velocity.dy < 0) {
		return DIRECTION::NORTH;
	}
	else {
		return direction;
	}
}
