/** hitbox.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Hitbox` for each entity is a collection of rectangles. Each group of
rectangles is mapped to the x and y coordinates of the sprite on the
spritesheet. The rectangles are relative to the individual spritebox.

The component data must be in the form

```
{
	"hitbox": {
		"third hitbox of animation": {
			"sprite": {
				"x": 100,
				"y": 200,
				"w": 50,
				"h": 25
			},
			"boxes": {
				"foot": {
					"x": 10,
					"y": 20,
					"w": 30,
					"h": 5
				},
				"head": {
					"x": 15,
					"y": 0,
					"w": 20,
					"h": 10
				}
			}
		}
	}
}
```

Each hitbox must have the key "sprite", which specifies the sprite to be
associated with the rectangles defined with the key "boxes". The keys of each
hitbox themselves are arbitrary.
*/


#ifndef SRC_COMPONENTS_HITBOX_HPP_
#define SRC_COMPONENTS_HITBOX_HPP_


#include <algorithm>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <SDL2/SDL.h>

#include "../SDL_extras.hpp"
#include "../globals.hpp"
#include "id.hpp"


/** Inject a hashing function for an `SDL_Rect` into the std namespace.
*/
namespace std {
	template<>
	struct hash<SDL_Rect> {
		std::size_t operator()(const SDL_Rect& rect) const {
			int h = 23;
			h = h * 37 + rect.x;
			h = h * 37 + rect.y;
			h = h * 37 + rect.w;
			h = h * 37 + rect.h;

			return std::hash<int>{}(h);
		}
	};
}


class Hitbox {
public:
	std::vector<SDL_Rect> hitbox(
		const SDL_Rect key,
		const DIRECTION direction = DIRECTION::SOUTH,
		const SDL_Point& centre = {0, 0}
	) const;

	int highpoint(const SDL_Rect& key) const;
	SDL_Rect unionbox() const;

	void set(const SDL_Rect& key, const std::vector<SDL_Rect>& value);

	/// ID of the associated entity
	ID id = -1;

private:
	/// Maps a sprite to its hitbox
	std::unordered_map<SDL_Rect, std::vector<SDL_Rect>> _hitboxes;

	/// Maps a sprite to the lowest part of the hitbox
	std::unordered_map<SDL_Rect, int> _highpoints;

	/// A union hitbox is a hitbox that encompasses all of the hitboxes of this
	/// entity
	SDL_Rect _unionbox = {999999, 999999, -999999, -999999};
};


#endif /* SRC_COMPONENTS_HITBOX_HPP_ */
