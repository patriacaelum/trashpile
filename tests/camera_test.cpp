/** camera_test.cpp

         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||

Unit test for `Camera`.
*/


#include <catch2/catch.hpp>
#include <SDL2/SDL.h>

#include "../src/camera.hpp"


TEST_CASE("Camera tests", "[camera]") {
	SECTION("Camera default constructor") {
		Camera camera = Camera();

		REQUIRE(camera.frame.x == 0);
		REQUIRE(camera.frame.y == 0);
		REQUIRE(camera.frame.w == 0);
		REQUIRE(camera.frame.h == 0);
	}

	SECTION("Camera constructor") {
		SDL_Rect frame = {0, 10, 640, 480};
		Camera camera = Camera(frame);

		REQUIRE(camera.frame.x == frame.x);
		REQUIRE(camera.frame.y == frame.y);
		REQUIRE(camera.frame.w == frame.w);
		REQUIRE(camera.frame.h == frame.h);
	}
}
