#include "json.hpp"


/** Loads and parses the specified JSON file.

\param filename		path to the JSON file.

\returns			the parsed json data.
*/
JSON loadJSONFile(const std::string filename) {
	std::ifstream file;
	file.open(filename, std::ifstream::in);

	if (file.fail()) {
		SDL_LogError(
			SDL_LOG_CATEGORY_INPUT,
			"JSON :: failed to open file '%s'",
			filename.c_str()
		);

		throw std::out_of_range("Failed to open JSON file");
	}

	JSON data;
	file >> data;

	file.close();

	SDL_LogDebug(
		SDL_LOG_CATEGORY_INPUT,
		"JSON :: successfully loaded data from '%s'",
		filename.c_str()
	);

	return data;
}


/** Loads JSON data from the specified key, and loads a new file if specified.

\param json		the JSON data.
\param key		the key in the JSON data to be loaded.

\returns		the loaded JSON data.
*/
JSON loadJSONData(const JSON& json, const std::string key) {
	JSON data;

	if (json.is_string()) {
		data = loadJSONFile(json).at(key);
	}
	else {
		data = json;
	}

	return data;
}


void to_json(JSON& json, const SDL_Point& point) {
	json["x"] = point.x;
	json["y"] = point.y;
}


void from_json(const JSON& json, SDL_Point& point) {
	json.at("x").get_to(point.x);
	json.at("y").get_to(point.y);
}


void to_json(JSON& json, const std::vector<SDL_Point>& points) {
	for (int i = 0; i < points.size(); ++i) {
		json[i] = points[i];
	}
}


void from_json(const JSON& json, std::vector<SDL_Point>& points) {
	for (const auto& item: json.items()) {
		points.push_back(item.value().get<SDL_Point>());
	}
}


void to_json(JSON& json, const SDL_Rect& rect) {
	json["x"] = rect.x;
	json["y"] = rect.y;
	json["w"] = rect.w;
	json["h"] = rect.h;
}


void from_json(const JSON& json, SDL_Rect& rect) {
	json.at("x").get_to(rect.x);
	json.at("y").get_to(rect.y);
	json.at("w").get_to(rect.w);
	json.at("h").get_to(rect.h);
}


void to_json(JSON& json, const std::vector<SDL_Rect>& rects) {
	for (int i = 0; i < rects.size(); ++i) {
		json[i] = rects[i];
	}
}


void from_json(const JSON& json, std::vector<SDL_Rect>& rects) {
	for (const auto& item: json.items()) {
		rects.push_back(item.value().get<SDL_Rect>());
	}
}


void to_json(JSON& json, const DialogueNode& node) {
	json["id"] = node.id;
	json["next_id"] = node.next_id;
	json["type"] = node.node_type;

	if (node.node_type == "line") {
		json["speaker"] = node.speaker;
		json["text"] = node.text;
	}
	else if (node.node_type == "choices") {
		json["choices"] = node.choices;
	}
	else if (node.node_type == "setter") {
		json["key"] = node.key;
		json["value"] = node.value;
	}
}


void from_json(const JSON& json, DialogueNode& node) {
	json.at("id").get_to(node.id);
	json.at("next_id").get_to(node.next_id);
	json.at("type").get_to(node.node_type);

	if (node.node_type == "line") {
		json.at("speaker").get_to(node.speaker);
		json.at("text").get_to(node.text);
	}
	else if (node.node_type == "choices") {
		json.at("choices").get_to(node.choices);
	}
	else if (node.node_type == "setter") {
		json.at("key").get_to(node.key);
		json.at("value").get_to(node.value);
	}
}


void to_json(JSON& json, const DialogueScript& script) {
}


void from_json(const JSON& json, DialogueScript& script) {
	ID first_node_id = json.at("first_node");

	script.first_node_id = first_node_id;
	script.current_node_id = first_node_id;

	for (const auto& item: json.at("nodes").items()) {
		ID current_node_id = item.value().at("id");

		script.nodes[current_node_id] = item.value().get<DialogueNode>();

		if (item.value().at("next_id").empty()) {
			script.last_node_id = current_node_id;
		}
	}
}


void to_json(JSON& json, const Hitbox& hitbox) {
}


void from_json(const JSON& json, Hitbox& hitbox) {
	JSON data = loadJSONData(json, "hitbox");

	for (const auto& item: data.items()) {
		SDL_Rect key = item.value().at("sprite").get<SDL_Rect>();
		std::vector<SDL_Rect> value = item.value().at("boxes").get<std::vector<SDL_Rect>>();

		hitbox.set(key, value);
	}
}


void to_json(JSON& json, const Position& position) {
	json["x"] = position.x;
	json["y"] = position.y;
	json["z"] = position.z;
	json["d"] = position.d;
}


void from_json(const JSON& json, Position& position) {
	JSON data = loadJSONData(json, "position");

	data.at("x").get_to(position.x);
	data.at("y").get_to(position.y);
	data.at("z").get_to(position.z);

	const std::string direction = data.at("d").get<std::string>();

	position.d = DIRECTION_STRINGS[direction];
}


void to_json(JSON& json, const Property& property) {
	for (const auto& item: property) {
		json[item.first] = item.second;
	}
}


void from_json(const JSON& json, Property& property) {
	JSON data = loadJSONData(json, "property");

	for (const auto& item: data.items()) {
		property[item.key()] = item.value();
	}
}


void to_json(JSON& json, const Sprite& sprite) {
	json["filename"] = sprite.filename;
	json["sprite"] = sprite.sprite;
}


void from_json(const JSON& json, Sprite& sprite) {
	JSON data = loadJSONData(json, "sprite");

	data.at("filename").get_to(sprite.filename);
	data.at("sprite").get_to(sprite.sprite);
}


void to_json(JSON& json, const Velocity& velocity) {
	json["dx"] = velocity.dx;
	json["dy"] = velocity.dy;
}


void from_json(const JSON& json, Velocity& velocity) {
	JSON data = loadJSONData(json, "velocity");

	data.at("dx").get_to(velocity.dx);
	data.at("dy").get_to(velocity.dy);
}

