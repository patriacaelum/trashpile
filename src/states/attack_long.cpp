#include "attack_long.hpp"


AttackLong::AttackLong(const ID id, const JSON& data, Stage& stage)
:	State("attack_long", false, {}, id, data)
{}


/** Enters the long attack state.

\param stage
	the currently loaded stage.
*/
void AttackLong::enter(Stage& stage) {
	State::enter(stage);

	ID subid = stage.scene.subentity(id);

	if (subid != -1) {
		stage.scene.pda(subid).notify(EVENT::INITIATE, id, stage);
	}
}


/** Updates the state and its subentity if it exists.

The animation is only interrupted if the subentity collides with another entity.

\param stage
	the currently loaded stage.

\returns
	a null pointer.
*/
State* AttackLong::update(Stage& stage) {
	int mass = stage.scene.property(id)["mass"];
	Position& position = stage.scene.position(id);
	Position position_old = position;
	SDL_Rect& sprite = stage.scene.sprite(id).sprite;

	const ID subid = stage.scene.subentity(id);
	const Hitbox& subhitbox = stage.scene.hitbox(subid);
	Position& subposition = stage.scene.position(subid);
	SDL_Rect& subsprite = stage.scene.sprite(subid).sprite;

	ID connection = stage.scene.connector(subid).connection();

	// Establish connection with colliding entity
	if (!stage.scene.connector(subid).isConnected()) {
		connection = stage.findCollision(subid);

		if (connection != -1) {
			stage.connect(subid, connection);
		}
	}

	// Resolve collisions with the subentity
	if (stage.scene.connector(subid).isConnected()) {
		const int subhframes = stage.scene.pda(subid).currentState()->hframes();

		// If the entity is in its extending phase, then change to its
		// symmetrical retracting phase
		if (sprite.x <= 2 * sprite.w) {
			_hframes = (((subsprite.x / sprite.w) % 3) * 5) + subhframes;

			sprite.x = (9 * sprite.w) - sprite.x;
			subsprite.x = (29 * subsprite.w) - subsprite.x;
		}

		const int conmass = stage.scene.property(connection)["mass"];
		Position& conposition = stage.scene.position(connection);
		Position conposition_old = conposition;
		const SDL_Rect consprite = stage.scene.sprite(connection).sprite;

		// Moving the entity is only necessary when the animation changes and
		// is not the last frame, which leads to overlapping with the
		// superentity
		if (subhframes <= 1 && subsprite.x < 26 * subsprite.w) {
			// Find change in distance
			SDL_Rect next_sprite = subsprite;
			next_sprite.x += next_sprite.w;
			int distance = subhitbox.highpoint(subsprite) - subhitbox.highpoint(next_sprite);
			// Move the lighter entity toward the heavier one
			if (2 * mass <= conmass) {
				// Move the subentity toward the connection
				switch (position.d) {
					case DIRECTION::SOUTH:
						position.y += distance;
						subposition.y += distance;
						break;
					case DIRECTION::WEST:
						position.x -= distance;
						subposition.x -= distance;
						break;
					case DIRECTION::EAST:
						position.x += distance;
						subposition.x += distance;
						break;
					case DIRECTION::NORTH:
						position.y -= distance;
						subposition.y -= distance;
						break;
				}

				if (stage.isCollision(id, connection)) {
					position = position_old;
				}
			}
			else if (mass >= 2 * conmass) {
				// Move the connection toward the subentity
				switch (position.d) {
					case DIRECTION::SOUTH:
						conposition.y -= distance;
						break;
					case DIRECTION::WEST:
						conposition.x += distance;
						break;
					case DIRECTION::EAST:
						conposition.x -= distance;
						break;
					case DIRECTION::NORTH:
						conposition.y += distance;
						break;
				}

				if (stage.isCollision(id, connection)) {
					conposition = conposition_old;
				}
			}
			else {
				// Similar masses don't move either
				notifySubentity(EVENT::TERMINATE, id, stage);

				_finished = true;

				return nullptr;
			}
		}
	}

	updateSprite(stage);
	updateSubentity(stage);

	return nullptr;
}
