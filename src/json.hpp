/** json.cpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

Loads JSON files to convert them into component objects to be loaded into scenes.
*/


#ifndef SRC_JSON_HPP_
#define SRC_JSON_HPP_


#include <fstream>
#include <exception>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>
#include <SDL2/SDL.h>

#include "components/dialogue_node.hpp"
#include "components/dialogue_script.hpp"
#include "components/hitbox.hpp"
#include "components/position.hpp"
#include "components/property.hpp"
#include "components/sprite.hpp"
#include "components/velocity.hpp"

#include "globals.hpp"
#include "SDL_extras.hpp"


using JSON = nlohmann::json;


JSON loadJSONFile(const std::string filename);
JSON loadJSONData(const JSON& json, const std::string key);


// Implicit conversions for SDL primitives
void to_json(JSON& json, const SDL_Point& point);
void from_json(const JSON& json, SDL_Point& point);


void to_json(JSON& json, const std::vector<SDL_Point>& points);
void from_json(const JSON& json, std::vector<SDL_Point>& points);


void to_json(JSON& json, const SDL_Rect& rect);
void from_json(const JSON& json, SDL_Rect& rect);


void to_json(JSON& json, const std::vector<SDL_Rect>& rects);
void from_json(const JSON& json, std::vector<SDL_Rect>& rects);


// Implicit conversions for components

void to_json(JSON& json, const DialogueNode& node);
void from_json(const JSON& json, DialogueNode& node);


void to_json(JSON& json, const DialogueScript& script);
void from_json(const JSON& json, DialogueScript& script);


void to_json(JSON& json, const Hitbox& hitbox);
void from_json(const JSON& json, Hitbox& hitbox);


void to_json(JSON& json, const Position& position);
void from_json(const JSON& json, Position& position);


void to_json(JSON& json, const Property& property);
void from_json(const JSON& json, Property& property);


void to_json(JSON& json, const Sprite& sprite);
void from_json(const JSON& json, Sprite& sprite);


void to_json(JSON& json, const Velocity& velocity);
void from_json(const JSON& json, Velocity& velocity);


#endif /* SRC_JSON_HPP_ */
