/** dialogue.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Dialogue` component maps all the IDs that this ID can speak to to a
`DialogueScript` object.
*/


#ifndef SRC_COMPONENTS_DIALOGUE_HPP_
#define SRC_COMPONENTS_DIALOGUE_HPP_


#include <unordered_map>

#include "dialogue_script.hpp"
#include "id.hpp"


class Dialogue {
public:
	DialogueScript& operator[](const ID id);
	DialogueScript& at(const ID id);

	/// ID of the associated entity
	ID id = -1;

     /// Maps the ID of the conversation partner to the script
	std::unordered_map<ID, DialogueScript> scripts;
};


#endif /* SRC_COMPONENTS_DIALOGUE_HPP_ */
