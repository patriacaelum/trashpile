# Algorithms
- [Lecture notes](http://jeffe.cs.illinois.edu/teaching/algorithms) on various
  algorithms, which covers topics like recursion, randomization, amortization,
  graph algorithms, optimization, and hard problems.
- [Mathemathical algorithms](http://essentialmath.com/index.htm) for things
  related to games.
  - [Video game cameras](https://www.youtube.com/watch?v=tu-Qe66AvtY) are
    discussed in this talk, specifically on the use of Perlin noise for camera
    shake and the use of Voronoi split-screen.
- [Paper on GOAP](http://alumni.media.mit.edu/~jorkin/gdc2006_orkin_jeff_fear.pdf)
  which discusses programming finite state machines and the goal-oriented action
  plan scheme for creating the AI for F.E.A.R.
  
## Collision Engines
- This is a tutorial on a simple implementation for a collision system.
  - [Part I](https://www.toptal.com/game/video-game-physics-part-i-an-introduction-to-rigid-body-dynamics)
  - [Part II](https://www.toptal.com/game/video-game-physics-part-ii-collision-detection-for-solid-objects)
  - [Part III](https://www.toptal.com/game/video-game-physics-part-iii-constrained-rigid-body-simulation)
  
## Skies
- [Advected Textures](http://www-evasion.imag.fr/Publications/2003/Ney03/neyret161.pdf)
  shows a procedural method for making looping blending textures.
- [Flow Visualization Using Moving Textures](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=2515B9299AFBC215B41DA487A9C5CF11?doi=10.1.1.42.2372&rep=rep1&type=pdf)
  shows mathematical ways to create a sky that moves more realistically.

# C++

- [C++ Programming for Game Developers: Module II](http://index-of.co.uk/Game-Development/Programming/C++%20Module%20II.pdf)
  is the second book in the series focusing on classes, error handling, event
  driven programming, menus, control schemes, animation, and colliders.

# Simple DirectMedia Layer (SDL2)

- [The Official Documentation](http://wiki.libsdl.org/FrontPage) for SDL2, which
  is a cross-platform development library that basically just gives access to
  the hardware and nothing else. It works with C++, but there are also wrappers
  for other languages, like Python
  ([PySDL2](https://pysdl2.readthedocs.io/en/latest/)).
- [Headerfile](http://headerphile.com) is a blog with some useful tutorials.
- [Lazy Foo](http://lazyfoo.net/tutorials/SDL/) has some tutorials on SDL2 using
  C++. 
- [TwinklebearDev](http://www.willusher.io/pages/sdl2/) has a short tutorial
  that shows you some of the basics.
