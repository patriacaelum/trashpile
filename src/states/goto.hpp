/** goto.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

The `Goto` state is a transitionary state where the entity moves toward the
specified location.
*/


#ifndef SRC_STATES_GOTO_HPP_
#define SRC_STATES_GOTO_HPP_


#include <queue>
#include <vector>

#include <SDL2/SDL.h>

#include "pathfinding.hpp"

#include "../components/id.hpp"
#include "../components/path.hpp"
#include "../components/position.hpp"
#include "../components/velocity.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Goto : public Pathfinding {
public:
    Goto(const ID id, const JSON& data, Stage& stage);

    void enter(Stage& stage) override;

    State* update(Stage& stage) override;
};


#endif /* SRC_STATES_GOTO_HPP_ */
