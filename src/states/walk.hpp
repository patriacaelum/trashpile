/** walk.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

A general moving state that moves at a slow one pixel per frame. The entity is
set in place if it collides with another entity while moving.

The state process is

```
Walk -> ShortAttack -> LongAttack
     -> Listen <-> Speak
     -> Speak <-> Listen
```
*/


#ifndef SRC_STATES_WALK_HPP_
#define SRC_STATES_WALK_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/id.hpp"
#include "../components/position.hpp"
#include "../components/velocity.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../stage.hpp"


class Walk : public State {
public:
	Walk(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

     State* notify(const EVENT event, const ID sender, Stage& stage) override;
	State* update(Stage& stage) override;

private:
     DIRECTION findDirection(
          const Velocity& velocity, 
          const DIRECTION direction
     );

     /// The maximum speed of the walk cycle in any given axis, measured in
     /// pixels/second.
     const int _speed = 2;
};


#endif /* SRC_STATES_WALK_HPP_ */
