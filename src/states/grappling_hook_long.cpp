#include "grappling_hook_long.hpp"


GrapplingHookLong::GrapplingHookLong(
	const ID id,
	const JSON& data,
	Stage& stage
) :	State("grappling_hook_long", false, {}, id, data)
{}


/** Enters the state.
*/
void GrapplingHookLong::enter(Stage& stage) {
	State::enter(stage);

	stage.scene.position(id) = stage.scene.position(stage.scene.superentity(id));
	stage.scene.sprite(id).sprite = {0, _origin, 48, 432};
}


/** Exits the state.

\param stage	the currently loaded stage.
*/
void GrapplingHookLong::exit(Stage& stage) {
	const Connector& connector = stage.scene.connector(id);

	if (connector.isConnected()) {
		stage.disconnect(id, connector.connection());
	}

	State::exit(stage);
}


/** Notifies the long grappling hook state of an event.

The long attack animation is 27 sprites * 5 frames / sprite = 135 frames. The
first 30 frames are the wind up, the next 30 are the extension, the next 30 are
the thorn extension, the next 30 are the thorn retraction, and the last 30 are
the retraction.

The animation cannot be interrupted except by a `TERMINATE` event.

\param event
	the event trigger.
\param sender
	the entity that posted the event.
\param stage
	the currently loaded stage.

\returns
	a null pointer.
*/
State* GrapplingHookLong::notify(
	const EVENT event, 
	const ID sender, 
	Stage& stage
) {
	switch(event) {
	case EVENT::TERMINATE:
		const ID superentity = stage.scene.superentity(id);

		if (sender == superentity) {
			_finished = true;

			return nullptr;
		}

		break;
	}

	return update(stage);
}
