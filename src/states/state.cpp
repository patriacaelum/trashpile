#include "state.hpp"

#include "alert.hpp"
#include "attack_long.hpp"
#include "attack_short.hpp"
#include "door_close.hpp"
#include "door_open.hpp"
#include "dormant.hpp"
#include "goto.hpp"
#include "grappling_hook_inactive.hpp"
#include "grappling_hook_long.hpp"
#include "grappling_hook_short.hpp"
#include "guard.hpp"
#include "idle.hpp"
#include "listen.hpp"
#include "noisemaker_off.hpp"
#include "noisemaker_on.hpp"
#include "roll.hpp"
#include "room.hpp"
#include "speak.hpp"
#include "pursue.hpp"
#include "walk.hpp"


State::State(
	const std::string name,
	const bool is_initial,
	const std::vector<STATES> dependent_states,
	const ID id,
	const JSON& data
) :	id(id),
	_name(name),
	_reference_id(-1),
	_dependent_states(dependent_states),
	_finished(false),
	_hframes(0),
	_origin(data.value("origin", 0)),
	_nsprites(data.value("nsprites", 1)),
	_loops(data.value("loops", true)),
	_max(data.value("max", 15)),
	_is_initial(is_initial)
{}


State::~State() {}


/** Factory creation method.

\param state
	the type of the state to create.
\param id
	the entity ID of the associated entity.
\param data
	the JSON data.
\param stage
	the currently loaded stage.

\returns
	a pointer to the created state object.
*/
State* State::create(
	const STATES state,
	const ID id,
	const JSON& data,
	Stage& stage
) {
	switch (state) {
	case STATES::ALERT:
		return new Alert(id, data, stage);
	case STATES::ATTACK_LONG:
		return new AttackLong(id, data, stage);
	case STATES::ATTACK_SHORT:
		return new AttackShort(id, data, stage);
	case STATES::DOOR_CLOSE:
		return new DoorClose(id, data, stage);
	case STATES::DOOR_OPEN:
		return new DoorOpen(id, data, stage);
	case STATES::DORMANT:
		return new Dormant(id, data, stage);
	case STATES::GOTO:
		return new Goto(id, data, stage);
	case STATES::GRAPPLING_HOOK_INACTIVE:
		return new GrapplingHookInactive(id, data, stage);
	case STATES::GRAPPLING_HOOK_LONG:
		return new GrapplingHookLong(id, data, stage);
	case STATES::GRAPPLING_HOOK_SHORT:
		return new GrapplingHookShort(id, data, stage);
	case STATES::GUARD:
		return new Guard(id, data, stage);
	case STATES::IDLE:
		return new Idle(id, data, stage);
	case STATES::LISTEN:
		return new Listen(id, data, stage);
	case STATES::NOISEMAKER_OFF:
		return new NoisemakerOff(id, data, stage);
	case STATES::NOISEMAKER_ON:
		return new NoisemakerOn(id, data, stage);
	case STATES::PURSUE:
		return new Pursue(id, data, stage);
	case STATES::ROOM:
		return new Room(id, data, stage);
	case STATES::ROLL:
		return new Roll(id, data, stage);
	case STATES::SPEAK:
		return new Speak(id, data, stage);
	case STATES::WALK:
		return new Walk(id, data, stage);
	}

	SDL_LogError(
		SDL_LOG_CATEGORY_ERROR,
		"State :: failed to create state due to missing case, returning `nullptr`"
	);

	return nullptr;
}


/** Returns the states that this state is dependent on.

Returns an empty vector by default.

\returns
	a vector of the states that this state is dependent on.
*/
std::vector<STATES> State::dependentStates() const {
	return _dependent_states;
}


/** Provides all the necessary changes when first entering a state.

This method should be the first method called when changing to a new state,
before any updates are provided.

For the base class, this method sets the sprite to the far left location on the
spritesheet based on the index and the direction of the entity.
*/
void State::enter(Stage& stage) {
	_finished = false;

	SDL_Rect& sprite = stage.scene.sprite(id).sprite;

	// The directional factor only applies for sprites that are not symmetrical
	int factor = 0;

	if (stage.scene.property(id)["symmetry"] == 0) {
		factor = factorFromDirection(stage.scene.position(id).d);
	}

	// Reset sprite to first sprite
	sprite.x = 0;
	sprite.y = _origin + factor * sprite.h;

	// Reset held frames
	_hframes = _max;
}


/** Provides all the necessary changes when leaving a state.

This method should be the last method called before changing to a new state,
after all updates are provided.
*/
void State::exit(Stage& stage) {
	if (!_loops) {
		_finished = true;
	}
}


/** Sets the reference ID for this entity.

\param reference_id
	the entity ID of the referenced entity.
*/
void State::setReferenceId(const ID reference_id) {
	_reference_id = reference_id;
}


/** The notify function should return a state pointer.

The notify function is meant to signify a sudden state change.
*/
State* State::notify(const EVENT event, const ID sender, Stage& stage) {
	return nullptr;
}


/** The update function should return a state pointer.

The update function is meant to signify a continuation of the state.
*/
State* State::update(Stage& stage) {
	updateSprite(stage);

	return nullptr;
}


/** Indicates the number of held frames left for this sprite.

\returns
	the number of held frames left.
*/
int State::hframes() const {
	return _hframes;
}


/** Indicates if the state has finished its action.

\returns
	`true` if the state is finished, `false` otherwise.
*/
bool State::isFinished() const {
	return _finished;
}


/** Indicates if this state is an initial state.

\returns
	`true` if this state is an initial state, `false` otherwise.
*/
bool State::isInitial() const {
	return _is_initial;
}


/** Indicates the name of the state.

\returns
	the name of the state.
*/
std::string State::name() const {
	return _name;
}


/** Updates the sprite for the next frame.

This method accounts for state loops by looping the sprite, otherwise it signals
that the current state is finished. The sprite is only changed if it has already
been held for the maximum number of frames.
*/
void State::updateSprite(Stage& stage) {
	--_hframes;

	if (_hframes <= 0) {
		SDL_Rect& sprite = stage.scene.sprite(id).sprite;

		// Reset held frames
		_hframes = _max;

		// Move to the next sprite or loop (if possible)
		if (sprite.x + sprite.w < sprite.w * _nsprites) {
			sprite.x += sprite.w;
		}
		else {
			if (_loops) {
				int factor = factorFromDirection(stage.scene.position(id).d);

				sprite.x = 0;
				sprite.y = _origin + factor * sprite.h;
			}
			else {
				_finished = true;
			}
		}
	}
}


/** Notifies the subentity of an event, if applicable.

\param event
	the event trigger.
\param id
	the entity that posted the event.
\param stage
	the currently loaded stage.
*/
void State::notifySubentity(const EVENT event, const ID id, Stage& stage) {
	const ID subid = stage.scene.subentity(id);

	if (subid != -1) {
		stage.scene.pda(subid).notify(event, id, stage);
	}
}


/** Updates the subentity, if applicable.

\param stage
	the current stage.
*/
void State::updateSubentity(Stage& stage) {
	const ID subid = stage.scene.subentity(id);

	if (subid != -1) {
		stage.scene.pda(subid).update(stage);
	}
}


/** Specifies the row of sprites that should be used based on direction.

\param d
	the direction the entity is facing.

\returns
	the row of sprites. The default value is `0`, indicating facing downward.
*/
int State::factorFromDirection(const DIRECTION direction) {
	switch (direction) {
	case DIRECTION::SOUTH:
		return 0;

	case DIRECTION::WEST:
		return 1;

	case DIRECTION::EAST:
		return 2;

	case DIRECTION::NORTH:
		return 3;

	default:
		return 0;
	}
}
