#include "pathfinding.hpp"


Pathfinding::Pathfinding(
    const std::string name,
    const bool is_initial,
    const std::vector<STATES> dependent_states,
    const ID id,
    const JSON& data
) : State(name, is_initial, dependent_states, id, data),
    _start(),
    _goal(),
    _thread(),
    _graph(),
    _start_node(),
    _goal_node()
{}


void Pathfinding::startPathfinding(Stage& stage) {
    setStart(stage);
    setGoal(stage);
    setGraph(stage);
    setNodes();
    findPath();
}


void Pathfinding::setStart(Stage& stage) {
    const SDL_Rect map = stage.scene.mapSize();
    const Position position = stage.scene.position(id);
    const SDL_Rect hitbox = stage.scene.hitbox(id).unionbox();

    _start = {
        (position.x - map.x + hitbox.x) / _scale,
        (position.y - map.y + hitbox.y) / _scale,
        (hitbox.w / _scale) + 2,
        (hitbox.h / _scale) + 2
    };
}


void Pathfinding::setGoal(Stage& stage) {
    _goal = _start;
}


void Pathfinding::setGraph(Stage& stage) {
    // Reset the graph
    const SDL_Rect map = stage.scene.mapSize();

    _graph.resize(map.w / _scale, map.h / _scale);

    // Set the traversibility of the graph
    const Position position = stage.scene.position(id);

    for (const ID iter_id: stage.scene.ids()) {
        const Position iter_position = stage.scene.position(iter_id);

        // Skip self
        if (iter_id == id) {
            continue;
        }

        // Skip entities on a different floor
        if (iter_position.z != position.z) {
            continue;
        }

        // Set traversibility based on the hitboxes
        const SDL_Rect iter_sprite = stage.scene.sprite(iter_id).sprite;

        for (const SDL_Rect iter_hitbox: stage.scene.hitbox(iter_id).hitbox(iter_sprite)) {
            const int x_iter = iter_position.x - map.x + iter_hitbox.x;
            const int x_min = x_iter / _scale;
            const int x_max = (x_iter + iter_hitbox.w) / _scale;

            const int y_iter = iter_position.y - map.y + iter_hitbox.y;
            const int y_min = y_iter / _scale;
            const int y_max = (y_iter + iter_hitbox.h) / _scale;

            for (int x = x_min; x <= x_max; ++x) {
                for (int y = y_min; y <= y_max; ++y) {
                    _graph.at(x, y).traversable = false;
                }
            }
        }
    }
}


void Pathfinding::setNodes() {
    _start_node = _graph.at(_start.x, _start.y);
    _goal_node = _graph.at(_goal.x, _goal.y);
}


void Pathfinding::findPath() {
    if (_graph.path_found()) {
        SDL_Point start_size = {_start.w, _start.h};
        SDL_Point goal_size = {_goal.w, _goal.h};

        _thread = std::thread(
            &Graph::find_path,
            std::ref(_graph),
            _start_node,
            _goal_node,
            manhattan_distance,
            start_size,
            goal_size
        );

        SDL_LogInfo(
            SDL_LOG_CATEGORY_APPLICATION,
            "Pathfinding :: successfully start pathfinding thread for id=%d",
            id
        );
    }
    else {
        SDL_LogWarn(
            SDL_LOG_CATEGORY_APPLICATION,
            "Pathfinding :: failed to start pathfinding thread: thread is busy"
        );
    }
}


void Pathfinding::setPath(Stage& stage) {
    if (_graph.path_found() && _thread.joinable()) {
        _thread.join();

        const SDL_Rect map = stage.scene.mapSize();
        const SDL_Rect hitbox = stage.scene.hitbox(id).unionbox();
        Path& path = stage.scene.path(id);

        path = {};

        for (const GraphNode node: _graph.path()) {
            path.push({
                (node.x * _scale) - hitbox.x + map.x,
                (node.y * _scale) - hitbox.y + map.y
            });
        }
    }
}


void Pathfinding::followPath(Stage& stage) {
    Path& path = stage.scene.path(id);
    Position& position = stage.scene.position(id);
    Velocity& velocity = stage.scene.velocity(id);

    // Continue moving toward the target location
    position.x += velocity.dx;
    position.y += velocity.dy;

    // Stop moving if target has been reached, checking along each axis
    if (position.x == path.front().x) {
        velocity.dx = 0;
    }

    if (position.y == path.front().y) {
        velocity.dy = 0;
    }

    // Begin moving toward the next target location if target has been reached
    if (velocity.dx == 0 && velocity.dy == 0) {
        path.pop();

        if (path.empty()) {
            // Final target has been reached
            _finished = true;

            return;
        }

        // Set velocity
        int dx = path.front().x - position.x;
        int dy = path.front().y - position.y;

        if (dx != 0) {
            velocity.dx = dx / std::abs(dx);
        }

        if (dy != 0) {
            velocity.dy = dy / std::abs(dy);
        }
    }
}
