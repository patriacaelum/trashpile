# Development and Team Management

last modified: 2017-12-10


### The Game Outcomes Project

This is a neat study that was done over a lot of professional game development
teams to really find the statistics on what makes a good team and what kind of
teams are able to produce the best games. You can look at the graphs and the
actual study here

- [The Best and the Rest](https://www.gamasutra.com/blogs/PaulTozour/20141216/232023/The_Game_Outcomes_Project_Part_1_The_Best_and_the_Rest.php)
- [Building Effective Teams](https://www.gamasutra.com/blogs/PaulTozour/20150106/233254/The_Game_Outcomes_Project_Part_2_Building_Effective_Teams.php)
- [Game Development Factors](https://www.gamasutra.com/blogs/PaulTozour/20150113/233922/The_Game_Outcomes_Project_Part_3_Game_Development_Factors.php)
- [Crunch Makes Games Worse](https://www.gamasutra.com/blogs/PaulTozour/20150120/234443/The_Game_Outcomes_Project_Part_4_Crunch_Makes_Games_Worse.php)
- [What Great Teams Do](https://www.gamasutra.com/blogs/PaulTozour/20150126/235024/The_Game_Outcomes_Project_Part_5_What_Great_Teams_Do.php)
  
The TL;DR version is

- Teams should have a clear, shared vision of game design and enthusiasm for
  that vision
  - Changes should cautious from deviating from the vision
  - The team should buy into the decisions that are made
  - There should be a clearly defined mission statement or set of values that
    the team genuinely believes in
- Working on the game, especially during your free time, should be voluntary
  - The core members of the team shouldn't change except for expansion when
    needed
  - Members should be held to high standards, usually implemented using design
    reviews, code reviews, etc. as oppurtunities to grow and learn
  - Everyone should be committed to making a good game
  - Teams should meet regularly for discussions and everyone should be present
    when major decisions or changes need to be made or when prioritizing the
    work to be done
- The working environment should be a safe space
  - Should be safe to take a risk and say what needs to be said and discussions
    should be unfiltered so important things don't go unsaid, the elephant in
    the room should be addressed
  - There should be a constant feedback loop so everyone knows how everyone else
    is doing and whatever issues that need to be resolved
  - Novel ideas should be celebrated, even when they fail, so members should
    feel safe to fail and experiment creatively
  - Should be an environment of mutual respect and everyone's opinion should
    count
  - Failures should be discussed openly
  - Team members should genuinely care about one another as human beings
  
Some of these points are obvious, and some are just about being a mensch.
However, having them outlined is kind of useful so we know what we need to take
care of first and what to dicuss in our meetings. For now, I suggest the
following

1. Create our mission statement or pillars of values. The three things that we
   want our game to do really really well. This will help to weed out ideas that
   don't necessarily fit to our vision and help point our thinking in the right
   direction for developing new ideas.
2. Get the working environment set up. You can't really program or paint or test
   without having the tools set up, right? Some of the following should probably
   be addressed at some point
   - Programming
     - Getting Eclipse set up
     - GitLab workflow, including the board, branching, merging, continuous
       integration, review board
     - Compiling and building for play-testing
     - Rapid prototyping using PySDL2 or GameMaker Studio
   - Art
     - Adobe master suite
     - Workfiles should be saved in PSD files and images should be saved in an
       uncompressed format (which is think the standard is BMP)
   - Sound
     - A digital audio studio (DAW) like FL Studio or LMMS would be best, though
       there are others like Ardour
     - Sound files should either uncompressed (WAV) or lossless (FLAC) format
       for editing, mixing, and exporting, and should not be lossy formats (MP3,
       OGG, etc.)
     - Scores can be written in Musescore for melodic and harmonic analysis
3. Create a backlog of tasks that need to be done
   - Create issues on GitLab
   - Prioritize issues
   - Assign tasks