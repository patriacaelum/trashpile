/** listen.hpp

\verbatim
         ||          __  __  ___  ____   ___  ____  _____ ____
         --         |  \/  |/ _ \|  _ \ / _ \/ ___|| ____/ ___|
    -- /    \ --    | |\/| | | | | |_) | | | \___ \|  _| \___ \
    -- \    / --    | |  | | |_| |  _ <| |_| |___) | |___ ___) |
         --         |_|  |_|\___/|_| \_\\___/|____/|_____|____/
         ||
\endverbatim

This `State` listens for a `RESPONSE` input from its connection.

The state process is

```
Listen <-> Speak
```
*/


#ifndef SRC_STATES_LISTEN_HPP_
#define SRC_STATES_LISTEN_HPP_


#include <string>
#include <vector>

#include "state.hpp"

#include "../components/connector.hpp"
#include "../components/id.hpp"
#include "../globals.hpp"
#include "../json.hpp"
#include "../scene.hpp"
#include "../stage.hpp"


class Listen : public State {
public:
	Listen(const ID id, const JSON& data, Stage& stage);

	void enter(Stage& stage) override;

	State* notify(const EVENT event, const ID sender, Stage& stage) override;
};


#endif /* SRC_STATES_LISTEN_HPP_ */
